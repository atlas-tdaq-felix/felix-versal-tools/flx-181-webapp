import sys
import os
from pprint import pprint
from chipscopy import create_session, delete_session, report_versions

CS_URL = os.getenv("CS_SERVER_URL", "TCP:localhost:3042")
HW_URL = os.getenv("HW_SERVER_URL", "TCP:localhost:3121")

session = create_session(cs_server_url=CS_URL, hw_server_url=HW_URL)
device = session.devices.filter_by(family="versal").get()
device.discover_and_setup_cores(ibert_scan=True)
ddr = device.ddrs[0]

ddr.get_cal_margin_mode()
'''
{'f0_rd_simp': True, 
'f0_wr_simp': True, 
'f0_rd_comp': True, 
'f0_wr_comp': True, 
'f1_rd_simp': False, 
'f1_wr_simp': False, 
'f1_rd_comp': False, 
'f1_wr_comp': False}
'''

ddr.ddr_node.get_property_group(['f0_rd_simp_rise_left'])
'''
first number - margin in picoseconds, second number is number of taps
{'f0_rd_simp_rise_lm_nib_08': [73, 148], 
'f0_rd_simp_rise_lm_nib_12': [73, 146], 
'f0_rd_simp_rise_lm_nib_14': [69, 140], 
'f0_rd_simp_rise_lm_nib_09': [73, 148], 
'f0_rd_simp_rise_lm_nib_01': [73, 150], 
'f0_rd_simp_rise_lm_nib_10': [74, 152], 
'f0_rd_simp_rise_lm_nib_00': [73, 150], 
'f0_rd_simp_rise_lm_nib_11': [74, 152], 
'f0_rd_simp_rise_lm_nib_04': [73, 148], 
'f0_rd_simp_rise_lm_nib_02': [72, 146], 
'f0_rd_simp_rise_lm_nib_05': [74, 150], 
'f0_rd_simp_rise_lm_nib_03': [72, 146], 
'f0_rd_simp_rise_lm_nib_07': [72, 146], 
'f0_rd_simp_rise_lm_nib_06': [69, 140], 
'f0_rd_simp_rise_lm_nib_15': [74, 148], 
'f0_rd_simp_rise_lm_nib_13': [74, 154]}
'''


ddr.ddr_node.get_property_group(['f0_rd_simp_rise_right'])
ddr.ddr_node.get_property_group(['f0_rd_simp_rise_center'])

ddr.report()
delete_session(session)
