'''
	Run this script from the root folder:
	python3 -m examples.vivado_remote

	The script connects to Vivado, runs an eye scan, and opens
	the python command line so that one can inspect variables.
'''

import pdb
from pprint import pprint
from util.vivado import Vivado

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "elena.zhivun@cern.ch"


# Create Vivado command wrapper
vivado_config = {
	"server" : "127.0.0.1",
	"port" : 8020,
	"timeout" : 5,
	"device" : "xcvm1802_1",
}
vivado = Vivado(vivado_config)

# Connect to the Vivado remote TCL script
vivado.connect()

# Retrieve DDRMC report
ddrmcs = vivado.get_hw_ddrmcs()
ddrmc_report = ddrmcs[0].report()

# remove existing scans and links
vivado.remove_all_hw_sio_links()
vivado.remove_all_hw_sio_scans()

# Create and configure a GTY link
link_config = {
	"LOOPBACK"		: "Near-End PMA",
	"RX_PATTERN"	: "PRBS 31",
	"TX_PATTERN"	: "PRBS 31",
	"RX_TERMINATION_VOLTAGE" : "800mv",
	"TX_DIFF_CTRL" :  "852 mV"
}

link = vivado.create_hw_sio_link("FMC_0", tx="IBERT_0.Quad_201.CH_0.TX",
	rx="IBERT_0.Quad_201.CH_0.RX")
link.set_properties(link_config)
link_report = link.report_property()

# Create a scan for this link
scan_config = {
	"DWELL_BER" : "1e-6",
	"HORIZONTAL_INCREMENT" : "2",
	"VERTICAL_INCREMENT" : "2",
}
scan = vivado.create_hw_sio_scan("pma_near_loopback", link)
scan.set_properties(scan_config)
scan_props = scan.report_property()

# Start the scan, wait until finished, retrieve the data
scan.run()
scan.wait_until_done(timeout=60)
scan_data = scan.data()

# Get the pdb command line, look at the variables
# type 'c' to continue
pdb.set_trace()

vivado.disconnect()