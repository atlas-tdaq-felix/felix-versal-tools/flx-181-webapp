open_hw_target
set_property PROGRAM.FILE {/home/lena/git/vmk180/vmk180_xdc/vmk180_xdc.runs/impl_1/Versal_CPU_wrapper.pdi} [get_hw_devices xcvm1802_1]
set_property PROBES.FILE {/home/lena/git/vmk180/vmk180_xdc/vmk180_xdc.runs/impl_1/Versal_CPU_wrapper.ltx} [get_hw_devices xcvm1802_1]
set_property FULL_PROBES.FILE {/home/lena/git/vmk180/vmk180_xdc/vmk180_xdc.runs/impl_1/Versal_CPU_wrapper.ltx} [get_hw_devices xcvm1802_1]
current_hw_device [get_hw_devices xcvm1802_1]
refresh_hw_device [lindex [get_hw_devices xcvm1802_1] 0]

create_hw_sio_link -description {SFP0} [get_hw_sio_txs {IBERT_0.Quad_105.CH_2.TX}] [get_hw_sio_rxs {IBERT_0.Quad_105.CH_2.RX}]
report_property -verbose -return_string [get_hw_sio_links {IBERT_0.Quad_105.CH_2.TX->IBERT_0.Quad_105.CH_2.RX}]

refresh_hw_device [lindex [get_hw_devices xcvm1802_1] 0]
report_property -verbose -return_string [get_hw_sio_links {IBERT_0.Quad_105.CH_2.TX->IBERT_0.Quad_105.CH_2.RX}]