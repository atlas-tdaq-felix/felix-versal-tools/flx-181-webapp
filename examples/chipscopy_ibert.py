import sys, pdb, os, json, argparse
from pprint import pprint
from pprint import pprint
from chipscopy import create_session, delete_session, report_versions, report_hierarchy
from chipscopy.api.ibert.aliases import (
    EYE_SCAN_HORZ_RANGE,
    EYE_SCAN_VERT_RANGE,
    EYE_SCAN_VERT_STEP,
    EYE_SCAN_HORZ_STEP,
    EYE_SCAN_TARGET_BER,
    PATTERN,
    RX_LOOPBACK,
)
from chipscopy.api.ibert import create_eye_scans, create_links


# converts processed chipscopy scan data to a better format
def convert_to_dict(scan):
    x_values = set()
    y_values = set()
    result = {}
    result.update(scan.scan_data.all_params)
    target_ber = result["Dwell BER"]

    data_in = scan.scan_data.processed
    for (x, y) in data_in.scan_points.keys():
        x_values.add(x)
        y_values.add(y)

    x_axis = sorted(x_values)
    y_axis = sorted(y_values)

    data_out = [[1 for y in range(len(y_axis))] for x in range(len(x_axis))]
    open_area = 0
    num_points = len(y_axis) * len(x_axis)

    for i in range(len(x_axis)):
        for j in range(len(y_axis)):
            point = data_in.scan_points[(x_axis[i], y_axis[j])]
            data_out[i][j] = point.ber
            if point.ber <= target_ber:
                open_area += 1

    result["2d statistical"] = y_axis
    result["voltage_codes"] = x_axis
    result["scan_data"] = data_out
    result["Date and Time Started"] = str(scan.start_time)
    result["Date and Time Ended"] = str(scan.stop_time)
    result["Open UI percentage"] = open_area / num_points * 100
    # this is a guess. I don't know how Vivado actually calculates this value
    result["Open Area"] = int(open_area / num_points * 16384)
    return result

parser = argparse.ArgumentParser(description="Webapp for automated hardware testing")
parser.add_argument('-ip', "--server_ip", type=str, help="Xilinx hw_server IP address", default="192.168.0.18")
parser.add_argument("-q", "--quad", type=str, help="Quad for the eye scan", default="Quad_201")
parser.add_argument("-c", "--channel", type=int, help="Channel for the eye scan", default=2)
parser.add_argument("-p", "--pattern", type=str, help="PRBS pattern", default="PRBS 31")
parser.add_argument("-l", "--loopback_mode", type=str, help="Loopback mode", default="Near-End PMA")
args = parser.parse_args()


CS_URL = os.getenv("CS_SERVER_URL", f"TCP:{args.server_ip}:3042")
HW_URL = os.getenv("HW_SERVER_URL", f"TCP:{args.server_ip}:3121")

session = create_session(cs_server_url=CS_URL, hw_server_url=HW_URL, bypass_version_check=True)
report_versions(session)
device = session.devices.filter_by(family="versal").get()
device.discover_and_setup_cores(ibert_scan=True)

ibert = device.ibert_cores.at(index=0)

# pdb.set_trace()

print(f"GT Groups available - {[gt_group_obj.name for gt_group_obj in ibert.gt_groups]}")

report_hierarchy(ibert)

quad = ibert.gt_groups.filter_by(name=args.quad)[0]
links = create_links(txs=[quad.gts[args.channel].tx], rxs=[quad.gts[args.channel].rx])

pdb.set_trace()

for link in links:
    print(f"\n----- {link.name} -----")
    tx_pattern_report = link.tx.property.report(link.tx.property_for_alias[PATTERN]).popitem()
    rx_pattern_report = link.rx.property.report(link.rx.property_for_alias[PATTERN]).popitem()
    rx_loopback_report = link.tx.property.report(link.rx.property_for_alias[RX_LOOPBACK]).popitem()

    # set TX link properties
    props = {link.tx.property_for_alias[PATTERN]: args.pattern}
    link.tx.property.set(**props)
    link.tx.property.commit(list(props.keys()))

    # set RX link properties
    props = {
        link.rx.property_for_alias[PATTERN]: args.pattern,
        link.rx.property_for_alias[RX_LOOPBACK]: args.loopback_mode,
    }
    link.rx.property.set(**props)
    link.rx.property.commit(list(props.keys()))
    
    assert link.rx.pll.locked and link.tx.pll.locked
    print(f"--> RX and TX PLLs are locked for {link}. Checking for link lock...")
    assert link.status != "No link"
    print(f"--> {link} is linked as expected")

    # don't use link.error_count on Versal! It always shows 0x9E 
    print(f"BER: {link.ber}, bit_count: {link.error_count}")


eye_scans = create_eye_scans(target_objs=[link for link in links])
for eye_scan in eye_scans:
    eye_scan.params[EYE_SCAN_HORZ_STEP].value = 8
    eye_scan.params[EYE_SCAN_VERT_STEP].value = 8
    eye_scan.params[EYE_SCAN_HORZ_RANGE].value = "-0.500 UI to 0.500 UI"
    eye_scan.params[EYE_SCAN_VERT_RANGE].value = "100%"
    eye_scan.params[EYE_SCAN_TARGET_BER].value = 1e-6

    eye_scan.start(show_progress_bar=True)
    print(f"Started eye scan {eye_scan}")

print("Waiting for the scans to complete...")

for eye_scan in eye_scans:
    eye_scan.wait_till_done()

pdb.set_trace()
scan = eye_scans[0]
data_dict = convert_to_dict(scan)

print(f"The scan is {scan.status}. Try checking these variables:\n\
      data_dict\n\
      scan.progress\n\
      scan.scan_data.all_params\n\
      scan.scan_data.processed")
pdb.set_trace()

# print("Saving data to chipscopy_data.json")
# with open("chipscopy_data.json", 'w') as f:
#     json.dump(data_dict, f, indent=4)


delete_session(session)