'''
	Run this script from the root folder:
	python3 -m examples.configure_8a34001

'''


from periphery import I2C
import xml.etree.ElementTree as ET

file_name = "./examples/8a34001/vmk180_322M265.xml"
#file_name = "./examples/8a34001/vmk180_156M25.xml"
i2c_dev = "/dev/i2c-17"
i2c_addr = 0x5b

print("Parsing configuration file: {}".format(file_name))
tree = ET.parse(file_name)
root = tree.getroot()
elems = root.findall("i2c_write")

i2c = I2C(i2c_dev)
print("Opened I2C device {}".format(i2c_dev))

for elem in elems:
	text = elem.text	
	radix = elem.get("radix")
	data = [int(x, int(radix, 10)) for x in text.split()]
	msgs = [I2C.Message(data)]
	i2c.transfer(i2c_addr, msgs)
	#print("i2c_write: {}".format(text))

i2c.close()
print("Done.")

