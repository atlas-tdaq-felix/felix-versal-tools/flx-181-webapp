#!/usr/bin/env python3

import requests, pprint

# set to the IP address of the Versal running the Flask webapp
endpoint = "http://192.168.1.114/api"

# request the list of all si570 devices
response = requests.get(endpoint + "/si570")
data = response.json()

# Print clock status
pprint.pprint(data)


# Example output:
# (assuming nominal fxtal = 114.285 MHz)

# $ python3 -m examples.using_api
# {'ddrdimm1_clk': {'FOUT': 200.105799409801,
#                   'HS_DIV': 7,
#                   'N1': 4,
#                   'RFREQ': 49.02622726932168},
#  'hsdp_clk': {'FOUT': 156.27394420990314,
#               'HS_DIV': 4,
#               'N1': 8,
#               'RFREQ': 43.7569778598845},
#  'lpddr_clk1': {'FOUT': 200.1676736773045,
#                 'HS_DIV': 7,
#                 'N1': 4,
#                 'RFREQ': 49.0413865596056},
#  'lpddr_clk2': {'FOUT': 200.1270181505914,
#                 'HS_DIV': 7,
#                 'N1': 4,
#                 'RFREQ': 49.03142589330673},
#  'ref_clk': {'FOUT': 33.33208333587013,
#              'HS_DIV': 5,
#              'N1': 30,
#              'RFREQ': 43.748632807284594},
#  'user_clk': {'FOUT': 100.00605089987889,
#               'HS_DIV': 5,
#               'N1': 10,
#               'RFREQ': 43.75292072445154},
#  'zsfp_clk': {'FOUT': 156.37633397209282,
#               'HS_DIV': 4,
#               'N1': 8,
#               'RFREQ': 43.78564717248082}}
