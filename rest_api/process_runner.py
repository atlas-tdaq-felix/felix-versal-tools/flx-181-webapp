from flask_restful import Resource
from flask import request
from markupsafe import Markup

class ProcessStatus(Resource):
	def __init__(self, model):
		self.model = model
		Resource.__init__(self)

	def get(self):
		result = self.model.get_status()
		if "start_time" in result:
			result["start_time"] = str(result["start_time"])
		if "end_time" in result:
			result["end_time"] = str(result["end_time"])
		return result
		

class ProcessStart(Resource):
	def __init__(self, model):
		self.model = model
		Resource.__init__(self)

	def post(self, model_kwargs={}):		
		result = self.model.start_test(**model_kwargs)
		if "start_time" in result:
			result["start_time"] = str(result["start_time"])
		return result
		

class ProcessStop(Resource):
	def __init__(self, model):
		self.model = model
		Resource.__init__(self)

	def post(self):
		result = self.model.stop_test()
		if "end_time" in result:
			result["end_time"] = str(result["end_time"])
		return result
