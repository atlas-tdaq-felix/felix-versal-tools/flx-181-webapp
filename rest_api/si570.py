from flask_restful import Resource


class SI570_List(Resource):
	def __init__(self, model):
		self.model = model
		Resource.__init__(self)

	def get(self):
		if self.model is None:
			return {"error" : "I/O is disabled in test mode"}
			
		return self.model.read_all()
