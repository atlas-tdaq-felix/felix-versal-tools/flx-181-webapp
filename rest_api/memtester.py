from util.form import form_fields
from . import process_runner


class MemtesterStatus(process_runner.ProcessStatus):
	pass
		

class MemtesterStart(process_runner.ProcessStart):
	def post(self):	
		fields = ["memory", "iterations", "mask"]	
		model_kwargs = form_fields(fields)

		return super().post(model_kwargs=model_kwargs)
		

class MemtesterStop(process_runner.ProcessStop):
	pass
