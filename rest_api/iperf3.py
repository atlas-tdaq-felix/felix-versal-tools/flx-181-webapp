import json
from markupsafe import Markup
from util.form import form_fields
from . import process_runner


def str_list_to_json(data):
	if not data:
		return {}
	output = "".join(data[1:])
	return json.loads(output)


class Iperf3Status(process_runner.ProcessStatus):
	def get(self):
		result = super().get()

		try:
			result["output"] = str_list_to_json(result["output"])
		except Exception as err:
			result["output"] = {}
			
		return result
		

class Iperf3Start(process_runner.ProcessStart):
	def post(self):	
		fields = ["server", "duration", "bitrate", "buffer_length",
			"window_size", "segment_size", "verbose", "udp", "reverse", "other"]	
		model_kwargs = form_fields(fields)
		model_kwargs["json_output"] = True

		return super().post(model_kwargs=model_kwargs)
		

class Iperf3Stop(process_runner.ProcessStop):
	pass
