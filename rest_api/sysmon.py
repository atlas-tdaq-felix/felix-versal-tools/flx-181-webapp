from flask_restful import Resource


class SYSMON_List(Resource):
	def __init__(self, model):
		self.model = model
		Resource.__init__(self)

	def get(self):
		return self.model.read_all()
