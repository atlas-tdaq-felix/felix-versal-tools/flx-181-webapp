from util.form import form_fields
from . import process_runner


class MtdStatus(process_runner.ProcessStatus):
	pass
		

class MtdStart(process_runner.ProcessStart):
	def post(self):	
		fields = ["device_id", "speed_test"]	
		model_kwargs = form_fields(fields)

		return super().post(model_kwargs=model_kwargs)
		

class MtdStop(process_runner.ProcessStop):
	pass
