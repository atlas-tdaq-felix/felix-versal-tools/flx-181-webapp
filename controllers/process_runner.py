from flask import render_template, redirect, session, request
from markupsafe import Markup
from datetime import datetime
from forms.memtester import MemtesterForm
from werkzeug.datastructures import ImmutableMultiDict
from pydoc import locate

form_state_running = {
	"start_btn_enabled" : False,
	"stop_btn_enabled" : True
}

form_state_ready = {
	"start_btn_enabled" : True,
	"stop_btn_enabled" : False
}


class ProcessRunnerController():
	def __init__(self, config):
		name = config["name"]
		model_path = "models.{}.{}".format(name, name.capitalize())
		self.model = locate(model_path)(config)		
		self.template = "{}.html".format(name)
		self.status_url = "/{}".format(name)
		self.form_path = "forms.{}.{}Form".format(name, name.capitalize())
		self.config = config		
		self.name = name
		self.title = config["title"]
		

	def get_status(self, template_kwargs={}):
		Form = locate(self.form_path)
		if self.name in session:
			form = Form(ImmutableMultiDict(session[self.name]))
		else:
			form = Form()

		result = self.model.get_status()
		alert = "Ready"
		running = result["running"]
		error = False
		process_output = [ Markup.escape(x) for x in result["output"] ]

		if running:
			form_state = form_state_running
			if result["start_time"] is None:
				elapsed_time = "???"
			else:
				elapsed_time = datetime.now() - result["start_time"]
			alert = [
				"Test is in progress, time elapsed: {}".format(elapsed_time),
				"Please check back again later.".format(elapsed_time)]
		else:
			form_state = form_state_ready			
			if result["start_time"] is None:
				alert = []
			else:
				error = result["return_code"] != 0
				if result["end_time"] is None:
					end_time = "???"
				else:
					end_time = result["end_time"]

				if result["end_time"] is None or result["start_time"] is None:
					elapsed_time = "???"
				else:
					elapsed_time = result["end_time"] - result["start_time"]

				alert = ["Completed on {}".format(end_time),
					"Total time passed: {}".format(elapsed_time),
					"Return code = {}".format(result["return_code"])]

		kwargs = {** template_kwargs, **self.extra_kwargs()}
		return render_template(self.template, form_state=form_state,
			process_output=process_output,
			status={ "error" : error, "running" : running, "alert" : alert},
			title=self.title, model_name=self.name,
			form=form, **kwargs)


	def start_test(self, template_kwargs={}):
		Form = locate(self.form_path)
		form = Form(request.form)
		session[self.name] = request.form		

		if not form.validate():
			result = self.model.get_status()
			kwargs = {** template_kwargs, **self.extra_kwargs()}
			return render_template(self.template,
				process_output=result["output"],
				status={
					"error" : True,
					"running" : result["running"],
					"message" : "Invalid arguments"},
				title=self.title, model_name=self.name,
				form=form, form_state=form_state_ready, **kwargs)

		self.model.start_test(**form.model_kwargs())

		return redirect(self.status_url)


	def stop_test(self):
		self.model.stop_test()
		return redirect(self.status_url)


	def extra_kwargs(self):
		return {}
