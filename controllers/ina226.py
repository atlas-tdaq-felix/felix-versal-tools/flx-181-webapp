from flask import render_template
from markupsafe import Markup
from models.ina226 import INA226


class INA226_Controller():

	def __init__(self, config):
		self.config = config
		self.model = INA226(config)


	# return all device data as a table
	def read_all(self):
		data = self.model.read_all()
		if data:
			if "error" in data:
				return render_template("error.html",
					error=Markup.escape(data["error"]))
			else:
				return render_template("ina226.html", data=data)
		else:
			return render_template("error.html",
				error="Can't read from INA226 devices")
						