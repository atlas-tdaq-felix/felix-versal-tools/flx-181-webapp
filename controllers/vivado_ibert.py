import pdb, time, json
from pprint import pprint

from flask import render_template, redirect, session, request
from markupsafe import Markup
from models.vivado_ibert import Vivado_IBERT
from models.eye_scan_plotter import EyeScanPlotter
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from werkzeug.datastructures import ImmutableMultiDict


ibert_root = "/ibert"


class Vivado_IBERT_Form(FlaskForm):
	scan_preset = SelectField("Scan preset")
	submit_button = SubmitField("Start")


Form = Vivado_IBERT_Form


class Vivado_IBERT_Controller():
	def __init__(self, config, vivado):
		self.config = config
		self.model = Vivado_IBERT(config, vivado)
		self.name = "Vivado_IBERT"
		self.plotter = EyeScanPlotter()


	def make_form(self):
		'''
			Create a form populated with the scan settings from config file
		'''
		if self.name in session:
			form = Form(ImmutableMultiDict(session[self.name]))
		else:
			form = Form()

		choices = []
		for scan_name, props in self.config["scans"].items():
			choices.append( (scan_name, props['comment']) )

		form.scan_preset.choices = choices

		return form


	def read_all(self):
		'''
			Retrieve all IBERT results
		'''
		form = self.make_form()
		ibert = self.model.get_ibert_data()
		scans = self.model.get_scan_data()

		# pdb.set_trace()
		# with open("scans.json", "w") as file: json.dump(scans, file, indent=2)

		if "error" in ibert or "error" in scans:
			error = ibert.get("error", None) or scans.get("error", None)
			if self.model.vivado is None:
				return render_template("vivado_connect.html", error=error,
					redirect=ibert_root, bad_configuration=True)
			elif not self.model.vivado.connected:
				return render_template("vivado_connect.html", error=error,
					redirect=ibert_root)
			else:
				return render_template("error.html", error=error)

		self.update_plots(scans)

		return render_template("vivado_ibert.html", ibert=ibert, scans=scans,
			form=form, timestamp=time.time())


	def run_scan(self):
		'''
			Run an eye scan
		'''
		form = Form(request.form)
		session[self.name] = request.form
		self.model.run_scan(form.scan_preset.data)
		return redirect(ibert_root)


	def ibert_reset(self):
		self.model.ibert_reset()
		return redirect(ibert_root)


	def tx_reset(self):
		self.model.tx_reset()
		return redirect(ibert_root)


	def rx_reset(self):
		self.model.rx_reset()
		return redirect(ibert_root)


	def update_plots(self, scans):
		for scan, data in scans.items():
			self.plotter.update(scan, data)


	def plot_png(self, scan, link):
		'''
			Returns png image data for the selected scan and link
		'''
		return self.plotter.plot(scan, link, format="png")