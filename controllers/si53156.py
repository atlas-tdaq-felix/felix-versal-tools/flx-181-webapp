from flask import render_template, request, redirect, session, current_app
from markupsafe import Markup
from models.si53156 import SI53156, amplitude_choices

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import validators, SelectField, SubmitField, DecimalField, BooleanField
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug.utils import secure_filename

import os, pdb

class SI53156_Controller():
	def __init__(self, config):
		self.config = config		
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.name = "si53156"
		self.model = SI53156(config)


	def read_all(self):
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()			

		if data:
			form = self.build_form(data)
			return render_template(f"{self.name}.html", data=data, form=form)
		else:
			return render_template("error.html",
				error="No data was received from SI53156 devices")


	def configure(self):
		if self.skip_io:
			return redirect(f"/{self.name}")

		data = self.model.read_all()
		form = self.build_form(data)
		
		if not form.validate():
			return render_template(f"{self.name}.html", data=data, form=form,
				error="Invalid form arguments")

		for device_name, values in data.items():
			if "error" in values:
				continue			

			output_enable = []			
			for i in values["output_enabled"]:
				output_enable.insert(i, form[f"{device_name}.oe{i}"].data)

			settings = {
				"set_amplitude" : form[f"{device_name}.set_amplitude"].data,
				"amp_sel" : int(form[f"{device_name}.amplitude"].data),
				"output_enable" : output_enable }

			self.model.configure_outputs(device_name, settings)

		return redirect(f"/{self.name}")
		


	def build_form(self, data):
		class SI53156_Form(FlaskForm):
			pass

		for device_name, values in data.items():
			if "error" in values:
				continue
			
			setattr(SI53156_Form, f"{device_name}.set_amplitude",
				BooleanField("Select amplitude", default=values["set_amplitude"]))
			setattr(SI53156_Form, f"{device_name}.amplitude", 
				SelectField("Amplitude", choices=amplitude_choices, default=values["amplitude"]["amp_sel"]) )
			for i in values["output_enabled"]:
				setattr(SI53156_Form, f"{device_name}.oe{i}",
					BooleanField(f"Output enabled {i}", default=values["output_enabled"][i]))

		form = SI53156_Form()

		return form

