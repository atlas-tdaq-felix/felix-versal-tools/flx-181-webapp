from .process_runner import ProcessRunnerController
from markupsafe import Markup

class Iperf3Controller(ProcessRunnerController):
	def extra_kwargs(self):		
		ifconfig = Markup.escape(self.model.ifconfig())
		template_kwargs={"ifconfig_output" : ifconfig}
		return template_kwargs


		
