from flask import render_template
from markupsafe import Markup
from models.vivado_ddrmc import Vivado_DDRMC
from util import vivado_ddrmc


class Vivado_DDRMC_Controller():
	def __init__(self, config, vivado):
		self.config = config
		self.model = Vivado_DDRMC(config, vivado)

	def read_all(self):
		data = self.model.read_all()		

		if "error" in data:
			if self.model.vivado is None:
				return render_template("vivado_connect.html", error=data["error"], redirect="/ddrmc",
					bad_configuration=True)
			elif not self.model.vivado.connected:
				return render_template("vivado_connect.html", error=data["error"], redirect="/ddrmc")
			else:
				return render_template("error.html", error=data["error"])
				

		if "style" in self.config:
			margins = vivado_ddrmc.vivado_read_margins(data, self.config["style"])	

		return render_template("vivado_ddrmc.html", ddrmc_data=data)
