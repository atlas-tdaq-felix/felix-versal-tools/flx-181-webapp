import pdb, time, json
from pprint import pprint

from flask import render_template, redirect, session, request
from markupsafe import Markup
from models.chipscopy_ibert import Chipscopy_IBERT
from models.eye_scan_plotter import EyeScanPlotter
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from werkzeug.datastructures import ImmutableMultiDict


root_url = "/eye_scan"


class ChipscopyEyeScanForm(FlaskForm):
	scan_preset = SelectField("Scan preset")
	submit_button = SubmitField("Start")



Form = ChipscopyEyeScanForm


class ChipscopyEyeScanController():
	def __init__(self, config, chipscopy, model):
		self.config = config
		self.model = model
		self.name = "Chipscopy_EyeScan"
		self.plotter = EyeScanPlotter(model)


	def make_form(self):
		'''
			Create a form populated with the scan settings from config file
		'''
		if self.name in session:
			form = Form(ImmutableMultiDict(session[self.name]))
		else:
			form = Form()

		choices = []
		for scan_name, props in self.config["scans"].items():
			choices.append( (scan_name, props['comment']) )

		form.scan_preset.choices = choices

		return form


	def read_all(self):
		'''
			Retrieve all IBERT results
		'''
		form = self.make_form()		
		scans = self.model.get_scan_data()

		# with open("scans.json", "w") as file: json.dump(scans, file, indent=2)

		if "error" in scans:
			error = scans.get("error", None)
			if self.model.chipscopy is None:
				return render_template("error.html", error=(
					"Bad board configuration, chipscopy connection is missing"))				
			elif not self.model.chipscopy.connected():
				return redirect(f"/chipscopy/connect?redirect={root_url}")
			else:
				error = f"Try disconnecting and reconnecting chipscopy. Error: {error}"
				return render_template("error.html", error=error)

		self.update_plots(scans)
		
		is_scheduler_running = self.model.is_scheduler_running()
		scheduler_errors = self.model.get_scheduler_errors()
		scheduler_progress = self.model.get_scheduler_progress()

		if is_scheduler_running:
			controller_result = session.get("eye_scan_run_result",
				   {"status" : 'Running a scan', 'error' : False})
		else:
			controller_result = session.get("eye_scan_run_result",
				   {"status" : 'Ready', 'error' : False})
			
		return render_template("chipscopy_eye_scan.html", scans=scans,
			form=form, timestamp=time.time(), is_scheduler_running=is_scheduler_running,
			scheduler_errors=scheduler_errors, controller_result=controller_result, 
			scheduler_progress=scheduler_progress)


	def run_scan(self):
		'''
			Run an eye scan
		'''
		form = Form(request.form)
		session[self.name] = request.form
		result = self.model.run_scan(form.scan_preset.data)
		session["eye_scan_run_result"] = result
		return redirect(root_url)


	def update_plots(self, scans):
		for scan, data in scans.items():
			self.plotter.update(scan, data)


	def plot_png(self, scan, link):
		'''
			Returns png image data for the selected scan and link
		'''		
		return self.plotter.plot(scan, link, format="png")

