from flask import render_template, request, redirect
from markupsafe import Markup
from models.gpio import GPIO

import re


class GPIO_Controller():

	def __init__(self, config):
		self.config = config
		self.model = GPIO(config)
		self.bitfield = re.compile("bit(?P<bit>\\d+)")


	# return all device data as a table
	def read_all(self):
		data = self.model.read_all()
		if "error" in data:
			return render_template("error.html", error=data["error"])
		else:			
			# self.add_forms(data)
			return render_template("gpio.html", data=data)


	def set(self):
		form = request.form
		if not "device" in form:
			return render_template("error.html", error="GPIO device is not specified")
		device = form["device"]

		bit_data = {}
		for name, value in request.form.items():
			match = self.bitfield.match(name)
			if match:
				bit = int(match.group("bit"))
				bit_data[bit] = value
		self.model.set(device, bit_data)		
		return redirect("/gpio")
			