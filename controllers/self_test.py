from flask import render_template, request, redirect, url_for, session, jsonify, make_response
from wtforms import StringField, FileField, SubmitField, HiddenField, BooleanField
from wtforms.validators import DataRequired, Regexp
from flask_wtf import FlaskForm
from markupsafe import Markup
from models.self_test import SelfTest
from datetime import datetime
import pdb, re

# Format the test runner status for the template
def format_status(status):
    start_time = status['start_time']
    end_time = status['end_time']

    if not start_time is None:
        status['start_time'] = start_time.isoformat()
        if not end_time is None:
            status['time_elapsed'] = str(end_time - start_time)                
            status['end_time'] = end_time.isoformat()
        else:
            status['time_elapsed'] = str(datetime.now() - start_time)
            status['end_time'] = "N/A"
    else:
        status['time_elapsed'] = "N/A"
        status['start_time'] = "N/A"


# Format the test results summary for the template
def format_summary(summary):
    for name, item in summary.items():
        if item["status"] == "error":
            item["class"] = "danger"
        elif item["status"] == "warning":
            item["class"] = "warning"
        elif item["status"] == "success":
            item["class"] = "success"
        else:
            item["class"] = "primary"


# Format the test runner configuration for the template
def format_schedule(schedule):
    for name, item in schedule.items():        
        item["checked"] = "" if item["skip"] else "checked"


# Validate board name, revision, etc
valid_name_regexp = r'^[a-zA-Z0-9\-_]+$'
valid_name_error_msg = "Please only use alphanumeric characters and _-"

def ValidName():
    return Regexp(valid_name_regexp, message=valid_name_error_msg)


class BoardUploadForm(FlaskForm):
    board_name = StringField('Board Name', validators=[DataRequired(), ValidName()])
    board_revision = StringField('Board Revision', validators=[DataRequired(), ValidName()])
    board_id = StringField('Board identifier', validators=[DataRequired(), ValidName()])
    submit = SubmitField('Upload')


schedule_regexp = re.compile(r"(?P<name>\w+)\.(?P<param>\w+)")

class SelfTestController():
    def __init__(self, config, test_models):
        self.config = config
        self.model = SelfTest(config, test_models)
        self.test_models = test_models
        self.name = "self_test"


    def start(self):
        schedule = {}
        for name, value in request.form.items():
            match = schedule_regexp.match(name)
            if not match:
                continue

            test_name = match.group("name")
            param_name = match.group("param")

            if not test_name in schedule:
                schedule[test_name] = {}

            if param_name == "test_enabled":
                if isinstance(value, str) and value != "":
                    enabled = False if value == "0" else True
                else:
                    enabled = bool(value)

                schedule[test_name]['skip'] = not enabled

            else:
                if not "kwargs" in schedule[test_name]:
                    schedule[test_name]["kwargs"] = {}

                schedule[test_name]["kwargs"][param_name] = value
        
        session[self.name] = self.model.start_test(schedule)
        return redirect(url_for("self_test_status"))


    def stop(self):
        session[self.name] = self.model.stop_test()
        return redirect(url_for("self_test_status"))


    def skip(self):
        session[self.name] = self.model.skip_test()
        return redirect(url_for("self_test_status"))


    def status(self):
        status = self.model.get_status()
        format_status(status)

        summary = self.model.get_summary()
        format_summary(summary)

        schedule = self.model.get_schedule()
        format_schedule(schedule)

        controller_status = session.pop(self.name, {})
        return render_template("self_test.html", status=status,
            controller_status=controller_status, schedule=schedule, summary=summary)


    def download_json_results(self):
        test_results = self.model.get_results()
        if not "board_id" in self.test_models:
            raise RuntimeError("BUG: board_id definitions are not found!")
        
        board_id = self.test_models["board_id"]
        board_name = board_id.get_board_name()
        end_time = test_results["self_test_status"]["end_time"]
        if end_time is None:
            time_stamp = "incomplete_test"
        else:
            time_stamp = end_time.strftime("%Y-%m-%d_%Hh%Mm")
        format_status(test_results["self_test_status"])
        file_name = f"{board_name}_{time_stamp}"        

        response = make_response(jsonify(test_results))
        response.headers["Content-Disposition"] = f"attachment; filename={file_name}.json"
        response.headers["Content-Type"] = "application/json"
        return response


    def upload_results_to_cloud(self):
        pass
