from flask import render_template, request, redirect, session, current_app
from markupsafe import Markup
from models.si5345 import SI5345

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import validators, SelectField, SubmitField, DecimalField
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug.utils import secure_filename

import os, pdb
from os import listdir, path
from os.path import isfile


class SI5345_Controller():
	def __init__(self, config):
		self.config = config		
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.name = "si5345"
		self.model = SI5345(config)


	def read_all(self):
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()

		if data:
			forms = self.build_forms()
			# pdb.set_trace()
			return render_template(f"{self.name}.html", data=data, forms=forms)
		else:
			return render_template("error.html",
				error="No data was received from SI5345 devices")


	def _form_helper(self, form_name):
		if self.skip_io:
			return redirect(f"/{self.name}")
		
		if form_name == "clock_config":
			forms = self.build_forms()
		else:
			forms = self.build_forms()

		form = forms[form_name]
		data = self.model.read_all()
		device_name = form.device_name.data

		return { "forms" : forms, "form" : form, "data" : data, "device_name" : device_name }


	def update_input_config(self):
		if self.skip_io:
			return redirect(f"/{self.name}")

		forms = self.build_forms()
		form = forms["clock_config"]
		device_name = form.device_name.data
		data = self.model.read_all()
		
		if not form.validate():	
			# pdb.set_trace()			
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error="Invalid form arguments")

		new_config = {
			'f_xaxb_MHz' : float(form.f_xaxb_MHz.data),
			'f_in0_MHz' : float(form.f_in0_MHz.data),
			'f_in1_MHz' : float(form.f_in1_MHz.data),
			'f_in2_MHz' : float(form.f_in2_MHz.data),
			'f_in3_MHz' : float(form.f_in3_MHz.data),			
			'in_sel' : int(form.in_sel.data),
		}		

		result = self.model.update_config(device_name, new_config)


		if "error" in result:
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error=f"Device {device_name} doesn't exist!")

		return redirect(f"/{self.name}")


	def load_configuration(self, device_name, register_data):
		result = self.model.load_configuration(device_name, register_data)
		if "error" in result:
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error=result["error"])		


	def upload_register_configuration(self):
		if self.skip_io:
			return redirect(f"/{self.name}")

		forms = self.build_forms()
		form = forms["registers_upload"]
		device_name = form.device_name.data
		data = self.model.read_all()
		
		if not form.validate():	
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error="Please select a file to upload!")
		
		f = form.file.data
		register_data = f.stream.readlines()

		self.load_configuration(device_name, register_data)

		return redirect(f"/{self.name}")


	def select_preset_configuration(self):	
		if self.skip_io:
			return redirect(f"/{self.name}")

		forms = self.build_forms()
		form = forms["preset_select"]
		device_name = form.device_name.data
		data = self.model.read_all()

		if not form.validate():				
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error="Please select a valid preset!")
		
		file_name = path.join(current_app.root_path, "chip_configs", "si5345", form.preset_name.data)

		if not isfile(file_name):
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error="Preset file doesn't exist!")

		with open(file_name, 'r') as file:
			register_data = file.readlines()	
			self.load_configuration(device_name, register_data)

		return redirect(f"/{self.name}")


	def load_default_preset(self):
		if self.skip_io:
			return redirect(f"/{self.name}")

		for device in self.config['mapping'].keys():
			preset = self.config['mapping'][device]['default_preset']
			file_name = path.join(current_app.root_path, "chip_configs", "si5345", preset)

			if not isfile(file_name):
				forms = self.build_forms()
				data = self.model.read_all()
				return render_template(f"{self.name}.html", data=data, forms=forms,
					error="Preset file doesn't exist!")

			with open(file_name, 'r') as file:
				register_data = file.readlines()
				self.load_configuration(device, register_data)


	def _reset(self, device_name, reset_function):
		if self.skip_io:
			return redirect(f"/{self.name}")
		
		result = reset_function(device_name)
		if "error" in result:
			data = self.model.read_all()
			forms = self.build_forms()
			return render_template(f"{self.name}.html", data=data, forms=forms,
				error=result["error"])

		return redirect(f"/{self.name}")


	def soft_rst(self, device_name):
		return self._reset(device_name, self.model.soft_rst)


	def hard_rst(self, device_name):
		# During hard reset, SI5345 doesn't ACK the transaction, generating an error message
		self._reset(device_name, self.model.hard_rst)
		return redirect(f"/{self.name}")


	def device_choices(self):
		choices = []
		for name in self.model.get_device_list():
			choices.append((name, name))
		return choices


	def preset_choices(self):
		config_directory = path.join(current_app.root_path, "chip_configs", "si5345")
		return [(f, f) for f in listdir(config_directory) if isfile(path.join(config_directory, f))]


	def build_config_form(self):
		form = SI5345_ConfigForm()
		form.device_name.choices = self.device_choices()
		return form


	def build_preset_form(self):
		device_choices = self.device_choices()
		preset_choices = self.preset_choices()
		form = SI5345_PresetSelectForm()
		form.device_name.choices = device_choices
		form.preset_name.choices = preset_choices
		return form


	def build_upload_form(self):
		device_choices = self.device_choices()
		form = SI5345_RegistersUploadForm()
		form.device_name.choices = device_choices
		return form


	def build_forms(self):
		config_form = self.build_config_form()
		upload_form = self.build_upload_form()
		preset_form = self.build_preset_form()

		return { 
			"clock_config" : config_form,
			"registers_upload" : upload_form,
			"preset_select" : preset_form }



class SI5345_ConfigForm(FlaskForm):
	device_name = SelectField("SI5345 device")
	f_xaxb_MHz = DecimalField("XAXB Frequency [MHz]", validators=[validators.InputRequired(),
		validators.NumberRange(min=24.97, max=54.06)])
	f_in0_MHz = DecimalField("IN0 Frequency [MHz]", validators=[validators.InputRequired(),
		validators.NumberRange(min=0.0, max=250)])
	f_in1_MHz = DecimalField("IN1 Frequency [MHz]", validators=[validators.InputRequired(),
		validators.NumberRange(min=0.0, max=250)])
	f_in2_MHz = DecimalField("IN2 Frequency [MHz]", validators=[validators.InputRequired(),
		validators.NumberRange(min=0.0, max=250)])
	f_in3_MHz = DecimalField("IN3 Frequency [MHz]", validators=[validators.InputRequired(),
		validators.NumberRange(min=0.0, max=250)])
	in_sel = SelectField("IN_SEL pin state", choices=[('0', '0'), ('1', '1'), ('2', '2'), ('3', '3')])
	submit_button = SubmitField("Update")


class SI5345_RegistersUploadForm(FlaskForm):
	device_name = SelectField("SI5345 device")
	file = FileField("ClockBuilder Pro register file", validators=[FileRequired()])
	submit_button = SubmitField("Upload")


class SI5345_PresetSelectForm(FlaskForm):
	device_name = SelectField("SI5345 device")
	preset_name = SelectField("Configuration preset")
	submit_button = SubmitField("Apply")