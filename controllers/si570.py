from flask import render_template, request, redirect, session, current_app
from markupsafe import Markup
from models.si570 import SI570

from flask_wtf import FlaskForm
from wtforms import validators, SelectField, SubmitField, DecimalField
from werkzeug.datastructures import ImmutableMultiDict

import pdb


class SI570_Controller():
	def __init__(self, config):
		self.config = config		
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.name = "si570"
		self.model = SI570(config) if not self.skip_io else None


	def read_all(self):	
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()
		form = self.build_form(device_data=data)

		if data:
			if "error" in data:
				return render_template("error.html", error=data["error"])
			return render_template("si570.html", data=data, form=form)
		else:
			return render_template("error.html",
				error="No data received from SI570 devices")
		

	def set_frequency(self):
		if self.skip_io:
			return redirect("/si570")

		form = self.build_form(form_data=request.form)		

		if not form.validate():	
			data = self.model.read_all()
			return render_template("si570.html", data=data, form=form)
		
		result = self.model.set_frequency(form.device_id.data,
			form.freq.data)

		if "error" in result:
			data = self.model.read_all()
			return render_template("si570.html", data=data, form=form,
				error=result["error"])

		return redirect("/si570")


	def load_nvm(self):
		if self.skip_io:
			return redirect("/si570")

		form = self.build_form(form_data=request.form)
		result = self.model.load_nvm(form.device_id.data)
		if "error" in result:
			data = self.model.read_all()
			return render_template("si570.html", data=data, form=form,
				error=result["error"])
		return redirect("/si570")


	def reset(self):
		if self.skip_io:
			return redirect("/si570")

		form = self.build_form(form_data=request.form)
		result = self.model.reset(form.device_id.data)
		if "error" in result:
			data = self.model.read_all()
			return render_template("si570.html", data=data, form=form,
				error=result["error"])
		return redirect("/si570")


	def build_form(self, form_data=None, device_data=None):
		if form_data is None:
			if self.name in session:
				form = SI570_Form(ImmutableMultiDict(session[self.name]))
			else:
				form = SI570_Form()
		else:
			form = SI570_Form(form_data)
			session[self.name] = form_data

		choices = []
		for name, device in self.config["mapping"].items():
			if not device_data is None and name in device_data and 'error' in device_data[name]:
				continue
			choices.append((name, name))
		form.device_id.choices = choices
		return form



class SI570_Form(FlaskForm):
	device_id = SelectField("SI570 device")
	freq = DecimalField("Frequency [MHz]",
		validators=[validators.DataRequired(),
		validators.NumberRange(min=10, max=1417.5)], default=100)
	submit_button = SubmitField("Set")
