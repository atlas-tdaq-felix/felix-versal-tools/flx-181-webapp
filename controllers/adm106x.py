from flask import render_template, request, current_app
from markupsafe import Markup
from models.adm106x import ADM106x
import pdb


class ADM106x_Controller():
	def __init__(self, config):
		self.config = config
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.model = ADM106x(config) if not self.skip_io else None


	def read_all(self):
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()
		
		for name, device in data.items():
			if "error" in device:
				device["device_type"] = "(N/A)"
				continue

			for pin_name, pin in device["inputs"].items():
				fault = []
				warning = False
				if pin["adc"]["fault"]:
					fault.append("FAULT")
				# if pin["adc"]["limit"]:
				# 	fault.append("ADC LIMIT")
				# 	warning = True
				if pin["adc"]["overvoltage"]:
					fault.append("OVER")
					warning = True
				if pin["adc"]["undervoltage"]:
					fault.append("UNDER")
					warning = True
				
				pin["adc"]["fault_summary"] = ", ".join(fault) if fault else " "
				pin["adc"]["warning"] = warning		

				
				if "uv_fault_enabled" in pin and "ov_fault_enabled" in pin:
					fault_select = []
					if pin["uv_fault_enabled"]:
						fault_select.append("UV") 
					if pin["ov_fault_enabled"]:
						fault_select.append("OV")
					pin["fault_select"] = ", ".join(fault_select) if fault_select else "(none)"

		return render_template("adm106x.html", data=data)

		