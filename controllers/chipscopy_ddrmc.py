from flask import render_template
from models.chipscopy_ddrmc import Chipscopy_DDRMC
from flask import render_template, request, redirect, url_for


class Chipscopy_DDRMC_Controller():
	def __init__(self, config, chipscopy):
		self.config = config
		self.model = Chipscopy_DDRMC(config, chipscopy)

	def read_all(self):
		data = self.model.read_all()		

		if "error" in data:
			if self.model.chipscopy is None:
				return render_template("error.html", error=(
					"Bad board configuration, chipscopy connection is missing"))
			elif not self.model.chipscopy.connected():
				return redirect("/chipscopy/connect?redirect=/ddrmc")
			else:
				return render_template("error.html", error=data["error"])
				
		return render_template("chipscopy_ddrmc.html", ddrmc_data=data)



