from .process_runner import ProcessRunnerController

class MemtesterController(ProcessRunnerController):
	def extra_kwargs(self):
		meminfo = self.model.meminfo()
		template_kwargs={"meminfo" : meminfo}
		return template_kwargs
