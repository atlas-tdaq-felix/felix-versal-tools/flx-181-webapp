import pdb, time, json
from pprint import pprint

from flask import render_template, redirect, session, request
from markupsafe import Markup
from models.chipscopy_ibert import Chipscopy_IBERT
from models.eye_scan_plotter import EyeScanPlotter
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from werkzeug.datastructures import ImmutableMultiDict


ibert_root = "/ibert"
ibert_fields = {
	"RX_LOOPBACK" : "Loopback",
	"TX_PATTERN" : "Pattern",
	"RX_PATTERN" : "Pattern",
	"TX_PRE_CURSOR" : "Pre Cursor",
	"TX_POST_CURSOR" : "Post Cursor",
	"TX_DIFF_CTRL" : "Differential Control",
	"RX_TERMINATION_VOLTAGE" : "Termination Voltage" }



def get_ibert_form(ibert):
	class IBERT_Form(FlaskForm):		
		pass

	for link_name, data in ibert.items():
		if "error" in data:
			continue
		
		for label in ibert_fields:
			setattr(IBERT_Form, f"{link_name}.{label}",
				SelectField(f"{link_name}.{label}", choices=[(x, x) for x in data[label]['Valid values']],
				default=data[label]['Current value']))

	return IBERT_Form()



class Chipscopy_IBERT_Controller():
	def __init__(self, config, chipscopy):
		self.config = config
		self.model = Chipscopy_IBERT(config, chipscopy)
		self.name = "Chipscopy_IBERT"


	def read_all(self):
		'''
			Retrieve all IBERT results
		'''
		
		# block IBERT page if the eye scan is running or it will freeze
		# (checked with ChipScoPy 2022.2)
		is_scheduler_running = self.model.is_scheduler_running()
		if is_scheduler_running:
			return render_template("error.html", error=(
				"An Eye Scan is currently running! Please wait for all eye scans to finish first."))

		ibert = self.model.get_ibert_data()

		if "error" in ibert:
			error = ibert.get("error", None)
			if self.model.chipscopy is None:
				return render_template("error.html", error=(
					"Bad board configuration, chipscopy connection is missing"))				
			elif not self.model.chipscopy.connected():
				return redirect(f"/chipscopy/connect?redirect={ibert_root}")
			else:
				error = f"Try disconnecting and reconnecting chipscopy. Error: {error}"
				return render_template("error.html", error=error)

		ibert_form = get_ibert_form(ibert)
		return render_template("chipscopy_ibert.html", ibert=ibert, ibert_form=ibert_form)


	def ibert_reset(self):
		self.model.ibert_reset()
		return redirect(ibert_root)


	def tx_reset(self):
		self.model.tx_reset()
		return redirect(ibert_root)


	def rx_reset(self):
		self.model.rx_reset()
		return redirect(ibert_root)


	def update_ibert_settings(self):
		ibert = self.model.get_ibert_data()

		if "error" in ibert:
			return redirect(ibert_root)

		ibert_form = get_ibert_form(ibert)

		if not ibert_form.validate():
			return redirect(ibert_root)

		link_settings = {}
		for link_name, data in ibert.items():
			for label in ibert_fields:
				field_name = f"{link_name}.{label}"
				if not field_name in ibert_form:
					continue

				form_data = ibert_form[field_name].data
				current_value = data[label]['Current value']
				if form_data == current_value:
					continue

				if not link_name in link_settings:
					link_settings[link_name] = { "rx" : {}, "tx" : {}}

				if label.startswith("RX_"):
					link_settings[link_name]["rx"][ibert_fields[label]] = form_data
				elif label.startswith("TX_"):
					link_settings[link_name]["tx"][ibert_fields[label]] = form_data
				else:
					raise ValueError(f"Invalid IBERT link property name: {label} must start with 'TX_' or 'RX_'!")		
		
		self.model.update_link_settings(link_settings)
		return redirect(ibert_root)
				
