import pdb
from flask import render_template, request, redirect
from markupsafe import Markup
from models.ccf_clocks import CCF_Clocks
import math

class CCF_ClocksController():

    def __init__(self, config):
        self.config = config
        self.model = CCF_Clocks(config)


    def read_all(self):
        data = self.model.read_all()
        if "error" in data:
            return render_template("error.html", error=data["error"])
        else:           
            return render_template("ccf_clocks.html", data=data)