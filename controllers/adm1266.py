from flask import render_template, request, current_app
from markupsafe import Markup
from models.adm1266 import ADM1266
import pdb


class ADM1266_Controller():
	def __init__(self, config):
		self.config = config
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.model = ADM1266(config) if not self.skip_io else None


	def read_all(self):
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()
		
		for name, device in data.items():
			if "error" in device:
				device["device_type"] = "(N/A)"
				continue

		return render_template("adm1266.html", data=data)
