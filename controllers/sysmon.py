from flask import render_template, request
from markupsafe import Markup
from models.sysmon import SYSMON
import pdb


class SYSMON_Controller():
	def __init__(self, config):
		self.config = config
		self.model = SYSMON(config)

	def read_all(self):
		device_class = request.args.get("class")
		valid_devices = self.config["device_classes"].keys()

		if device_class is None:
			return render_template("error.html", error=(f"HWMON device class must be specified "
				+ f"as a 'class' GET parameter. Valid choices: {list(valid_devices)}"))

		data = self.model.read_all(device_class)

		if "error" in data:
			return render_template("error.html", error=data["error"])
		else:
			return render_template("sysmon.html", data=data)
