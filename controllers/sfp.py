from flask import render_template, current_app, redirect, request
from markupsafe import Markup
from models.sfp import SFP


class SFP_Controller():
	def __init__(self, config):
		self.config = config
		self.skip_io = current_app.config["USER_SKIP_IO"] if "USER_SKIP_IO" in current_app.config else False
		self.model = SFP(config) if not self.skip_io else None
		self.name = "sfp"


	def read_all(self):
		if self.skip_io:
			return render_template("error.html", error="I/O is disabled in test mode")

		data = self.model.read_all()

		if data:
			if "error" in data:
				return render_template("error.html",
					error=Markup.escape(data["error"]))
			else:
				return render_template("sfp.html", data=data)
		else:
			return render_template("error.html",
				error="No data received from SFP devices")


	def configure(self, device_name):
		if self.skip_io:
			return redirect(f"/{self.name}")
	
		self.model.configure(device_name, dict(request.form))

		return redirect(f"/{self.name}")


	def configure_all(self):
		if self.skip_io:
			return redirect(f"/{self.name}")

		self.model.configure_all()
		return redirect(f"/{self.name}")


	def reset(self):
		if self.skip_io:
			return redirect(f"/{self.name}")
	
		self.model.reset()
		return redirect(f"/{self.name}")
