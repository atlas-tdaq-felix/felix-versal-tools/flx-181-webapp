from flask_wtf import FlaskForm
from wtforms import validators, BooleanField, StringField, SelectField
from wtforms import PasswordField, IntegerField, SubmitField


class MemtesterForm(FlaskForm):
	memory = StringField("Memory size", validators=[
		validators.DataRequired(),
		validators.Regexp("^\\d+[BKMG]?$")],
		default="64M")

	iterations = IntegerField("Iterations", [validators.NumberRange(min=1)],
		default=1)

	test_random_value = BooleanField("Random Value") #0
	test_cmp_xor = BooleanField("Compare XOR") #1
	test_cmp_sub = BooleanField("Compare SUB") #2
	test_cmp_mul = BooleanField("Compare MUL") #3
	test_cmp_div = BooleanField("Compare DIV") #4
	test_cmp_or = BooleanField("Compare OR") #5
	test_cmp_and = BooleanField("Compare AND") #6

	test_seq_inc = BooleanField("Sequential Increment") #7
	test_solid_bits = BooleanField("Solid Bits") #8
	test_block_seq = BooleanField("Block Sequential", default=True) #9
	test_checkerboard = BooleanField("Checkerboard") # 10
	test_bit_spread = BooleanField("Bit Spread") # 11 
	test_bit_flip = BooleanField("Bit Flip") # 12
	test_walking_one = BooleanField("Walking Ones") #13
	test_walking_zeroes = BooleanField("Walking Zeroes") #14

	submit_button = SubmitField("Start")


	def memtester_mask(self):
		# return bit mask for memtester
		mask = (int(self.test_random_value.data)
			| int(self.test_cmp_xor.data) << 1 
			| int(self.test_cmp_sub.data) << 2 
			| int(self.test_cmp_mul.data) << 3 
			| int(self.test_cmp_div.data) << 4 
			| int(self.test_cmp_or.data) << 5
			| int(self.test_cmp_and.data) << 6 
			| int(self.test_seq_inc.data) << 7 
			| int(self.test_solid_bits.data) << 8
			| int(self.test_block_seq.data) << 9 
			| int(self.test_checkerboard.data) << 10
			| int(self.test_bit_spread.data) << 11 
			| int(self.test_bit_flip.data) << 12
			| int(self.test_walking_one.data) << 13 
			| int(self.test_walking_zeroes.data) << 14 )

		print(mask)

		return "0x{:X}".format(mask)


	def model_kwargs(self):
		return {
			"memory" : self.memory.data,
			"iterations" : str(self.iterations.data),
			"mask" : self.memtester_mask()}