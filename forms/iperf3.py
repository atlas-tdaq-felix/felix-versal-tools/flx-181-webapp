from flask import request
from flask_wtf import FlaskForm
from wtforms import validators, BooleanField, StringField, SelectField
from wtforms import PasswordField, IntegerField, SubmitField
from util.form import ProcessForm


class Iperf3Form(FlaskForm, ProcessForm):
	server = StringField("Server IP address", [validators.DataRequired(),
		validators.IPAddress()], default=request.remote_addr)
	duration = IntegerField("Duration [s]", [validators.Optional(),
		validators.NumberRange(min=1)], default=10)
	bitrate = StringField("Target bitrate n[KM]", [validators.Optional()])
	buffer_length = StringField("Read/write buffer length n[KM]", [validators.Optional()])
	window_size = StringField("Window size n[KM]", [validators.Optional()])
	segment_size = StringField("Maximum segment size n", [validators.Optional()])
	other = StringField("Extra command line arguments", [validators.Optional()])

	verbose = BooleanField("Verbose", default=True)
	udp = BooleanField("UDP")
	reverse = BooleanField("Reverse")

	submit_button = SubmitField("Start")
