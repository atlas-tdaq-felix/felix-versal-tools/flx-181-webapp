from flask import request
from flask_wtf import FlaskForm
from wtforms import validators, BooleanField, StringField, SelectField
from wtforms import PasswordField, IntegerField, SubmitField
from util.form import ProcessForm

from subprocess import check_output
import re, pdb

device_regex = re.compile('^(?P<name>mtd(?P<id>\\d+)):\\s+(?P<size>[0-9a-fA-F]+)\\s+(?P<erasesize>[0-9a-fA-F]+)\\s+\"(?P<label>.*)\"$')


# Returns a dictionary of all available mtd devices
def mtd_devices():
	result = {}
	lines = check_output(['cat', '/proc/mtd']).decode("utf-8").split("\n")

	for line in lines:		
		m = device_regex.match(line)
		if m:
			result[m.group("id")] = {
				"name" : m.group("name"),
				"size" : m.group("size"),
				"erasesize" : m.group("erasesize"),
				"label" : m.group("label"),
			}
	return result


# Return the list of choices for the device select field
def get_device_choices():
	devices = mtd_devices()
	choices = []
	for device_id, device in devices.items():
		label = "{}: {}, size={}".format(device["name"], device["label"], device["size"])
		choices.append((device_id, label))
	return choices


class MtdForm(FlaskForm, ProcessForm):
	choices = get_device_choices()
	device_id = SelectField("MTD devices", choices=choices, validators=[validators.DataRequired()])
	# speed_test = BooleanField("Speed test", default=False)
	submit_button = SubmitField("Start")




