#!/usr/bin/env bash

usage() {
    echo "Usage: $0 [-s] [-h] -d <mtd number>"
    echo ""
    echo "  -h      Display this message and exit"
    echo "  -s      Run speed test"
    echo "  -d      MTD device number"    
}

exit_if_error() {
    if [ $1 -ne  0 ]
    then
        exit $1
    fi
}

device_specified=0
do_speedtest=0

while getopts ":shd:" o; do
    case "${o}" in
        s)
            do_speedtest=1
            ;;
        d)
            device=${OPTARG}
            device_specified=1
            ;;
        h)
            usage
            exit 0
            ;;
    esac
done
shift $((OPTIND-1))

if [ ${device_specified} -eq  0 ]
then
    echo "You must specify MTD number to test"
    exit 1
fi

# Run nandtest utility
echo "# nandtest /dev/mtd${device}"
nandtest /dev/mtd${device}
exit_if_error $?


# Run speed test if enabled
# This test uses printk, output is not easily redirected
if [ ${do_speedtest} -eq  1 ]
then
    echo "# insmod /lib/modules/`uname -r`/kernel/drivers/mtd/tests/mtd_speedtest.ko dev=${device}"
    insmod /lib/modules/`uname -r`/kernel/drivers/mtd/tests/mtd_speedtest.ko dev=${device}
    exit_if_error $?
    modprobe -r mtd_speedtest
    exit_if_error $?
fi