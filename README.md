# SoC board demo and hardware self-test

This board demo webapp allows users to access the board peripherals and run self-tests via the web-interface. The webapp can be easily adapted to different hardware by adjusting the board configuration (see `boards` folder).

Ultimately, the webapp will upload the test results of the individual boards to the centralized storage, to facilitate the QA of the manufactured boards.


# Requirements

TBD

# How to run

## On the embedded platform

Upload the webapp to the SoC root filesystem and run `./app.py` with root privileges. (BitBake recipe is to be added). Navigate browser to \<SoC IP\>:80 to begin.

Start `iperf3 -s` server on a machine in the same subnet as SoC to facilitate Ethernet performance tests.

Start Vivado remote access server on a machine connected to the SoC's JTAG (see `vivado` folder) to facilitate IBERT and DDRMC tests.

## On the development machine

Activate `venv` and install the required python packages:
```
python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt
```

Start the webapp by running `./app.py` with normal user privileges with `venv` activated. Navigate browser to 127.0.0.1:8080 to begin.

# REST API

The SoC peripherals can also be accessed remotely via REST API.

(API documentation is TBD.)

# CI tests

The webapp has self-tests defined in `tests` folder. Tests run in `python:3.8.12-buster` container whenever an update is pushed to the repository.

# Support

Please contact Elena Zhivun <elena.zhivun@cern.ch> if you have any questions.