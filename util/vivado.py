'''
Python interface to Vivado Hardware Manager
with thread-safe class implementation.

Tested with Vivado Hardware Manager 2021.1.1 and Python 3.8.10

Before running, open Hardware Manager, open the target,
refresh the devices and run in Tcl Console: 
source <project directory>/vivado/remote.tcl

The Vivado class was developed to facilitate testing of
custom hardware based on the Versal chip.
The class implements methods needed for getting and setting
cell properties, retrieving DDRMC test results, and
performing IBERT eye scans.

To run ay other TCL command on Vivado and retrieve the 
result, use query() method.

'''

import re, pdb, atexit
from threading import Lock, Thread
from functools import wraps

from asyncio import open_connection, wait_for, new_event_loop
from asyncio import run_coroutine_threadsafe


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "elena.zhivun@cern.ch"


ready_msg = "!!!READY!!!"
exception_msg = "!!!EXCEPTION!!!"
tmp_file_name = "tmp_file.csv"

# Selects name of the cell from the full path
name_regex = re.compile("(?P<name>[^/]+$)")

# Parses the output of report_property command
prop_regex = re.compile("^(?P<name>\\S+)[^\\S\r\n]+(?P<type>enum|string|bool|int)[^\\S\r\n]+(?P<read_only>true|false)[^\\S\r\n]+(?P<value>.+)$")


class VivadoException(Exception):
	pass


class TCL_Exception(VivadoException):
	pass


class LockException(Exception):
	pass


class Vivado():
	def __init__(self, config):
		self.config = config
		self.timeout = config["timeout"]
		self.device = config["device"]	
		self.network_lock = Lock()
		self.loop = new_event_loop()
		self.network_thread = Thread(target=self.loop.run_forever, daemon=True)
		self.network_thread.start()
		self.connected = False

		self._add_convenience_methods()
		atexit.register(self._cleanup)
		

	def connect(self, server="127.0.0.1", port=8020):
		'''
			Open the connection to Vivado. 
			Blocks execution until connected.
			Throws asyncio.TimeoutError if timed out.
			Throws asyncio.ConnectionRefusedError if refused connection
		'''
		run_coroutine_threadsafe(self._open(server=self.config["server"],
			port=self.config["port"]), self.loop).result(timeout=self.timeout)


	def disconnect(self):
		'''
			Close the connection to Vivado. 
		'''
		run_coroutine_threadsafe(self._close(), self.loop).result(timeout=self.timeout)


	def query(self, command, strip=False, timeout=None):
		'''
			Run a Vivado command and return the result.
			The result is an array of lines returned by Vivado

			@param strip if True, joins all lines, and strips the resulting string
		'''
		timeout = timeout or self.timeout
		with self.network_lock:
			result = run_coroutine_threadsafe(self._send_recv(
				command, timeout=timeout), self.loop).result(timeout=timeout)
			if strip:
				return "".join(result).strip()
			else:
				return result


	def get_property(self, name, cell, getter="get_hw_devices"):
		query = "get_property {{{}}} [{} {{{}}}]".format(name, getter, cell)
		return self.query(query, strip=True)


	def get_properties(self, properties, cell, getter="get_hw_devices"):
		result = {}
		for name in properties:
			result["name"] = self.get_property(name=name, cell=cell, getter=getter)
		return result


	def set_property(self, name, value, cell, getter="get_hw_devices", commit=True):
		query = "set_property {{{}}} {{{}}} [{} {{{}}}]".format(name, value, getter, cell)
		self.query(query)

		if getter == "get_hw_sio_links" and commit:
			self.commit_hw_sio(cell)


	def set_properties(self, properties, cell, getter="get_hw_devices"):
		for name, value in properties.items():
			self.set_property(name=name, value=value, cell=cell,
				getter=getter, commit=False)

		if getter == "get_hw_sio_links":
			self.commit_hw_sio(cell)


	def commit_hw_sio(self, cell):
		self.query("commit_hw_sio [get_hw_sio_links {{{}}}]".format(cell))


	def report_property(self, cell, getter="get_hw_devices", **kwargs):
		'''
			Returns all properties of a Vivado object as a dictionary

			@param cell cell name
			@param getter command to get the cell
		'''
		query = "report_property -verbose -return_string [{} {{{}}}]".format(getter, cell)
		lines = self.query(query, strip=False)
		result = {}
		for line in lines:
			match = prop_regex.match(line)
			if match:
				value_type = match.group("type")
				if value_type == "bool":
					value = match.group("value") != "0"
				elif value_type == "int":
					value = int(match.group("value"), 10)
				else:
					value = match.group("value")
				name = match.group("name")
				result[name] = value
		return result


	def report_hw_ddrmc(self, cell):
		return self.query(("report_hw_ddrmc -return_string -verbose "
			+ "[get_hw_ddrmcs {{{}}}]").format(cell), strip=False)


	def create_hw_sio_link(self, name, tx, rx):		
		cell_name = self.query(("create_hw_sio_link -description {{{}}} [get_hw_sio_txs {{{}}}] "
			+ "[get_hw_sio_rxs {{{}}}]").format(name, tx, rx), strip=True)
		return SerialLink(vivado=self, name=cell_name)


	def remove_hw_sio_link(self, cell):
		self.query(("remove_hw_sio_link [get_hw_sio_links {{{}}}]").format(cell))


	def remove_all_hw_sio_links(self):
		self.query("remove_hw_sio_link [get_hw_sio_links]")


	def create_hw_sio_scan(self, name, link):		
		scan = self.query(("create_hw_sio_scan -description {{{}}} 2d_full_eye "
			+ "[get_hw_sio_links {{{}}}]").format(name, link), strip=True)
		return SerialScan(vivado=self, name=scan)


	def remove_hw_sio_scan(self, cell):
		self.query(("remove_hw_sio_scan [get_hw_sio_scans {{{}}}]").format(cell))


	def remove_all_hw_sio_scans(self):
		self.query("remove_hw_sio_scan -quiet [get_hw_sio_scans]")


	def run_hw_sio_scan(self, cell):
		self.query(("run_hw_sio_scan [get_hw_sio_scans {{{}}}]").format(cell))


	def read_hw_sio_scan(self, cell):
		self.query("write_hw_sio_scan -force {{{}}} [get_hw_sio_scans {{{}}}]".format(
			tmp_file_name, cell))
		return self.query("return_file_data {{{}}}".format(tmp_file_name), strip=False)


	def get_hw_cells(self, getter, mask=None):
		CellClass = cell_types[getter]["class"]
		if mask is None:
			cells = self.query("{}".format(getter), strip=True)
		else:
			cells = self.query("{} {{{}}}".format(getter, mask), strip=True)
		return [CellClass(vivado=self, name=cell, getter=getter) for cell in cells.split()]


	# ---------- private methods -------------


	async def _open(self, server="127.0.0.1", port=8020):		
		with self.network_lock:
			if self.connected:
				return

			self.reader, self.writer = await wait_for(open_connection(server, port),
				timeout=self.timeout)
			self.connected = True


	async def _close(self):
		with self.network_lock:
			if not self.connected:
				return

			writer = self.writer
			writer.write_eof()
			await writer.drain()
			writer.close()
			await writer.wait_closed()
			self.connected = False


	async def _recv(self, timeout=None):
		if not self.network_lock.locked():
			raise LockException("Network lock must be held when calling _recv()")

		if not self.connected:
			raise ConnectionError("Vivado Remote HW Manager is not connected")

		# Read complete response from Vivado
		result = []
		ready = False
		error = False
		timeout = timeout or self.timeout

		try:
			while not ready:
				data = await wait_for(self.reader.readline(), timeout=timeout)
				data = data.decode().strip()			
				if data == ready_msg:
					ready = True
				else:				
					if exception_msg in data:
						error = True
						result = [data]
					else:
						result.append(data)
					
			if error:
				message = '\n'.join(result).replace(exception_msg, "")		
				raise TCL_Exception(message)
			else:
				return result

		except ConnectionError as err:
			self.connected = False
			raise err


	async def _send(self, command, drain=True, timeout=None):
		"""
			Send a command to Vivado.

			@param command command string.
			@param drain wait for the sending to be complete (default)
		"""
		if not self.network_lock.locked():
			raise LockException("Network lock must be held when calling _send()")

		if not self.connected:
			raise ConnectionError("Vivado Remote HW Manager is not connected")

		try:
			timeout = timeout or self.timeout
			self.writer.write((command + "\n").encode())
			self.writer.write(("place_ready_marker\n").encode())
			if drain:
				await wait_for(self.writer.drain(), timeout)
		except ConnectionError as err:
			self.connected = False
			raise err


	async def _send_recv(self, command, timeout=None):
		if not self.network_lock.locked():
			raise LockException("Network lock must be held when calling _send_recv()")

		await self._send(command, timeout=timeout)
		return await self._recv(timeout=timeout)


	def _cleanup(self):
		self.disconnect()
		self.loop.call_soon_threadsafe(self.loop.stop)
		self.network_thread.join()


	def __repr__(self):
		return "<Vivado HW Manager at {}:{}, connected={}>".format(
			self.config["server"], self.config["port"], self.connected)


	def _add_convenience_methods(self):
		# generate methods for getting/setting properties
		# for different cell types		

		functions = {
			# "get_{}_property" : self.get_property,
			# "get_{}_properties" : self.get_properties,
			# "set_{}_property" : self.set_property,
			# "set_{}_properties" : self.set_properties,
			# "report_{}_property" : self.report_property,
			"get_hw_{}s" : self.get_hw_cells,
		}		

		def decorator(func, getter):
			@wraps(func)
			def wrapper(*args, **kwargs):
				return func(*args, **kwargs, getter=getter)
			return wrapper

		for getter, cell_type in cell_types.items():
			for pattern, target in functions.items():
				setattr(self, pattern.format(cell_type["name"]), decorator(target, getter))


class Cell():
	'''
		Representation of the object in Vivado cell tree.
		Implements settings and getting properties.
	'''
	def __init__(self, vivado, name, getter, **kwargs):
		self.vivado = vivado
		self.name = name
		self.getter = getter
		self.type = cell_types[getter]["name"]

	def get_property(self, *args, **kwargs):
		return self.vivado.get_property(*args, **kwargs, cell=self.name, getter=self.getter)

	def get_properties(self, *args, **kwargs):
		return self.vivado.get_properties(*args, **kwargs, cell=self.name, getter=self.getter)

	def set_property(self, *args, **kwargs):
		return self.vivado.set_property(*args, **kwargs, cell=self.name, getter=self.getter)

	def set_properties(self, *args, **kwargs):
		return self.vivado.set_properties(*args, **kwargs, cell=self.name, getter=self.getter)

	def report_property(self, *args, **kwargs):
		return self.vivado.report_property(*args, **kwargs, cell=self.name, getter=self.getter)

	def report_properties(self, *args, **kwargs):
		return self.report_property(*args, **kwargs)

	def properties(self, *args, **kwargs):
		return self.report_property(*args, **kwargs)

	def list_property(self, *args, **kwargs):
		data = self.vivado.query("list_property [{} {{{}}}]".format(self.getter, self.name), strip=True)
		return data.split()

	def __str__(self):
		return self.name

	def __format__(self, format_spec):
		return self.__str__()

	def __hash__(self):
		return ("{}.{}.{}".format(id(self.vivado), self.getter, self.name)).__hash__()

	def __eq__(self, other):
		if not isinstance(other, self.__class__):
			return False
		return self.vivado == other.vivado and self.getter == other.getter and self.name == other.name

	def __repr__(self):
		return "<Vivado HW Cell {}, type={}>".format(self.name, self.type)


class SerialLink(Cell):
	'''
		Represents Vivado transceiver link
	'''

	# Retrieves channel prefix and name from property name
	prefix_regex = re.compile("^(?P<channel>CH\\d+_)(?P<name>.+)")


	def __init__(self, *args, **kwargs):
		kwargs["getter"] = "get_hw_sio_links"
		super().__init__(*args, **kwargs)
		self._build_translate_dict()


	def _build_translate_dict(self):
		property_names = self.list_property()
		self.translate_dict = {}
		self.reverse_translate_dict = {}
		for prefixed_name in property_names:
			match = self.prefix_regex.match(prefixed_name)
			if match:
				name = match.group("name")
				self.translate_dict[name] = prefixed_name
				self.reverse_translate_dict[prefixed_name] = name

		self.property_names = property_names
		


	def _translate_name(self, name):
		'''
			Some link properties include channel name prefix, resulting in inconsistent
			parameter names for different links. This function returns the parameter
			name with prefix given the parameter name without prefix for this particular link.

			For example: 
				input: TX_DIFF_CTRL, output: CH0_TX_DIFF_CTRL
				input: RX_STATUS, output: CH3_RX_STATUS

		'''
		if (name in self.property_names) or (not name in self.translate_dict):
			return name

		return self.translate_dict[name]


	def _translate_properties(self, properties):
		'''
			Translates a dictionary of properties, adding channel prefix as needed
		'''
		result = {}
		for name, value in properties.items():
			result[self._translate_name(name)] = value
		return result


	def _translate_args(self, args):
		arg_list = list(args)
		if isinstance(arg_list[0], dict):
			arg_list[0] = self._translate_properties(arg_list[0])
		else:
			arg_list[0] = self._translate_name(arg_list[0])
		return tuple(arg_list)


	def _reverse_translate(self, report):
		result = {}
		for original_name in report.keys():
			if original_name in self.reverse_translate_dict:
				name = self.reverse_translate_dict[original_name]
				result[name] = report[original_name]
			else:
				result[original_name] = report[original_name]
		return result


	def get_property(self, *args, **kwargs):
		return super().get_property(*self._translate_args(args), **kwargs)


	def get_properties(self, *args, **kwargs):
		return super().get_properties(*self._translate_args(args), **kwargs)


	def set_property(self, *args, **kwargs):
		return super().set_property(*self._translate_args(args), **kwargs)


	def set_properties(self, *args, **kwargs):
		return super().set_properties(*self._translate_args(args), **kwargs)


	def report_property(self, *args, **kwargs):
		translate = kwargs.get("strip_channel_prefix", False)
		result = super().report_property(*args, **kwargs)

		if translate:
			return self._reverse_translate(result)
		else:
			return result


	def report_properties(self, *args, **kwargs):
		return self.report_property(*args, **kwargs)

	def properties(self, *args, **kwargs):
		return self.report_property(*args, **kwargs)


	def remove(self, *args, **kwargs):
		return self.vivado.remove_hw_sio_link(*args, **kwargs, cell=self.name)


	def ibert_reset(self):
		self.set_property("RX_PRBS_RESET.BER", 1)
		self.set_property("RX_PRBS_RESET.BER", 0)


	def tx_reset(self):
		self.set_property("TX_RESET", 1)
		self.set_property("TX_RESET", 0)


	def rx_reset(self):
		self.set_property("RX_RESET", 1)
		self.set_property("RX_RESET", 0)


class SerialScan(Cell):
	'''
		Represents Vivado transceiver scan
	'''
	def __init__(self, *args, **kwargs):
		kwargs["getter"] = "get_hw_sio_scans"
		super().__init__(*args, **kwargs)

	def remove(self, *args, **kwargs):
		return self.vivado.remove_hw_sio_scan(*args, **kwargs, cell=self.name)

	def run(self, *args, **kwargs):
		return self.vivado.run_hw_sio_scan(*args, **kwargs, cell=self.name)

	def data(self, *args, **kwargs):
		'''
			Retrieve and parse scan data
		'''
		data = self.vivado.read_hw_sio_scan(*args, **kwargs, cell=self.name)
		result = {}
		scan_data = []
		voltage_codes = []
		scan_start = False
		statistical = "2d statistical"
		for line in data:
			line = line.strip()
			if line.lower() == "scan start":
				scan_start = True
			elif line.lower() == "scan end":
				break
			elif scan_start:
				line_data = line.split(",")
				if line_data[0] == statistical:
					try:
						result[statistical] = [float(x) for x in line_data[1:]]
					except Exception:
						result[statistical] = []
				else:
					try:
						voltage_codes.append(float(line_data[0]))
						scan_data.append([float(x) for x in line_data[1:]])
					except Exception as err:
						pass
			else:
				name, value = line.split(",")
				result[name] = value
		result["scan_data"] = scan_data
		result["voltage_codes"] = voltage_codes
		return result


	def done(self):
		props = self.properties()
		return props["DATA_READY"] and props["STATUS"] == "Done"

	def wait_until_done(self, timeout=None):
		if timeout == None:
			return self.vivado.query("wait_on_hw_sio_scan [get_hw_sio_scans {{{}}}]".format(self.name))
		else:
			return self.vivado.query("wait_on_hw_sio_scan -timeout {} [get_hw_sio_scans {{{}}}]".format(
				timeout, self.name), timeout=timeout)


class DDRMC(Cell):
	'''
		Represents DDRMC on a Xilinx device
	'''
	def __init__(self, *args, **kwargs):
		kwargs["getter"] = "get_hw_ddrmcs"
		super().__init__(*args, **kwargs)

	def report(self, *args, **kwargs):
		data = self.vivado.report_hw_ddrmc(*args, **kwargs, cell=self.name)
		return "\n".join(data)


class Device(Cell):
	'''
		Represents DDRMC on a Xilinx device
	'''
	def __init__(self, *args, **kwargs):
		kwargs["getter"] = "get_hw_devices"
		super().__init__(*args, **kwargs)

	def refresh(self, *args, **kwargs):
		return self.vivado.query("refresh_hw_device [get_hw_devices {{{}}}]".format(self.name))


cell_types = {
	"get_hw_sio_links"	: {
		"name" : "sio_link",
		"class" : SerialLink
	},

	"get_hw_sio_scans"	: {
		"name" : "sio_scan",
		"class" : SerialScan
	},		

	"get_hw_sio_iberts"	: {
		"name" : "sio_ibert",
		"class" : Cell
	},

	"get_hw_sio_plls"	: {
		"name" : "sio_pll",
		"class" : Cell
	},

	"get_hw_sio_gts"	: {
		"name" : "sio_gt",
		"class" : Cell
	},

	"get_hw_sio_rxs"	: {
		"name" : "sio_rx",
		"class" : Cell
	},

	"get_hw_sio_txs"	: {
		"name" : "sio_tx",
		"class" : Cell
	},

	"get_hw_sio_gtgroups"	: {
		"name" : "sio_gtgroup",
		"class" : Cell
	},

	"get_hw_sio_linkgroups"	: {
		"name" : "sio_linkgroup",
		"class" : Cell
	},

	"get_hw_sio_sweeps"	: {
		"name" : "sio_sweep",
		"class" : Cell
	},

	"get_hw_ddrmcs"	: {
		"name" : "ddrmc",
		"class" : DDRMC
	},

	"get_hw_pcies"		: {
		"name" : "pcie",
		"class" : Cell
	},

	"get_hw_sysmons"	: {
		"name" : "sysmon",
		"class" : Cell
	},

	"get_hw_targets"	: {
		"name" : "target",
		"class" : Cell
	},

	"get_hw_servers"	: {
		"name" : "server",
		"class" : Cell
	},

	"get_hw_devices"	: {
		"name" : "device",
		"class" : Device
	},

	"get_hw_axis"		: {
		"name" : "axi",
		"class" : Cell
	},

	"get_hw_vios"		: {
		"name" : "vio",
		"class" : Cell
	},

	"get_hw_ilas"		: {
		"name" : "ila",
		"class" : Cell
	},

	"get_hw_ila_datas"	: {
		"name" : "ila_data",
		"class" : Cell
	},

	"get_hw_probes"	: {
		"name" : "probe",
		"class" : Cell
	},

	"get_hw_cfgmems"	: {
		"name" : "cfgmem",
		"class" : Cell
	},

	"get_hw_hbms"	: {
		"name" : "hbm",
		"class" : Cell
	},

	"get_hw_migs"	: {
		"name" : "mig",
		"class" : Cell
	}
}

