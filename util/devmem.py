import os, mmap

def devmem_read(target_adr, length=4):
    page_size= os.sysconf("SC_PAGE_SIZE")
    reg_base = int(target_adr // page_size) * page_size
    seek_size= int(target_adr %  page_size)
    map_size = seek_size+length
    fd = os.open("/dev/mem", os.O_RDWR|os.O_SYNC)
    mem = mmap.mmap(fd, map_size, mmap.MAP_SHARED, mmap.PROT_READ|mmap.PROT_WRITE, offset=reg_base)
    mem.seek(seek_size, os.SEEK_SET)
    value = mem.read(length)
    os.close(fd)
    return value

def devmem_write(target_adr, value):
    page_size= os.sysconf("SC_PAGE_SIZE")
    reg_base = int(target_adr // page_size) * page_size
    seek_size= int(target_adr %  page_size)
    map_size = seek_size+len(value)
    fd = os.open("/dev/mem", os.O_RDWR|os.O_SYNC)
    mem = mmap.mmap(fd, map_size, mmap.MAP_SHARED, mmap.PROT_READ|mmap.PROT_WRITE, offset=reg_base)
    mem.seek(seek_size, os.SEEK_SET)
    value = mem.write(value)
    os.close(fd)
