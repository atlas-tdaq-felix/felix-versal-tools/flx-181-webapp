import pdb
from util.i2c import I2C, I2CError
from util.gpio import set_line, GPIOError
from contextlib import contextmanager
from unittest.mock import Mock


transceiver_types = {
    0x00: 'Unknown or unspecified',
    0x01: 'GBIC',
    0x02: 'Module/connector soldered to motherboard',
    0x03: 'SFP/SFP+/SFP28',
    0x04: '300 pin XBI',
    0x05: 'XENPAK',
    0x06: 'XFP',
    0x07: 'XFF',
    0x08: 'XFP-E',
    0x09: 'XPAK',
    0x0a: 'X2',
    0x0b: 'DWDM-SFP/SFP+',
    0x0c: 'QSFP',
    0x0d: 'QSFP+ or later',
    0x0e: 'CXP or later',
    0x0f: 'Shielded Mini Multilane HD 4X',
    0x10: 'Shielded Mini Multilane HD 8X',
    0x11: 'QSFP28 or later',
    0x12: 'CXP2 (aka CXP28) or later',
    0x13: 'CDFP (Style 1/Style2)',
    0x14: 'Shielded Mini Multilane HD 4X Fanout Cable',
    0x15: 'Shielded Mini Multilane HD 8X Fanout Cable',
    0x16: 'CDFP (Style 3)',
    0x17: 'microQSFP',
    0x18: 'QSFP-DD Double Density 8X Pluggable Transceiver',
    0x19: 'OSFP 8X Pluggable Transceiver',
    0x81: 'SAMTEC FireFly ECUO 25G/28G Transceiver',
    0x1a: 'SFP-DD Double Density 2X Pluggable Transceiver',
}


reg_qsfp = {
    "part_number" : range(168, 183 + 1),
    "vendor" : range(148, 163 + 1),
    "serial" : range(196, 211 + 1),
    # In SFP standard the temperature is represented as signed
    # two-complement 16-bit number with increments of 1/256 deg C
    "temperature_monitor" : {
        "address" : [22, 23],
        "type" : "sfp"
    },
}

regs_samtec_cern = {
        "vendor" : range(0x98, 0xA1 + 1),
        "part_number" : range(0xAB, 0xBA + 1),
        "serial" : range(0xBD, 0xC6 + 1), 
        # In SAMTEC FireFly modules, the temperature is represented
        # as signed two-complement 8-bit number with 1 deg C increments
        "temperature_monitor" : {
            "address" : 22,
            "type" : "samtec"
        },
    }

register_maps = {
    0x03 : {
        "part_number" : range(40, 59 + 1),
        "vendor" : range(20, 35 + 1),
        "serial" : range(68, 83 + 1),
        "temperature_monitor" : {
            "address" : [96, 97],
            "type"    : "sfp"
        },
    },

    0x0c : reg_qsfp,
    0x0d : reg_qsfp,

    0x81 : {
        "vendor" : range(148, 163 + 1),
        "part_number" : range(168, 180 + 1),
        "serial" : range(196, 205 + 1),
        "temperature_monitor" : {
            "address" : 22,
            "type" : "samtec"
        },
        "cdr_enable" : 98,
        "cdr_rate" : 99,
    },    

    "CERN-B-* Receiver" : regs_samtec_cern,
    "CERN-B-* Transmitter" : regs_samtec_cern,
}



# The registers are different for different transceiver module types

# reg1 = {
#     "temp_hi_alarm" : [0, 1],
#     "temp_lo_alarm" : [2, 3],
#     "temp_hi_warning" : [4, 5],
#     "temp_lo_warning" : [6, 7],
#     "vcc_hi_alarm" : [8, 9],
#     "vcc_lo_alarm" : [10, 11],
#     "vcc_hi_warning" : [12, 13],
#     "vcc_lo_warning" : [14, 15],
#     "tx_bias_hi_alarm" : [16, 17],
#     "tx_bias_lo_alarm" : [18, 19],
#     "tx_bias_hi_warning" : [20, 21],
#     "tx_bias_lo_warning" : [22, 23],
#     "tx_pwr_hi_alarm" : [24, 25],
#     "tx_pwr_lo_alarm" : [26, 27],
#     "tx_pwr_hi_warning" : [28, 29],
#     "tx_pwr_lo_warning" : [30, 31],
#     "rx_pwr_hi_alarm" : [32, 33],
#     "rx_pwr_lo_alarm" : [34, 35],
#     "rx_pwr_hi_warning" : [36, 37],
#     "rx_pwr_lo_warning" : [38, 39],
# }




# device class responsible for talking to a pluggable device
class SFP():
    def __init__(self, device):
        self.device = device
        if "select" in device:
            self.select = device["select"]


    def read_string(self, reg_addr, length):
        i2c_addr = self.device["i2c_addr"]
        result = ""
        for i in range(0, length):
            char = self.i2c.read(i2c_addr, reg_addr + i)
            if char != 0:
                result += chr(char)
        return result.strip()


    def read_string_range(self, reg_range):
        return self.read_string(reg_range.start, len(reg_range))


    def __read_temperature(self):
        if not "temperature_monitor" in self.registers:
            return float("NaN")

        monitor = self.registers["temperature_monitor"]
        i2c_addr = self.device["i2c_addr"]
        if monitor["type"] == "samtec":
            temp_raw = int.to_bytes(self.i2c.read(i2c_addr, monitor["address"]), 1, 'big')
            temperature = float(int.from_bytes(temp_raw, 'big', signed=True))
        elif monitor["type"] == "sfp":
            temp_msb = int.to_bytes(self.i2c.read(i2c_addr, monitor["address"][0]), 1, 'big')
            temp_lsb = int.to_bytes(self.i2c.read(i2c_addr, monitor["address"][1]), 1, 'big')
            temperature = float(int.from_bytes(temp_msb + temp_lsb, 'big', signed=True) / 256.0)
        else:
            raise ValueError(f"Unsupported temperature monitor type: {monitor['type']}")
        return temperature


    def __read_field(self, field):
        if not field in self.registers:
            return None
        return self.read_string_range(self.registers[field])


    def __xcvr_open(self):
        if "select" in self.device:
            set_line(device=self.device["select"]["gpio"],
                line=self.device["select"]["line"],
                value=self.device["select"]["active_level"])

        self.i2c = I2C()
        self.i2c.open(self.device["i2c_bus"])

        # determine pluggable transceiver type
        self.type_regval = self.i2c.read(self.device["i2c_addr"], 0)
        if self.type_regval == 0 and "type" in self.device:
            self.type = self.device["type"]            
        else:
            self.type = self.type_regval        
        self.registers = register_maps[self.type] if self.type in register_maps else None


    def __xcvr_close(self):
        if hasattr(self, 'i2c'):
            self.i2c.close()

        if "select" in self.device:
            set_line(device=self.device["select"]["gpio"],
                    line=self.device["select"]["line"],
                    value=not self.device["select"]["active_level"])


    @contextmanager
    def open_device(self):
        try:
            self.__xcvr_open()
            yield
        finally:
            self.__xcvr_close()


    def read(self):
        data = {}        

        try:
            with self.open_device():
                if "type" in self.device:
                    data["type"] = self.device["type"]
                elif self.type_regval in transceiver_types:
                    data["type"] = transceiver_types[self.type_regval]
                else:
                    data["type"] = "(unsupported)"

                if self.registers is None:
                    data.update({                    
                        "part_number" : "(unsupported)",
                        "vendor" : "(unsupported)",
                        "temperature" : float("NaN"),
                        "serial" : "(unsupported)"}) 
                else:     
                    data["part_number"] = self.__read_field("part_number")
                    data["vendor"] = self.__read_field("vendor")
                    data["serial"] = self.__read_field("serial")
                    data["temperature"] = self.__read_temperature()

                return data

        except (GPIOError, I2CError) as err:  
            data = {
                "type" : "N/A",
                "part_number" : "N/A",
                "vendor" : "N/A",
                "serial" : "N/A",
                "temperature" : float("NaN"),
                "error" : str(err)
            } 
            return data


    def get_part_number(self):
        with self.open_device():
            return self.__read_field("part_number")

    
    def configure(self, config):
        with self.open_device():
            part_number = self.__read_field("part_number")

            # Only applies to the 28G transceivers
            if part_number == "B042804005170":
                cdr = 0
                rate = 0

                if('cdr' in config):
                    cdr = 0xFF
                
                if(config['rate'] == '25g'):
                    rate = 0
                else:
                    rate = 0xFF
            
                i2c_addr = self.device["i2c_addr"]
                self.i2c.write(i2c_addr, self.registers["cdr_enable"], cdr)
                self.i2c.write(i2c_addr, self.registers["cdr_rate"], rate)


class TransactionValidator():
    '''
        Verify that the modesel_l is driven correctly during the 
        I2C communication with the 
    '''

    class I2C():
        '''
            Wrapper that verifies I2C transaction is done in the correct order
        '''
        def __init__(self, sfp, delegate):
            self.delegate = delegate
            self.sfp = sfp
            self.is_open = False
            self.bytes_read = 0
            self.bytes_written = 0


        def open(self, *args, **kwargs):
            if not self.sfp.line_selected:
                self.sfp.errors.append("i2c.open was called without asserting MODSEL")

            if self.is_open:
                self.sfp.errors.append("i2c.open was called when i2c is already open")

            self.is_open = True
            return self.delegate.open(*args, **kwargs)
        

        def close(self, *args, **kwargs):
            # closing a closed i2c object is allowed
            if not self.sfp.line_selected:
                self.sfp.errors.append("i2c.close was called without asserting MODSEL")

            self.is_open = False
            return self.delegate.close(*args, **kwargs)


        def read(self, *args, **kwargs):            
            if not self.sfp.line_selected:
                self.sfp.errors.append("i2c.read was called without asserting MODSEL")

            if not self.is_open:
                self.sfp.errors.append("i2c.read was called before i2c.open")

            self.bytes_read += 1
            return self.delegate.read(*args, **kwargs)


        def write(self, *args, **kwargs):
            if not self.sfp.line_selected:
                self.sfp.errors.append("i2c.write was called without asserting MODSEL")

            if not self.is_open:
                self.sfp.errors.append("i2c.write was called before i2c.open")
                
            self.bytes_written += 1
            return self.delegate.write(*args, **kwargs)


    def raise_error(err):
        raise err


    def __init__(self, config, i2c, set_line=None):
        self.config = config
        self.i2c_delegate = i2c
        if set_line is None:
            self.set_line_delegate = lambda : self.raise_error(Exception("set_line delegate is not provided"))
        else:
            self.set_line_delegate = set_line

        self.controls_modesel = not set_line is None        
        self.line_selected = set_line is None        
        self.errors = []
        self.i2c = self.I2C(delegate=i2c, sfp=self)


    def i2c_generator(self):
        return self.i2c


    def gpio_set_line(self, *args, **kwargs):
        self.line_selected = kwargs['value'] == self.config["select"]["active_level"]
        self.set_line_delegate(*args, **kwargs)


    def transaction_valid(self):
        if self.errors:
            # there were general errors during transactions
            return False

        if self.i2c.bytes_read == 0 and self.i2c.bytes_written == 0:
            # there were no read or write operations
            return False

        if self.i2c.is_open:
            # I2C line was not closed after the transaction
            return False

        if self.controls_modesel and self.line_selected:
            # modesel was not released after the transaction
            return False

        return True



