from os import path, listdir
import json

# read file and return contents as a string
def load_file(file_name):
	with open(file_name, 'r') as file:
		return file.read().strip()


# read json file
def load_json(file_name):
	with open(file_name, 'r') as file:
		return json.load(file)


# Return list of all matching IIO devices by name as device paths
def get_devices(device_path, name):
	if not path.isdir(device_path):
		return []
	
	result = []
	devices = listdir(device_path)

	for device in devices:		
		device_name = load_file(path.join(device_path, device, "name"))
		if name in device_name:
			result.append(path.join(device_path, device))	

	return result