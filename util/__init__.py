# Various utility functions


# read file and return contents as a string
def load_file(file_name):
	with open(file_name, 'r') as file:
		return file.read().strip()