import sys, pdb
import os, re
import pprint
from threading import Lock
from flask import Flask, render_template, request, redirect, url_for
from flask import session as flask_session
from chipscopy import create_session, delete_session

from flask_wtf import FlaskForm
from wtforms import validators, BooleanField, StringField, SelectField
from wtforms import PasswordField, IntegerField, SubmitField, HiddenField
from werkzeug.datastructures import ImmutableMultiDict

addr_regex = re.compile("^(?P<protocol>\\S+):(?P<address>\\S+):(?P<port>\\d+)$")


class ChipscopyError(Exception):
	pass


class ChipscopyWrapper():
	def __init__(self, config):
		self.config = config
		if not ("cs_server_url" in config and "hw_server_url" in config
			and "device_family" in config and "bypass_version_check" in config):
			raise RuntimeError("Missing chipscopy settings, please fix your board configuration!")
		self.session = None
		self.device = None
		self.lock = Lock()


	def connect(self):
		with self.lock:
			session = create_session(cs_server_url=self.config["cs_server_url"],
				hw_server_url=self.config["hw_server_url"],
				bypass_version_check=self.config["bypass_version_check"])			
			device_family = self.config["device_family"]

			try:			
				device = session.devices.filter_by(family=device_family).get()
			except Exception as err:
				delete_session(session)
				raise ChipscopyError(f"Hardware Manager can't find any {device_family} devices")

			try:
				device.discover_and_setup_cores(ibert_scan=True)
			except Exception as err:
				delete_session(session)
				raise ChipscopyError(f"Can't set up debug cores for the {device_family} device")

			try:
				ibert = device.ibert_cores.at(index=0)
			except Exception as err:
				ibert = None			

			self.session = session
			self.device = device
			self.ibert = ibert


	def disconnect(self):
		with self.lock:
			self.device = None
			session = self.session
			self.session = None
			delete_session(session)


	def connected(self):
		return not self.session is None



class ChipscopyController():
	def __init__(self, chipscopy_wrapper):
		self.chipscopy = chipscopy_wrapper
		self.name = "chipscopy"


	def read_form(self, form):
		bypass_version_check = bool(form.bypass_version_check.data)
		cs_ip = form.cs_server_ip.data
		hw_ip = form.hw_server_ip.data
		protocol = form.protocol.data
		cs_port = form.cs_server_port.data
		hw_port = form.hw_server_port.data
		self.chipscopy.config["bypass_version_check"] = bypass_version_check
		self.chipscopy.config["cs_server_url"] = f'{protocol}:{cs_ip}:{cs_port}'
		self.chipscopy.config["hw_server_url"] = f'{protocol}:{hw_ip}:{hw_port}'
		self.chipscopy.config["device_family"] = form.family.data
		

	def get_form_from_session(self):
		if self.name in flask_session:
			return ChipscopyForm(ImmutableMultiDict(flask_session[self.name]))
		else:			
			return ChipscopyForm.make(self.chipscopy.config)


	def connect(self):		
		connected = self.chipscopy.connected()

		if request.method == "POST":
			form = ChipscopyForm(request.form)
			flask_session[self.name] = request.form
			if not form.validate():
				return render_template("chipscopy_connect.html", form=form,
					error="Invalid form arguments", connected=connected)
		
			if connected:
				return render_template("chipscopy_connect.html", form=form,
					error="Chipscopy is already connected!", connected=connected)
			
			self.read_form(form)
			try:
				self.chipscopy.connect()
				return redirect(form.redirect.data)
			except Exception as err:				
				return render_template("chipscopy_connect.html", form=form,
					error=str(err), connected=connected)


		else:
			form = self.get_form_from_session()

			if self.chipscopy.connected() and self.chipscopy.ibert is None:
				warning = "Chipscopy is connected, but IBERT cores are not found in the design!"
			else:
				warning = ""

			if self.chipscopy.connected():
				cs_server = self.chipscopy.session._cs_server_url
				hw_server = self.chipscopy.session._hw_server_url
				status = f"Connected, cs_server_url={cs_server}, hw_server_url={hw_server}"
			else:
				status = "Disconnected"

			return render_template("chipscopy_connect.html", form=form, error="", warning=warning,
				status=status, connected=connected)


	def disconnect(self):
		connected = self.chipscopy.connected()
		form = self.get_form_from_session()		
		destination = "/chipscopy/connect"

		if not connected:
			form.redirect.data = destination
			return render_template("chipscopy_connect.html", form=form, error="")

		self.chipscopy.disconnect()
		return redirect(destination)




class ChipscopyForm(FlaskForm):
	cs_server_ip = StringField("Chipscopy server IP", [validators.IPAddress()])
	cs_server_port = IntegerField("Chipscopy server port", [validators.NumberRange(min=0, max=65535)])
	hw_server_ip = StringField("Vivado hardware server IP", [validators.IPAddress()])
	hw_server_port = IntegerField("Vivado hardware server port", [validators.NumberRange(min=0, max=65535)])
	bypass_version_check = BooleanField("Bypass chipscopy version check", default=True)
	protocol = HiddenField("Connection protocol", [validators.DataRequired()], default="TCP")
	redirect = HiddenField("Redirect URL", default="/")
	family = StringField("Xilinx device family", [validators.DataRequired()], default="versal")
	submit_button = SubmitField("Connect")


	def make(config):
		params = {}
		match = addr_regex.match(config["cs_server_url"])
		if match:
			params["cs_server_ip"] = match["address"]
			params["cs_server_port"] = match["port"]
			params["cs_server_protocol"] = match["protocol"]
		else:
			params["cs_server_ip"] = "127.0.0.1"
			params["cs_server_port"] = "3042"
			params["cs_server_protocol"] = "TCP"

		match = addr_regex.match(config["hw_server_url"])
		if match:
			params["hw_server_ip"] = match["address"]
			params["hw_server_port"] = match["port"]
			params["hw_server_protocol"] = match["protocol"]
		else:
			params["hw_server_ip"] = "127.0.0.1"
			params["hw_server_port"] = "3121"
			params["hw_server_protocol"] = "TCP"

		params["bypass_version_check"] = config["bypass_version_check"]
		params["redirect"] = request.args.get("redirect", "/chipscopy/connect")
		params["device_family"] = config["device_family"]

		return ChipscopyForm(ImmutableMultiDict(params))