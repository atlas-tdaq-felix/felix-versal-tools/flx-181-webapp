import pdb
from util.i2c import I2C_ADM106x, I2CError
from time import sleep
from pprint import pprint


# TODO: locks on the registers for each i2c bus+address combination
# TODO: reprogramming support
# TODO: this should ideally be turned into an hwmon driver


class RegisterManager():
	"""
		Reading ADM1066 registers and register blocks, caching data
	"""	

	# how large of a buffer to allocate for the register cache
	register_count = 0x100

	def __init__(self, i2c_bus, i2c_addr):
		self.i2c_addr = i2c_addr
		self.i2c_bus = i2c_bus
		self.registers = [0] * self.register_count


	def read(self, reg_id, use_cache=True):
		if use_cache:
			return self.registers[reg_id]

		with I2C_ADM106x.context(self.i2c_bus) as i2c:
			data = i2c.read(self.i2c_addr, reg_id)
			self.registers[reg_id] = data
			return data


	def write(self, reg_addr, reg_val):
		with I2C_ADM106x.context(self.i2c_bus) as i2c:
			i2c.write(self.i2c_addr, reg_addr, reg_val)


	def read_block(self, start, length):		
		with I2C_ADM106x.context(self.i2c_bus) as i2c:
			data = i2c.read_block(self.i2c_addr, start, length)			
			return data


	def refresh(self, start, length):
		data = self.read_block(start, length)
		for i in range(length):
			self.registers[i + start] = data[i]



class RegisterField():
	"""
		Mix-in for reading and writing registers by name
	"""
	def read_field(self, name, use_cache=True):
		return self.regs.read(self.fields[name], use_cache)


	def write_field(self, name, value):
		return self.regs.write(self.fields[name], value)



class VoltagePin(RegisterField):
	"""
		Reading out voltage pins
	"""
	base_offsets = {
		"vp1" : 0x00,
		"vp2" : 0x08,
		"vp3" : 0x10,
		"vp4" : 0x18,
		"vh"  : 0x20,
		"vx1" : 0x28,
		"vx2" : 0x30,
		"vx3" : 0x38,
		"vx4" : 0x40,
		"vx5" : 0x48,
	}	


	field_offsets = {
		"ov_th" : 0x00,
		"ov_hyst" : 0x01,
		"uv_th" : 0x02,
		"uv_hyst" : 0x03,
		"sfd_cfg" : 0x04,
		"sfd_sel" : 0x05,
	}


	delay_map = {
		0 : 0,
		1 : 5,
		2 : 10,
		3 : 20,
		4 : 30,
		5 : 50,
		6 : 75,
		7 : 100
	}


	def __init__(self, name, registers):
		base = self.base_offsets[name]
		self.fields = {}
		self.regs = registers
#		self.input_type = name[0:2]

		# calculate and store relevant register offsets
		for name, offset in self.field_offsets.items():
			self.fields[name] = base + offset		


	def read(self):
		result = {
			"ov_threshold_raw" : self.read_field("ov_th"),
			"ov_hysteresis_raw" : self.read_field("ov_hyst") & 0b11111,
			"uv_threshold_raw" : self.read_field("uv_th"),
			"uv_hysteresis_raw" : self.read_field("uv_hyst") & 0b11111,
		}

		# parse delay field
		sfd_cfg = self.read_field("sfd_cfg")
		rs_cfg = sfd_cfg & 0b11
		gf_cfg = (sfd_cfg & 0b11100) >> 2
		result["delay_us"] : self.delay_map[gf_cfg]

		# parse fault type select
		result["ov_fault_enabled"] = rs_cfg == 1 or rs_cfg == 0
		result["uv_fault_enabled"] = rs_cfg == 1 or rs_cfg == 2

		return result


	def convert_raw_voltage(self, result):
		coef = (result["range"]["high"] - result["range"]["low"]) / 255
		offset = result["range"]["low"]
		result["ov_threshold"] = result["ov_threshold_raw"] * coef + offset
		result["uv_threshold"] = result["uv_threshold_raw"] * coef + offset
		result["ov_hysteresis"] = result["ov_hysteresis_raw"] * coef
		result["uv_hysteresis"] = result["uv_hysteresis_raw"] * coef
		return result



class VP(VoltagePin):
	"""
		Reading out VPx pins
	"""

	range_map = {
		0 : {
			"comment" : "Midrange (2.5V to 6V)",
			"low" : 2.5,
			"high" : 6.0,
			"adc_high" : 6.0,
			"attenuation" : 4.363,
		},
		1 : {
			"comment" : "Low range (1.25V to 3V)",
			"low" : 1.25,
			"high" : 3.0,
			"adc_high" : 4.46,
			"attenuation" : 2.181,
		},
		2 : {
			"comment" : "Ultralow range (0.573V to 1.375V)",
			"low" : 0.573,
			"high" : 1.375,
			"adc_high" : 2.048,
			"attenuation" : 1,
		},
		3 : {
			"comment" : "Ultralow range (0.573V to 1.375V)",
			"low" : 0.573,
			"high" : 1.375,
			"adc_high" : 2.048,
			"attenuation" : 1,
		},
	}


	def __init__(self, name, registers):
		super().__init__(name, registers)


	def read(self):
		result = super().read()

		# parse range
		sel_cfg = self.read_field("sfd_sel") & 0b11
		result["range"] = self.range_map[sel_cfg]
		result["type"] = "vp"
		self.convert_raw_voltage(result)
		return result



class VH(VoltagePin):
	"""
		Reading out VH pin
	"""

	range_map = {
		0 : {
			"comment" : "Low range (2.5V to 6V)",
			"low" : 2.5,
			"high" : 6.0,
			"adc_high" : 6.0,
			"attenuation" : 4.363,
		},
		1 : {
			"comment" : "High range (6V to 14V)",
			"low" : 6.0,
			"high" : 14.0,
			"adc_high" : 14.4,
			"attenuation" : 10.472,
		},
	}


	def __init__(self, name, registers):
		super().__init__(name, registers)


	def read(self):
		result = super().read()

		# parse range
		sel_cfg = self.read_field("sfd_sel") & 0b1
		result["range"] = self.range_map[sel_cfg]
		result["type"] = "vh"
		self.convert_raw_voltage(result)
		return result



class VX(VoltagePin):
	"""
		Reading out VH pin
	"""

	range_map = {
		"comment" : "Ultralow range (0.573V to 1.375V)",
		"low" : 0.573,
		"high" : 1.375,
		"adc_high" : 2.048,
		"attenuation" : 1,
	}


	function_map = {
		0 : "SFD (fault) only",
		1 : "GPI (fault) only",
		2 : "GPI (fault) + SFD (warning)",
		3 : "No function (ADC only)"
	}


	def __init__(self, name, registers):
		super().__init__(name, registers)


	def read(self):
		result = super().read()
		result["range"] = self.range_map
		sel_cfg = self.read_field("sfd_sel") & 0b11
		result["function"] = self.function_map[sel_cfg]
		result["type"] = "vx"
		self.convert_raw_voltage(result)
		return result



class AUX():
	def read(self):
		return {"range" : {"low" : 0, "high" : 5.0, "adc_high" : 6.0, "attenuation" : 4.363,}, "type" : "aux"}


pdo_clk_100_kHz = "Enables 100 kHz clock out onto pin"

class PDO():
	pdo_cfg = {
		1 : 0x07,
		2 : 0x0F,
		3 : 0x17,
		4 : 0x1F,
		5 : 0x27,
		6 : 0x2F,
		7 : 0x37,
		8 : 0x3F,
		9 : 0x47,
		10 : 0x4F,
	}


	pullup_map = {
		0b0000 : "None",
		0b0001 : "None",
		0b0010 : "Pull-up to 12V charge pump voltage",
		0b0011 : "Pull-up to 12V charge pump voltage",
		0b0100 : "Weak open-drain pull-up to VP1",
		0b0101 : "Push-pull pull-up to VP1",
		0b0110 : "Weak open-drain pull-up to VP2",
		0b0111 : "Push-pull pull-up to VP2",
		0b1000 : "Weak open-drain pull-up to VP3",
		0b1001 : "Push-pull pull-up to VP3",
		0b1010 : "Weak open-drain pull-up to VP4",
		0b1011 : "Push-pull pull-up to VP4",
		0b1110 : "Weak open-drain pull-up to VDDCAP",
		0b1111 : "Push-pull pull-up to VDDCAP",
	}


	status_map = {
		0b000 : "Disabled with weak pull-down",
		0b001 : "Enabled, follows the logic driven by the Sequence Engine",
		0b010 : "Enables SMBus data, drive low",
		0b011 : "Enables SMBus data, drive high",
		0b100 : pdo_clk_100_kHz,
		0b101 : pdo_clk_100_kHz,
		0b110 : pdo_clk_100_kHz,
		0b111 : pdo_clk_100_kHz,
	}


	def __init__(self, registers):
		self.regs = registers


	def read(self):	
		result = {}
		for pdo_id, pdo_cfg_reg in self.pdo_cfg.items():
			pdo_cfg = self.regs.read(pdo_cfg_reg)
			pullup = self.pullup_map[pdo_cfg & 0b1111]
			status = self.status_map[(pdo_cfg & 0b01110000) >> 4]
			result[pdo_id] = { "status" : status, "pullup" : pullup }
		return result


RR_GO = 0b1
RR_RUN = 0b10
RR_AVG = 0b100
RR_STOP_WRITE = 0b1000
RR_CLEAR_LIM = 0b10000


class ADC(RegisterField):

	fields = {
		"rrsel1" : 0x80,
		"rrsel2" : 0x81,
		"rrctrl" : 0x82,
		"adc_vp1_msb" : 0xA0,
		"adc_vp1_lsb" : 0xA1,
		"adc_vp2_msb" : 0xA2,
		"adc_vp2_lsb" : 0xA3,
		"adc_vp3_msb" : 0xA4,
		"adc_vp3_lsb" : 0xA5,
		"adc_vp4_msb" : 0xA6,
		"adc_vp4_lsb" : 0xA7,
		"adc_vh_msb" : 0xA8,
		"adc_vh_lsb" : 0xA9,
		"adc_vx1_msb" : 0xAA,
		"adc_vx1_lsb" : 0xAB,
		"adc_vx2_msb" : 0xAC,
		"adc_vx2_lsb" : 0xAD,
		"adc_vx3_msb" : 0xAE,
		"adc_vx3_lsb" : 0xAF,
		"adc_vx4_msb" : 0xB0,
		"adc_vx4_lsb" : 0xB1,
		"adc_vx5_msb" : 0xB2,
		"adc_vx5_lsb" : 0xB3,
		"adc_ints_msb" : 0xB4,
		"adc_ints_lsb" : 0xB5,
		"adc_aux1_msb" : 0xB4,
		"adc_aux1_lsb" : 0xB5,
		"adc_exts1_msb" : 0xB6,
		"adc_exts1_lsb" : 0xB7,
		"adc_aux2_msb" : 0xB6,
		"adc_aux2_lsb" : 0xB7,
		"adc_exts2_msb" : 0xB8,
		"adc_exts2_lsb" : 0xB9,
		"fstat1" : 0xe0,
		"fstat2" : 0xe1,
		"ovstat1" : 0xe2,
		"ovstat2" : 0xe3,
		"uvstat1" : 0xe4,
		"uvstat2" : 0xe5,
		"limstat1" : 0xe6,
		"limstat2" : 0xe7,
		"gpistat" : 0xe8,
	}

	stat_bits = {
		"vp1" : {"group" : 1, "bit" : 0},
		"vp2" : {"group" : 1, "bit" : 1},
		"vp3" : {"group" : 1, "bit" : 2},
		"vp4" : {"group" : 1, "bit" : 3},
		"vh"  : {"group" : 1, "bit" : 4},
		"vx1" : {"group" : 1, "bit" : 5},
		"vx2" : {"group" : 1, "bit" : 6},
		"vx3" : {"group" : 1, "bit" : 7},
		"vx4" : {"group" : 2, "bit" : 0},
		"vx5" : {"group" : 2, "bit" : 1},
		"aux1" : {"group" : 2, "bit" : 2},
		"aux2" : {"group" : 2, "bit" : 3},
	}

	gpi_bits = {
		"vx1" : 0,
		"vx2" : 1,
		"vx3" : 2,
		"vx4" : 3,
		"vx5" : 4,
	}


	# channel_count = 13
	# sleep_per_channel_ms = 0.00044
	rr_delay_ms = 13 * 0.00044

	def __init__(self, adm):
		self.regs = adm.regs
		self.adm = adm
		self.adc_bits = 12


	def adc_to_volts(self, channel, adc_raw):
		attenuation = self.adm.adc_attenuation[channel]
		if self.adc_bits == 12:
			return adc_raw / ((1 << 12) - 1) * attenuation * 2.048
		else:
			return adc_raw / ((1 << 16) - 1) * attenuation * 2.048


	def read(self):
		self.write_field("rrsel1", 0x00)
		self.write_field("rrsel2", 0x00)
		if self.adc_bits == 12:
			self.write_field("rrctrl", RR_GO | RR_CLEAR_LIM)
			sleep(self.rr_delay_ms * 1.5)
		else:
			self.write_field("rrctrl", RR_GO | RR_CLEAR_LIM | RR_AVG)
			sleep(self.rr_delay_ms * 16 * 1.5)

		self.regs.refresh(start=0x80, length=64)
		self.regs.refresh(start=0x0e, length=8)


		result = {}
		for pin in self.adm.input_pins.keys():
			msb = self.read_field(f"adc_{pin}_msb")
			lsb = self.read_field(f"adc_{pin}_lsb")
			adc_raw = ((msb & 0b1111) << 8) | lsb if self.adc_bits == 12 else (msb << 8) | lsb
			adc_voltage = self.adc_to_volts(pin, adc_raw)
			result[pin] = {
				"raw" : adc_raw,
				"volts" : adc_voltage,
				"fault" : self.status_field(pin, "fstat"),
				"overvoltage" : self.status_field(pin, "ovstat"),
				"undervoltage" : self.status_field(pin, "uvstat"),
				"limit" : self.status_field(pin, "limstat"),
			}

			if isinstance(self.adm.input_pins[pin], VX):
				result[pin]["gpi"] = self.gpi_status(pin)

		return result


	def status_field(self, pin, field):
		group = self.stat_bits[pin]["group"]
		bit = self.stat_bits[pin]["bit"]
		group_status = self.read_field(f"{field}{group}")
		return bool((group_status >> bit) & 1)


	def gpi_status(self, pin):
		bit = self.gpi_bits[pin]
		group_status = self.read_field("gpistat")
		return bool((group_status >> bit) & 1)



class DAC(RegisterField):
	fields = {
		"dacctrl1" : 0x50,
		"dac1" : 0x58,
		"dplim1" : 0x60,
		"dnlim1" : 0x68,

		"dacctrl2" : 0x51,
		"dac2" : 0x59,
		"dplim2" : 0x61,
		"dnlim2" : 0x69,

		"dacctrl3" : 0x52,
		"dac3" : 0x5a,
		"dplim3" : 0x62,
		"dnlim3" : 0x6a,

		"dacctrl4" : 0x53,
		"dac4" : 0x5b,
		"dplim4" : 0x63,
		"dnlim4" : 0x64,

		"dacctrl5" : 0x54,
		"dac5" : 0x5c,
		"dplim5" : 0x64,
		"dnlim5" : 0x6c,

		"dacctrl6" : 0x55,
		"dac6" : 0x5d,
		"dplim6" : 0x65,
		"dnlim6" : 0x6d,
	}

	channels = range(1, 6+1)

	offset_map = {
		0b00 : 1.2,
		0b01 : 1.0,
		0b10 : 0.8,
		0b11 : 0.6
	}

	def code_to_voltage(code, midcode_voltage):
		return (code - 0x7f)/256 * 0.6015 + midcode_voltage


	def clamp(value, min_value, max_value):
		return max(min(value, max_value), min_value)


	def __init__(self, registers):
		self.regs = registers


	def read_channel(self, channel):
		dac_ctrl = self.read_field(f'dacctrl{channel}')
		midcode_voltage = self.offset_map[dac_ctrl & 0b11]
		enabled = bool(dac_ctrl & 0b100)
		raw = self.read_field(f'dac{channel}')
		pos_limit_raw = self.read_field(f'dplim{channel}')
		neg_limit_raw = self.read_field(f'dnlim{channel}')
		dac_output = DAC.code_to_voltage(raw, midcode_voltage)
		pos_limit = DAC.code_to_voltage(pos_limit_raw, midcode_voltage)
		neg_limit = DAC.code_to_voltage(neg_limit_raw, midcode_voltage)
		dac_output = DAC.clamp(dac_output, min_value=neg_limit, max_value=pos_limit)

		return {
			"enabled" : enabled,
			"raw" : raw,
			"pos_limit_raw" : pos_limit_raw,
			"neg_limit_raw" : neg_limit_raw,
			"voltage" : dac_output,
			"pos_limit_voltage" : pos_limit,
			"neg_limit_voltage" : neg_limit,
		}


	def read(self):
		result = {}
		for channel in self.channels:
			result[channel] = self.read_channel(channel)
		return result


class ADM106x(RegisterField):
	fields = {		
		"revid" : 0xf5,
		"manid" : 0xf4
	}

	adm_id = 0x41

	input_pin_types = {
		"vp" : ["vp1", "vp2", "vp3", "vp4"],
		"vx" : ["vx1", "vx2", "vx3", "vx4", "vx5"],
		"vh" : ["vh"],
		"aux" : ["aux1", "aux2"]}


	def __init__(self, i2c_bus, i2c_addr, reg_manager=None):
		self.regs = RegisterManager(i2c_bus, i2c_addr) if reg_manager is None else reg_manager
		try:
			self.identify_device()
			self.initialize_input_pins()
			self.adc = ADC(self)
			self.pdo = PDO(self.regs)
			self.dac = DAC(self.regs)
			self.error = None
		except (I2CError, IOError) as err:
			self.error = str(err)


	def read(self):
		if self.error is not None:
			return {"error" : self.error}

		result = { "inputs" : {} }
		for name, pin in self.input_pins.items():			
			pin_config = pin.read()			
			result["inputs"][name] = pin_config		
			self.adc_range[name] = pin_config["range"]["adc_high"]
			self.adc_attenuation[name] = pin_config["range"]["attenuation"]

		all_adc_data = self.adc.read()
		for name, adc_data in all_adc_data.items():
			result["inputs"][name]["adc"] = adc_data

		result["pdo"] = self.pdo.read()
		result["dac"] = self.dac.read()
		result["device_type"] = self.name

		return result


	def initialize_input_pins(self):
		self.input_pins = {}
		self.adc_range = {}
		self.adc_attenuation = {}
		for pin_type, pin_names in self.input_pin_types.items():			
			for name in pin_names:
				if pin_type == "vp":
					controller = VP(name, self.regs)
				elif pin_type == "vx":
					controller = VX(name, self.regs)
				elif pin_type == "vh":
					controller = VH(name, self.regs)
				else:
					controller = AUX()

				self.input_pins[name] = controller
				self.adc_range[name] = float("NaN")	
				self.adc_attenuation[name] = float("NaN")	


	def identify_device(self):		
		self.regs.refresh(start=0, length=0xC0)
		self.regs.refresh(start=0xe0, length=0x17)
		manid = self.read_field("manid")
		if manid != self.adm_id:
			raise IOError(f"Invalid MANID of ADM106x device, expected 0x{self.adm_id:02x}, actual 0x{manid:02x}.")

		self.revid = (self.read_field("revid") >> 4) 
		if self.revid == 0:
			self.name = "ADM106x"
		elif self.revid == 1:
			self.name = "ADM1166"
		else: 
			self.name = "(unknown revision)"



class TestRegisterManager():
	"""
		Emulator for ADM1066 registers
	"""	
	register_count = 0xff
	default_registers = [
		#  0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,  # 0x00
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,  # 0x10
		0xD4,0x01,0x98,0x01,0x0C,0x01,0x00,0x1E,0xFF,0x00,0x00,0x00,0x01,0x01,0x1A,0x20,  # 0x20
		0x68,0x13,0x00,0x00,0x0C,0x00,0x00,0x20,0xFF,0x00,0xFF,0x00,0x0C,0x01,0x02,0x20,  # 0x30
		0xD7,0x00,0x00,0x00,0x0C,0x00,0x00,0x20,0xD7,0x09,0xA8,0x09,0x0C,0x00,0x00,0x00,  # 0x40
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0x50
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0x60
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,  # 0x70
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0x80
		0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0x90
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xA0
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xB0
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xC0
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xD0
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xE0
		0x00,0x00,0x00,0x00,0x41,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  # 0xF0
	]

	def __init__(self, i2c_bus, i2c_addr):
		self.i2c_addr = i2c_addr
		self.i2c_bus = i2c_bus
		self.registers = self.default_registers


	def read(self, reg_id, use_cache=True):
		return self.registers[reg_id]


	def write(self, reg_id, reg_val):
		self.registers[reg_id] = reg_val


	def read_block(self, start, length):
		return self.registers[start:start+length]


	def refresh(self, start, length):
		pass


	def set_adc_raw(self, adc, value):
		regs = {
			"vp1" : 0xa0, "vp2" : 0xa2, "vp3" : 0xa4, "vp4" : 0xa6, "vh" : 0xa8,
			"vx1" : 0xaa, "vx2" : 0xac, "vx3" : 0xae, "vx4" : 0xb0, "vx5" : 0xb2,
			"aux1" : 0xb4, "aux2" : 0xb6, "exts1" : 0xb6, "exts2" : 0xb8 }
		
		lsb = value & 0xff
		msb = (value >> 8) & 0xff
		msb_addr = regs[adc]
		lsb_addr = msb_addr + 1

		self.registers[lsb_addr] = lsb
		self.registers[msb_addr] = msb


