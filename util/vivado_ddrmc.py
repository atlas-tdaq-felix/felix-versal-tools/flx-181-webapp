import pdb

def progress_bar(left, right, center, max_tap, max_edge=100, tap_width=1):
	'''
		Calculates progress bar based on the margin values
	'''
	free_space = max_edge - tap_width

	left_edge = left / max_tap * free_space
	left_margin = (center - left) / max_tap * free_space
	right_margin = (right - center) / max_tap * free_space
	right_edge = (max_tap - right) / max_tap * free_space

	return {
		"left_edge" : left_edge,
		"left_margin" : left_margin,
		"left_margin_value" : center - left,
		"tap_width" : tap_width,
		"right_margin" : right_margin,
		"right_margin_value" : right - center,
		"right_edge" : right_edge,
		"left_value" : left,
		"right_value" : right,
		"center_value" : center,
	}


def progress_bar_center_aligned(left, right, max_tap, max_edge=100, tap_width=1):
	'''
		Calculates progress bar based on the margin values
	'''
	free_space = max_edge - tap_width

	center = (max_tap + 1) / 2

	left_edge = (center - left) / max_tap * free_space
	left_margin = left / max_tap * free_space
	right_margin = right / max_tap * free_space
	right_edge = (max_tap - right -center) / max_tap * free_space

	return {
		"left_edge" : left_edge,
		"left_margin" : left_margin,
		"left_margin_value" : left,
		"tap_width" : tap_width,
		"right_margin" : right_margin,
		"right_margin_value" : right,
		"right_edge" : right_edge,
		"left_value" : center - left,
		"right_value" : center + right,
		"center_value" : center,
	}
 

def read_margins_versal(props, kind, edge, nibble):
	left = "F0_RD{}_{}QTR_LEFT_NIBBLE{:02d}".format(kind, edge, nibble)
	right = "F0_RD{}_{}QTR_RIGHT_NIBBLE{:02d}".format(kind, edge, nibble)
	center = "F0_RD{}_{}QTR_FINAL_NIBBLE{:02d}".format(kind, edge, nibble)
	if (left in props and right in props and center in props):
		return {
			"left" : props[left],
			"right" : props[right],
			"center" : props[center]}
	else:
		return {}


def write_margins_versal(props, kind, byte):
	"""
		It's unclear how Vivado calculates the Center point value from the properties.
		For this reason, the plots are center-aligned
	"""
	left = "F0_WR{}_LEFT_MARGIN_BYTE{:01d}".format(kind, byte)
	right = "F0_WR{}_RIGHT_MARGIN_BYTE{:01d}".format(kind, byte)
	
	if (left in props and right in props):
		result = { "left" : props[left], "right" : props[right]}
		return result
	else:
		return {}


def margins_versal(data):
	calibrations = {
		"read_simple_pos_edge" : {
			"comment" : "Read mode, simple pattern, rising clock edge [taps]",
			"kind" : "DQ",
			"edge" : "P",
			"read" : True
		},
		"read_simple_neg_edge" : {
			"comment" : "Read mode, simple pattern, falling clock edge [taps]",
			"kind" : "DQ",
			"edge" : "N",
			"read" : True
		},
		"read_complex_pos_edge" : {
			"comment" : "Read mode, complex pattern, rising clock edge [taps]",
			"kind" : "CMPLX",
			"edge" : "P",
			"read" : True
		},
		"read_complex_neg_edge" : {
			"comment" : "Read mode, complex pattern, falling clock edge [taps]",
			"kind" : "CMPLX",
			"edge" : "N",
			"read" : True
		},

		"write_simple" : {
			"comment" : "Write mode, simple pattern, center aligned taps",
			"kind" : "DQDBI",
			"edge" : "P",
			"read" : False
		},

		"write_complex" : {
			"comment" : "Write mode, complex pattern, center aligned taps",
			"kind" : "CMPLX",
			"edge" : "P",
			"read" : False
		}
	}

	for ddrmc_name, ddrmc_data in data.items():
		margins = {}
		ddrmc_data["margins"] = margins
		nibbles = 16
		nbytes = 8

		props = ddrmc_data["properties"]
		for name, calibration in calibrations.items():
			result = { "data" : {} }
			margins[name] = result
			result["comment"] = calibration["comment"]
			if calibration["read"]:
				for nibble in range(0, nibbles):
					margin_data = read_margins_versal(props, kind=calibration["kind"],
						edge=calibration["edge"], nibble=nibble)								
					if margin_data:
						result["data"]["Nibble #{}".format(nibble)] = progress_bar(
							left=margin_data["left"], right=margin_data["right"],
							center=margin_data["center"], max_tap=255)
			else:
				for byte in range(0, nbytes):
					margin_data = write_margins_versal(props, kind=calibration["kind"], byte=byte)
					if margin_data:
						result["data"]["Byte #{}".format(byte)] = progress_bar_center_aligned(
							left=margin_data["left"], right=margin_data["right"], max_tap=511)


def vivado_read_margins(data, style):
	if style == "versal":
		return margins_versal(data)

	