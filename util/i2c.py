import pdb, contextlib, inspect
import periphery
from threading import Lock
from periphery import I2C as I2C_p
from datetime import datetime, timedelta
from loguru import logger

I2CError = periphery.I2CError

# Lock guarding the i2c_locks data structure
lock = Lock()

# locks serializing access to each i2c bus
i2c_locks = {}

# maximum time that a requester is allowed to wait for a bus
bus_lock_timeout = 1


class I2C():
    def read(self, i2c_addr, reg_addr):
        msgs = [I2C_p.Message([reg_addr]), I2C_p.Message([0x00], read=True)]
        self.i2c.transfer(i2c_addr, msgs)
        return msgs[1].data[0]


    def write(self, i2c_addr, reg_addr, reg_val):
        msgs = [I2C_p.Message([reg_addr, reg_val], read=False)]
        self.i2c.transfer(i2c_addr, msgs)

    def transfer(self, i2c_addr, messages):
        msgs = [I2C_p.Message(msg["data"], read=msg["read"]) for msg in messages]
        self.i2c.transfer(i2c_addr, msgs)
        return msgs

    def close(self):
        with lock:
            i2c_locks[self.i2c_bus]["lock"].release()

        if hasattr(self, "i2c"):
            self.i2c.close()


    def open(self, i2c_bus):
        with lock:
            if not i2c_bus in i2c_locks:
                i2c_locks[i2c_bus] = { "lock" : Lock(), "last_requester" : None}

            # record who is acquiring the lock in case if they forget to release it
            caller_frame = inspect.currentframe().f_back            
            caller_method = caller_frame.f_code.co_name
            caller_module = caller_frame.f_code.co_filename
            caller_line = caller_frame.f_lineno

            if "self" in caller_frame.f_locals:
                caller_class = caller_frame.f_locals["self"].__class__.__name__
            else:
                caller_class = None

            bus_record = i2c_locks[i2c_bus]
            success = bus_record["lock"].acquire(timeout=bus_lock_timeout)

            if not success:
                last_requester = bus_record["last_requester"]
                raise TimeoutError(f"I2C bus {i2c_bus} appears stuck." 
                    + f" Last requester information: {last_requester}")

            self.i2c_bus = i2c_bus

            bus_record["last_requester"] = {"module" : caller_module, "line" : caller_line,
                "caller_method" : caller_method, "caller_class" : caller_class}

        self.i2c = I2C_p(i2c_bus)


    @classmethod
    @contextlib.contextmanager
    def context(cls, i2c_bus):
        i2c = cls()
        try:
            i2c.open(i2c_bus)
            yield i2c
        finally:
            i2c.close()




class I2C_ADM106x(I2C):   
    '''
        NOTE: I2C operations with repeated START condition must be supported by the 
        system I2C driver for this class to work correctly!
    '''
    def __init__(self):
        # how many registers read_block operation returns
        self.block_length = 32


    def read_block_helper(self, i2c_addr, reg_addr, length):
        # perform block address read operation
        if length > self.block_length or length <= 0:
            raise ValueError(f"Block length ({length}) is out of range! Allowed values: 1..32")
        msgs = [I2C_p.Message([0xFD]), I2C_p.Message([0x00] * (self.block_length + 1), read=True)]
        self.i2c.transfer(i2c_addr, msgs)
        return msgs[1].data[1:length + 1]


    def set_read_pointer(self, i2c_addr, ptr_addr):
        # set the block read pointer
        msgs = [I2C_p.Message([ptr_addr], read=False)]
        self.i2c.transfer(i2c_addr, msgs)


    def read_block(self, i2c_addr, reg_addr, length):
        block_count = length // self.block_length
        remainder_length = length - block_count * self.block_length

        data = []

        for i in range(block_count):
            self.set_read_pointer(i2c_addr, reg_addr + i * self.block_length)
            data += self.read_block_helper(i2c_addr, reg_addr, self.block_length)

        if remainder_length > 0:
            self.set_read_pointer(i2c_addr, reg_addr + block_count * self.block_length)
            data += self.read_block_helper(i2c_addr, reg_addr, remainder_length)
        
        return data


    # I will not be implementing or testing this operation
    # def write_block(self, i2c_addr, data):
    #     pass


# TODO: replace this with calling class method generator instead
@contextlib.contextmanager
def i2c_handler(i2c_bus):
    i2c = I2C()
    try:
        i2c.open(i2c_bus)
        yield i2c
    finally:
        i2c.close()
