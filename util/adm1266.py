import pdb
from util.i2c import i2c_handler, I2CError, I2C

def sign_extend(value, bits):
	sign = 1 << (bits - 1)
	return (value & (sign - 1)) - (value & sign)

class ADM1266():

	pins = [
		"VH1", "VH2", "VH3", "VH4",
		"VP1", "VP2", "VP3", "VP4",
		"VP5", "VP6", "VP7", "VP8",
		"VP9", "VP10", "VP11", "VP12",
		"VP13",
	]

	flags = {
		1 << 4 : "UV fault",
		1 << 5 : "UV warn",
		1 << 6 : "OV warn",
		1 << 7 : "OV fault",
	}

	NUM_CHANNELS = 17

	PAGE = 0x00
	VOUT_MODE = 0x20
	VOUT_OV_FAULT_LIMIT = 0x40
	VOUT_OV_WARN_LIMIT = 0x42
	VOUT_UV_WARN_LIMIT = 0x43
	VOUT_UV_FAULT_LIMIT = 0x44
	STATUS_VOUT = 0x7A
	READ_VOUT = 0x8B
	IC_DEVICE = 0xAD
	IC_DEVICE_REV = 0xAE
	POWERUP_COUNTER = 0xE4

	def write_reg(self, cmd, data):
		with i2c_handler(self.i2c_bus) as i2c:
			messages = [
				{ "data" : [cmd] + data, "read" : False }
			]

			i2c.transfer(self.i2c_addr, messages)

	def read_reg(self, cmd, size):
		with i2c_handler(self.i2c_bus) as i2c:
			messages = [
				{ "data" : [cmd], "read" : False },
				{ "data" : [0x00] * size, "read" : True }
			]

			messages = i2c.transfer(self.i2c_addr, messages)
			return messages[1].data

	def __init__(self, i2c_bus, i2c_addr, rails):
		self.i2c_addr = i2c_addr
		self.i2c_bus = i2c_bus
		self.rails = rails

	def read(self):
		result = { 'pins' : {} }

		chip_id = self.read_reg(self.IC_DEVICE, 4)
		result['chip_id'] = f'{chip_id[1]:02X}{chip_id[2]:02X}{chip_id[3]:02X}'

		data = self.read_reg(self.IC_DEVICE_REV, 9)
		fw_rev = data[1:4]
		boot_rev = data[4:7]
		chip_rev = data[7:9]

		result['fw_rev'] = f'{fw_rev[0]}.{fw_rev[1]}.{fw_rev[2]}'
		result['boot_rev'] = f'{boot_rev[0]}.{boot_rev[1]}.{boot_rev[2]}'
		result['chip_rev'] = f'{chr(chip_rev[0])}{chr(chip_rev[1])}'

		counter = int.from_bytes(self.read_reg(self.POWERUP_COUNTER, 3)[1:2], 'little')
		result['powerup_counter'] = counter

		for i in range(0, self.NUM_CHANNELS):
			self.write_reg(self.PAGE, [i])

			exp = sign_extend(self.read_reg(self.VOUT_MODE, 1)[0] & 0x1F, 5)
			status = self.read_reg(self.STATUS_VOUT, 1)[0]
			vout = int.from_bytes(self.read_reg(self.READ_VOUT, 2), 'little') * 2 ** exp
			ov_fault = int.from_bytes(self.read_reg(self.VOUT_OV_FAULT_LIMIT, 2), 'little') * 2 ** exp
			ov_warn = int.from_bytes(self.read_reg(self.VOUT_OV_WARN_LIMIT, 2), 'little') * 2 ** exp
			uv_warn = int.from_bytes(self.read_reg(self.VOUT_UV_WARN_LIMIT, 2), 'little') * 2 ** exp
			uv_fault = int.from_bytes(self.read_reg(self.VOUT_UV_FAULT_LIMIT, 2), 'little') * 2 ** exp

			status_str = ""

			for f, s in self.flags.items():
				if status & f:
					status_str += s

			rail = self.rails[i] if i < len(self.rails) else "N/A"

			result['pins'][i] = { 'pin' : self.pins[i], 'rail' : rail, 'vout' : vout, 'uv_fault' : uv_fault, 'uv_warn' : uv_warn, 'ov_warn' : ov_warn, 'ov_fault' : ov_fault, 'status' : status_str}

		return result
