import pdb
from pprint import pprint
from copy import deepcopy
from util.i2c import i2c_handler, I2CError, I2C
from time import sleep

# this pre-amble and post-amble are from ClockBuilder Pro for revision B
rev_B_pre_amble = [(0x0b24, 0xD8), (0x0b25, 0x00), (0x0540, 0x01)]
rev_B_post_amble = [(0x0514, 0x01), (0x01c, 0x01), (0x0540, 0x00), (0x0b24, 0xDB), (0x0b25, 0x02)]

# this pre-amble and post-amble are from ClockBuilder Pro for revision D
rev_D_pre_amble = [(0x0b24, 0xC0), (0x0b25, 0x00), (0x0540, 0x01)]
rev_D_post_amble = [(0x0514, 0x01), (0x01c, 0x01), (0x0540, 0x00), (0x0b24, 0xc3), (0x0b25, 0x02)]

# this pre-amble and post-amble are from the reference manual for revision D
#pre_amble = [(0x0b24, 0xc0), (0x0b25, 0x00), (0x0540, 0x01)]
#post_amble = [(0x01c, 0x01), (0x0540, 0x00), (0x0b24, 0xc3), (0x0b25, 0x02)]


f_vco_nominal = 14.028 * 1e9

register_map = {
	"page"		: 0x0001,
	"los"		: 0x000d,
	"status"	: 0x000c,
	'oof'		: 0x000d,
	'los'		: 0x000d,
	'lol'		: 0x000e,
	'grade'		: 0x0004,
	'i2c_base_addr'		: 0x000B,
	'oof_ref_sel'		: 0x0040,
	'revision'		: 0x0005,
	'base_number' : [0x2, 0x3],
	'cal_status'		: 0x000f,
	"soft_rst"	: 0x001c, # bit 0
	'hard_rst'	: 0x001e, # bit 1
	'hold'		: 0x00e,
	'hold_hist_valid'	: 0x053f, # bit 1
	'clk_switch_mode'	: 0x0536,
	'in_sel'			: 0x052a,
	'zdm_en'	: 0x0487, # bit 0
	'in_en'		: 0x0949,
	'out' : {
		'base' : [0x0108, 0x010d, 0x0112, 0x0117, 0x011c, 0x0121, 0x0126, 0x012b, 0x0130, 0x013a],
		'block_length' : 5,
		'config'	: 0,
		'format'	: 1,
		'swing'		: 2,
		'mux'		: 3,
		'all_disable_low' : 0x0102,
	},

	'xaxb_extclk_en'	: 0x090e,
	'pxaxb'		: 0x0206, # pre-scale divide ratio for XAXB

	'p_div' : {
		'base' : [0x0208, 0x0212, 0x021c, 0x0226 ],
		'block_length' : 10,
		'num_bits' : 48,
		'den_bits' : 32,
	},
	'r_div' : {
		'base' : [0x024a, 0x024d, 0x0250, 0x0253, 0x0256, 0x0259, 0x025c, 0x025f, 0x0262, 0x0268],
		'block_length' : 3,
		'num_bits' : 24,
		'den_bits' : 0,
	}, 
	'n_div' : {
		'base' : [0x0302, 0x030d, 0x0318, 0x0323, 0x032e], 
		'block_length' : 11,
		'num_bits' : 44,
		'den_bits' : 32,
	}, 
	'm_div' : {
		'base'	: [0x0515],
		'block_length' : 11,
		'num_bits' : 56,
		'den_bits' : 32,
	},
	'mxaxb_div' : {
		'base' : [0x235],
		'block_length' : 10,
		'num_bits' : 44,
		'den_bits' : 32,
	}
}



class SI5345():
	def __init__(self, config: dict):
		self.config = deepcopy(config)
		self.i2c_addr = config['i2c_addr']
		self.i2c_bus = config['i2c_bus']

		if 'i2c_addr_alt' in config:
			self.update_i2c_address(config['i2c_addr_alt'])


	def update_i2c_address(self, alt_addresses):
		new_base_address = self.base_i2c_address()
		for alt_i2c_addr in alt_addresses:
			try:
				with i2c_handler(self.i2c_bus) as i2c:
					i2c.write(alt_i2c_addr, register_map['page'], 0)
					i2c.write(alt_i2c_addr, register_map['i2c_base_addr'], new_base_address)
					print(f"Updated the base address of {self.config['comment']} to 0x{new_base_address:x}")
			except I2CError:
				pass


	def base_i2c_address(self):
		return self.i2c_addr & 0xFC


	def update_config(self, new_config: dict):
		for key, value in new_config.items():
			self.config[key] = value


	def load_configuration(self, register_data: str):	
		'''
			Export configuration as csv file		
			No headers, no preamble (uncheck the checkboxes)
		'''

		if not register_data:
			return

		

		registers = parse_register_data(register_data)
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				revision = self.read_register(i2c, register_map['revision']) 
				if revision == 1:
					pre_amble = rev_B_pre_amble
					post_amble = rev_B_post_amble
				else:
					pre_amble = rev_D_pre_amble
					post_amble = rev_D_post_amble

				self.write_registers(i2c, pre_amble)
				sleep(0.3)
				self.write_registers(i2c, registers)
				self.write_registers(i2c, post_amble)
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		return {"status" : "success" }


	def write_registers(self, i2c: I2C, registers: list):
		if not registers:
			return

		i2c_addr = self.i2c_addr
		current_page = float("NaN")
		debug_advice = ("If this keeps happening when you upload register configuration, "
						+ "double-check the following settings in ClockBuilder Pro: "
						+ "1) XAXB source is correctly configured (XTAL or oscillator), "
						+ "2) IO_VDD_SEL is correctly configured, and "
						+ f"3) the base I2C address is correct (should be b'{self.base_i2c_address():b}). "
						+ "Power-cycle the board to recover SI5345.")
		
		last_block = None
		for block in register_blocks(registers):
			page = block["page"]
			if current_page != page:
				try:
					self.set_page(i2c, page)
				except I2CError as err:
					msg = (f"{str(err)}; I2C transfer (bus {self.i2c_bus}, device 0x{i2c_addr:02x})  "
						+ f"hanged while changing register page. Previous block: {last_block}. "
						+ debug_advice)
					print(msg)
					raise I2CError(msg).with_traceback(err.__traceback__)

			block["data"].insert(0, block["start"])
			messages = [
				{ "data" : block["data"], "read" : False }
			]

			# NOTE: misconfiguring XAXB_EXTCLK_EN or IO_VDD_SEL will hang I2C bus here, recoverable by power cycle
			try:
				i2c.transfer(i2c_addr, messages)
			except I2CError as err:
				msg = (f"{str(err)}; I2C write transfer (bus {self.i2c_bus}, device 0x{i2c_addr:02x})  "
						+ f"hanged on block: {block}. Previous block: {last_block}. "
						+ debug_advice)
				print(msg)
				raise I2CError(msg).with_traceback(err.__traceback__)

			last_block = block


	def read_registers(self, i2c: I2C, registers: list):	
		if not registers:
			return

		i2c_addr = self.i2c_addr
		current_page = float("NaN")
		result = {}

		last_block = None
		for block in register_blocks(registers):
			page = block["page"]
			if current_page != page:
				try:
					self.set_page(i2c, page)
				except I2CError as err:
					if last_block is None:
						msg = (f"{str(err)}; I2C transfer (bus {self.i2c_bus}, device 0x{i2c_addr:02x}) "
							+ "didn't respond to I2C communication")
					else:	
						msg = (f"{str(err)}; I2C transfer (bus {self.i2c_bus}, device 0x{i2c_addr:02x})  "
							+ f"hanged while changing register page. Previous block: {last_block}")
					print(msg)
					raise I2CError(msg).with_traceback(err.__traceback__)

			messages = [
				{ "data" : [ block["start"] ], "read" : False},
				{ "data" : block["data"], "read" : True }
			]

			try:
				msgs = i2c.transfer(i2c_addr, messages)
			except I2CError as err:
				msg = (f"{str(err)}; I2C read transfer (bus {self.i2c_bus}, device 0x{i2c_addr:02x})  "
						+ f"hanged on block: {block}. Previous block: {last_block}")
				print(msg)
				raise I2CError(msg).with_traceback(err.__traceback__)

			response = msgs[1].data

			for i in range(block["length"]):
				result[block["long_address"] + i] = response[i]

			last_block = block

		return result


	def read_register(self, i2c: I2C, long_address: int) -> int:
		i2c_addr = self.i2c_addr
		page, reg_addr = address_to_8bit_data(long_address)
		self.set_page(i2c, page)
		reg_value = i2c.read(i2c_addr=i2c_addr, reg_addr=reg_addr)
		return reg_value


	def set_page(self, i2c: I2C, page: int):
		i2c.write(i2c_addr=self.i2c_addr, reg_addr=0x01, reg_val=page)
		return page


	def soft_rst(self):
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				self.soft_rst_helper(i2c)
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		return { "status" : "success" }


	def soft_rst_helper(self, i2c: I2C):
		self.write_registers(i2c, [(register_map['soft_rst'], 0x01)])


	def hard_rst(self):
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				self.write_registers(i2c, [(register_map['hard_rst'], 0x02)])
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		return { "status" : "success" }


	def read(self):		
		divider_names = ['mxaxb_div', 'p_div', 'r_div', 'n_div', 'm_div']
		registers = {}
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				for name in divider_names:
					registers[name] = self.read_registers(i2c, get_register_range(register_map[name]))
				registers['in_sel'] = self.read_register(i2c, register_map['in_sel'])
				registers['zdm_en'] = self.read_register(i2c, register_map['zdm_en'])
				registers['out_mux'] = self.read_registers(i2c, get_register_range(register_map['out'])
					+ [register_map['out']['all_disable_low']])
				registers['status'] = self.read_registers(i2c, [0x2, 0x3, 0x4, 0x5, 0xc, 0xd, 0xe, 0xf, 0x40])
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		dividers = {}
		for name in divider_names:
			dividers[name] = calculate_divider(register_map[name], registers[name])
		
		multisynth_select = get_multisynth_select(registers)
		in_sel = self.dspll_in_sel(registers)
		inputs = self.build_input_data(registers['status'], in_sel)
		zdm_en = registers['zdm_en'] & 0b1 == 1
		free_run_mode = dividers['m_div'][0]['num'] == 0 and dividers['m_div'][0]['den'] == 0
		errors = []

		if zdm_en and in_sel == 0b11:
			f_vco = float("NaN")
			errors.append("Invalid configuration: DSPLL IN_SEL=0b11 simultaneously with ZDM_EN=1")
		else:
			f_vco = self.calculate_f_vco(dividers, in_sel)

		outputs = parse_out_mux(registers)
		update_output_data(outputs, f_vco, dividers, multisynth_select)

		out_disable_all = not bool(registers['out_mux'][register_map['out']['all_disable_low']] & 0b1)
		if out_disable_all:
			errors.append("All outputs are disabled!")

		# format the results
		result = {
			'inputs' : inputs,
			'outputs' : outputs,
			'comment' : self.config['comment'],
			'status' : {
				'f_vco' : f_vco,
				'dspll_lol' : bool(registers['status'][register_map['lol']] & 0b10),
				'dspll_hold' : bool(registers['status'][register_map['lol']] & 0b100000),
				'free_run_mode' : free_run_mode,
				'cal_pll_busy' : bool(registers['status'][register_map['cal_status']] & 0b100000),
				'part' : parse_part_number(registers['status']),
				'sysincal' : bool(registers['status'][register_map['status']] & 0b1),				
				'in_sel' : in_sel,
				'in_sel_pins' : self.config["in_sel"],
				'zdm_en' : zdm_en,
				'errors' : errors,
				'out_disable_all' : out_disable_all,
			}
		}
		return result


	def build_input_data(self, registers: dict, in_sel: int):
		result = {}

		oof_ref_sel = registers[register_map['oof_ref_sel']] & 0b111

		# look up los, oof bits for the four inputs, copy the nominal frequency value
		for i in range(4):
			result[f"IN_{i}"] = {
				'los' : bool(registers[register_map['los']] & (1 << i)),
				'oof' : bool(registers[register_map['oof']] & (1 << i + 4)),
				'f_MHz' : self.config[f"f_in{i}_MHz"],
				'dspll_input' : i == in_sel,
				'oof_ref_sel' : i == oof_ref_sel,
			}

		result['XAXB'] = {
			"f_MHz" : self.config[f"f_xaxb_MHz"],
			'los' : bool(registers[register_map['status']] & 0b10),
			'oof' : bool(registers[register_map['status']] & 0b1000),
			'oof_ref_sel' : oof_ref_sel == 4,
		}

		return result


	def calculate_f_vco(self, dividers: dict, in_sel: int) -> float:
		f_in = self.dspll_in_freq(in_sel)
		p_num = dividers['p_div'][in_sel]['num']
		p_den = dividers['p_div'][in_sel]['den']
		m_num = dividers['m_div'][0]['num']
		m_den = dividers['m_div'][0]['den']

		try:
			if m_num == 0 and m_den == 0:
				result = f_vco_nominal
			else:
				result = f_in * 5 * p_den / p_num * m_num / m_den
		except ZeroDivisionError:
			result = float("NaN")
			
		return result


	def dspll_in_sel(self, registers: dict) -> int:
		'''
			Determine which input is used by DSPLL 
		'''
		pin_controlled_clk_select = registers['in_sel'] & 0b1 == 0
		zdm_en = bool(registers['zdm_en'] & 0b1)
	
		if pin_controlled_clk_select:
			# pin-controlled clock select
			# Since it's not possible to read this value form the device,
			# I am using config or user input form instead
			in_sel = self.config["in_sel"]
		else:
			if zdm_en:
				# register-controlled clock select wit zero-delay mode
				in_sel = (registers['zdm_en'] & 0b110) >> 1
			else:	
				# register-controlled clock select without zero-delay mode
				in_sel = (registers['in_sel'] & 0b110) >> 1	
		return in_sel



	def dspll_in_freq(self, in_sel: int) -> float:
		'''
			Retrieve input frequency of DSPLL
		'''
		return self.config[f"f_in{in_sel}_MHz"] * 10**6

		



def to_8bit_data(register: tuple) -> tuple:
	'''
		Split register data tuple (16-bit register address, i2c data)
		into 8-bits (page, address, data) usable by the I2C controller
	'''

	long_address, reg_data = register
	page, reg_id = address_to_8bit_data(long_address)
	return page, reg_id, reg_data


def address_to_8bit_data(long_address : int):
	page = (long_address & 0xff00) >> 8
	reg_id = (long_address & 0xff)
	return page, reg_id


def parse_register_data(data: str):
	'''
		Parses si5345 configuration file produced by ClockBuilderPro
		into a set of register values to write
	'''
	registers = []
	for line in data:
		if isinstance(line, bytes):
			line = line.decode()

		line = line.strip()
		if line[0] == "#":
			continue

		try:
			register_id_str, register_value_str = line.split(",")
			register_id = int(register_id_str, 16)
			register_value = int(register_value_str, 16)
		except ValueError:
			continue

		registers.append((register_id, register_value))
	return registers


def get_register_range(divider : dict):
	'''
		Calculate which registers must be retrieved from the device
		in order to calculate the given divider
	'''
	base = divider['base']
	block_length = divider['block_length']
	return list(range(base[0], base[-1] + block_length))


def get_byte_count(num_bits : int):
	'''
		Calculate how many bytes are needed to hold this many bits
	'''
	return num_bits // 8 + int(num_bits % 8 > 0)


def assemble_value(registers : list, base : int, num_bits : int) -> int:
	'''
		Build a divider value from several sequential single-byte registers
	'''
	num_bytes = get_byte_count(num_bits)
	mask = 2 ** num_bits - 1
	value = 0
	for i in range(num_bytes):
		value += registers[base + i] * 2**(8*i) 
	value &= mask
	return value


def get_multisynth_select(registers: dict) -> dict:
	result = {}
	bases = register_map['out']['base']
	offset = register_map['out']['mux']
	for i in range(len(bases)):
		result[i] = registers['out_mux'][bases[i] + offset] & 0b111
	return result


def calculate_divider(divider: dict, registers: dict):
	'''
		Calculate numerator and denominator of the given divider
	'''
	result = {}
	for i in range(len(divider['base'])):
		num_base = divider['base'][i]
		den_base = num_base + get_byte_count(divider['num_bits'])
		result[i] = {
			'num' : assemble_value(registers, num_base, divider['num_bits']),
			'den' : assemble_value(registers, den_base, divider['den_bits']),
		}
	return result


def parse_out_mux(registers: dict):
	result = {}
	mux_regs = register_map['out']
	for i in range(len(mux_regs['base'])):
		base = mux_regs['base'][i]
		result[i] = {
			'mux' : registers['out_mux'][base + mux_regs['mux']] & 0b111,
			'enable' : bool(registers['out_mux'][base + mux_regs['config']] & 0b10),
			'force_r_div_2' : bool(registers['out_mux'][base + mux_regs['config']] & 0b100),
			'out_pwr_down' : bool(registers['out_mux'][base + mux_regs['config']] & 0b1),
			'format' : parse_output_format(registers['out_mux'][base + mux_regs['format']] & 0b111),
			'sync_en' : bool(registers['out_mux'][base + mux_regs['format']] & 0b1000),
			'disabled_state' : parse_disabled_state((registers['out_mux'][base + mux_regs['format']] & 0b110000) >> 4),
		}
	return result


def parse_output_format(bitfield_value: int):
	if bitfield_value == 0:
		return "Reserved"
	elif bitfield_value == 1:
		return "Swing mode (normal swing) differential"
	elif bitfield_value == 2:
		return "Swing mode (high swing) differential"
	elif bitfield_value == 3:
		return "Reserved"
	elif bitfield_value == 4:
		return "LVCMOS single ended"
	elif bitfield_value == 5:
		return "LVCMOS (+ pin only)"
	elif bitfield_value == 6:
		return "LVCMOS (- pin only)"
	elif bitfield_value == 7:
		return "Reserved"

	raise ValueError(f"FORMAT is out of range (bitfield_value={bitfield_value})")


def parse_disabled_state(bitfield_value: int):
	if bitfield_value == 0:
		return "LOW"
	elif bitfield_value == 1:
		return "HIGH"
	elif bitfield_value == 2:
		return "(reserved)"
	elif bitfield_value == 3:
		return "(reserved)"

	raise ValueError(f"DIS_STATE is out of range (bitfield_value={bitfield_value})")


def update_output_data(outputs, f_vco: float, dividers: dict, multisynth_select: dict):
	num_outputs = len(register_map['r_div']['base'])
	for i in range(num_outputs):
		# calculate output frequency
		if outputs[i]['force_r_div_2']:
			r_div = 2
		else:
			r_div = dividers['r_div'][i]['num']

		select = multisynth_select[i]
		if select >= 0 and select <= 4:			
			n_num = dividers['n_div'][select]['num']
			n_den = dividers['n_div'][select]['den']
		else:			
			n_num = float("NaN")
			n_den = float("NaN")

		try:
			out_frequency = f_vco / r_div * n_den / n_num
		except ZeroDivisionError:
			out_frequency = float("NaN")			
		outputs[i]['frequency'] = out_frequency
		outputs[i]['name'] = f"OUT{i}"


def parse_part_number(registers: dict):
	msb = registers[register_map['base_number'][1]]
	lsb = registers[register_map['base_number'][0]]
	base_number = f"{msb:X}{lsb:X}"
	grade = chr(ord('A') + registers[register_map['grade']])
	revision = chr(ord('A') + registers[register_map['revision']])
	return f"SI{base_number}{grade}-{revision}"


def register_blocks(registers, max_block_length=32):
	"""
		The iterator that breaks the list of registers to read into a continuous read blocks for the I2C operation
	"""
	if not registers:
		return
    
	last = None
	start = None
	page_select = None
	start_long_address = None
	data = []
	for register in registers:
		if isinstance(register, int):
			page, reg_addr = address_to_8bit_data(register)
			reg_value = 0
			long_address = register
		elif isinstance(register, tuple):
			long_address, reg_value = register
			page, reg_addr = address_to_8bit_data(long_address)
		else:
			raise ValueError("Invalid register specification: expected an array of int or tuple")

		if start == None:
			start = reg_addr
			start_long_address = long_address
			last = start			
			page_select = page
			data = [ reg_value ]
		elif last != reg_addr - 1 or max_block_length <= last - start + 1 or page != page_select:
			yield { "start" : start, "length" : last - start + 1, "page" : page_select, "data" : data,
				"long_address" : start_long_address }
			start = reg_addr
			start_long_address = long_address
			last = start
			page_select = page
			data = [ reg_value ]
		else:
			last = reg_addr
			data.append(reg_value)
            
	yield { "start" : start, "length" : last - start + 1, "page" : page_select, "data" : data, "long_address" : start_long_address }