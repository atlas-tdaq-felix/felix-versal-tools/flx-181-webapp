from periphery import MMIO


def get_idcode(addr):
	# Retrieve the IDCODE of the device running this script from MMIO
	idcode_mmio = MMIO(addr, 0x10)
	device_id = idcode_mmio.read32(0x00)
	idcode_mmio.close()
	return device_id