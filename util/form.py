from flask import request

def form_fields(fields):
	result = {}
	for field in fields:			
		result[field] = str(request.form[field]) if field in request.form else None
	return result


class ProcessForm():
	def model_kwargs(self):
		result = {}
		for field in self:
			result[field.name] = getattr(self, field.name).data
		return result