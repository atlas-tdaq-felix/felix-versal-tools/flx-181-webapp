import periphery
from periphery import GPIO as GPIO_periphery
import fcntl
import struct
import glob

GPIOError = periphery.GPIOError

def set_line(device, line, value):
	direction = "high" if value else "low"
	gpio = GPIO_periphery("/dev/{}".format(device), line, direction)
	gpio.close()


def get_line(device, line):
	gpio = GPIO_periphery("/dev/{}".format(device), line, "in")
	value = gpio.read()
	gpio.close()
	return value


GPIO_GET_CHIPINFO_IOCTL = 0x8044B401

def detect(chip_label):
	chips = glob.glob("/dev/gpiochip*")
	for chip in chips:
		f = open(chip, "rb")

		gpiochip_info = struct.Struct("32s 32s L")
		buffer = gpiochip_info.pack(b' ', b' ', 0)
		result = fcntl.ioctl(f, GPIO_GET_CHIPINFO_IOCTL, buffer)

		name, label, lines = gpiochip_info.unpack(result)

		name = name.rstrip(b'\0').decode("utf-8")
		label = label.rstrip(b'\0').decode("utf-8")

		if label == chip_label:
			return name
