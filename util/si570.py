"""
    Utility functions to read and update frequency of SI570 devices
    (hardcoded for 20 ppm and 50 ppm version)
"""

from util.i2c import i2c_handler, I2CError
import pdb

register_map = {
    7 : "High Speed / N1 dividers",
    8 : "Reference frequency",
    9 : "Reference frequency",
    10 : "Reference frequency",
    11 : "Reference frequency",
    12 : "Reference frequency",
    13 : "High Speed / N1 dividers",
    14 : "Reference frequency",
    15 : "Reference frequency",
    16 : "Reference frequency",
    17 : "Reference frequency",
    18 : "Reference frequency"
}

# minimum frequency si570 can output
fmin_MHz = 10

# maximum frequency si570 can output
fmax_MHz = 1417.5

# Internal crystal frequency (nominal)
fxtal_MHz_nominal = 114.285

rfreq_scaling = 2**28

# Minimum allowed DCO frequency
dco_min_MHz = 4850 + 30

# Maximum allowed DCO frequency
dco_max_MHz = 5670 - 30


# calculate RFREQ from raw register value
def get_rfreq(regval):
    return regval / rfreq_scaling


# calculate HS_DIV from raw register value
def hs_div_reg2num(regval):
    if regval == 0b000:
        return 4
    if regval == 0b001:
        return 5
    if regval == 0b010:
        return 6
    if regval == 0b011:
        return 7
    if regval == 0b100:
        return "(0b100 - not used)"
    if regval == 0b101:
        return 9
    if regval == 0b110:
        return "(0b110 - not used)"
    if regval == 0b111:
        return 11
    return "(0b{0:b} - invalid)".format(regval)


# calculate HS_DIV register value for the selected divider value
def hs_div_num2reg(value):
    if value == 4:
        return 0b000
    if value == 5:
        return 0b001
    if value == 6:
        return 0b010
    if value == 7:
        return 0b011
    if value == 9:
        return 0b101
    if value == 11:
        return 0b111
    return "(0b{0:b} - invalid)".format(regval)


# calculate N1 from raw register value
def get_n1(regval):
    if regval == 0:
        return 1
    elif regval % 2 != 0:
        return regval + 1
    else:
        return regval


# calculate output frequency from the configuration parameters
def get_fout(hs_div, n1, rfreq, fxtal):
    return fxtal * rfreq / hs_div / n1


# compute si570 configuration parameters from the raw register values
def get_configuration(regmap, fxtal):

    hs_div = hs_div_reg2num((regmap[7] & 0b11100000) >> 5)
    n1 = get_n1(((regmap[7] & 0b11111) << 2)
        | ((regmap[8] & 0b11000000) >> 6))
    rfreq = get_rfreq((((regmap[8] & 0b111111) << 32) 
        | (regmap[9] << 24) | (regmap[10] << 16)
        | (regmap[11] << 8) | regmap[12]))
    fout = get_fout(hs_div, n1, rfreq, fxtal)

    # hs_div_7ppm = hs_div_reg2num((regmap[13] & 0b11100000) >> 5)
    # n1_7ppm = get_n1(((regmap[13] & 0b11111) << 2)
    #     | ((regmap[14] & 0b11000000) >> 6))
    # rfreq_7ppm = get_rfreq(((regmap[14] & 0b111111) << 32) 
    #         | (regmap[15] << 24) | (regmap[16] << 16)
    #         | (regmap[17] << 8) | regmap[18])

    configuration = {}
    configuration["HS_DIV"] = hs_div
    configuration["N1"] = n1
    configuration["FOUT"] = fout
    configuration["RFREQ"] = rfreq
    configuration["FDCO"] = rfreq * fxtal
    configuration["FXTAL"] = fxtal

    return configuration


# returns configuration of SI570 device given its name
def read_device(device):

    # open the bus, save the registers
    regmap = {}

    with i2c_handler(device["i2c_bus"]) as i2c:
        for reg_addr in register_map.keys():
            regval = i2c.read(device["i2c_addr"], reg_addr)
            regmap[reg_addr] = regval
    

    if "fxtal" in device:
        fxtal = device["fxtal"]
    else:
        fxtal = fxtal_MHz_nominal

    configuration = get_configuration(regmap, fxtal)
    configuration["regmap"] = regmap
    
    return configuration


# updates selected registers of the device
# reg_list is a list of tuples of (address, value) format
def update_device(device, reg_list):
    with i2c_handler(device["i2c_bus"]) as i2c:
        # print("Addressing I2C device 0x{:02x} on bus {}".format(
        #     device["i2c_addr"], device["i2c_bus"]))

        for reg_addr, reg_val in reg_list:
            # print("Set reg {} = 0x{:02x}".format(reg_addr, reg_val))
            i2c.write(device["i2c_addr"], reg_addr, reg_val)



# Checks if the frequency change is within 3500 ppm and DCO
def is_small_change(freq_new, freq_old):
    max_change = 3500 * 10**-6
    return abs(freq_new - freq_old) / freq_old < max_change


# return register sequence to update RFREQ register from value
def set_rfreq_n1(freq_MHz, n1_reg):    
    regval = int(freq_MHz * rfreq_scaling)
    new_regs = []
    new_regs.append((8, ((n1_reg << 6) & 0b11000000) | (regval >> 32 & 0b00111111)))
    new_regs.append((9, regval >> 24 & 0xFF))
    new_regs.append((10, regval >> 16 & 0xFF))
    new_regs.append((11, regval >> 8 & 0xFF))
    new_regs.append((12, regval & 0xFF))
    return new_regs
    

# returns f_dco diven multipliers and desired frequency
def f_dco(f_out, hs_div, n1):
    return f_out * hs_div * n1


# Select optimal dividers for given f_out
def optimal_dividers(f_out):
    hs_div_values = [4, 5, 6, 7, 9, 11]
    n1_values = list(range(2, 128 + 2, 2))
    n1_values.insert(0, 1)
    
    f_dco_min = float("inf")
    n1_final = None
    hs_div_final = None

    for hs_div in hs_div_values:
        for n1 in n1_values:
            f_dco_val = f_dco(f_out, hs_div, n1)
            if (f_dco_val > dco_min_MHz and f_dco_val < dco_max_MHz
                and f_dco_val < f_dco_min):
                f_dco_min = f_dco_val
                n1_final = n1
                hs_div_final = hs_div
    return { "FDCO" : f_dco_min, "N1" : n1_final, "HS_DIV" : hs_div_final}


# Updates output frequency of SI570 device
def set_frequency(device, freq_MHz, data):
    # print("set FOUT = {}".format(freq_MHz))
    new_regs = []

    if "fxtal" in device:
        fxtal = device["fxtal"]
    else:
        fxtal = fxtal_MHz_nominal

    if is_small_change(freq_MHz, data["FOUT"]):
        f_dco = data["RFREQ"] * fxtal
        regmap = data["regmap"]
        n1_reg = (regmap[7] & 0b11111) << 2 | (regmap[8] & 0b11000000) >> 6
        new_rfreq = data["RFREQ"] * freq_MHz / data["FOUT"]
        new_regs += set_rfreq_n1(new_rfreq, n1_reg)

    else:
        dividers = optimal_dividers(freq_MHz)
        # print("Selected dividers: {}".format(dividers))
        if dividers["N1"] is None or dividers["HS_DIV"] is None:
            raise Exception("Can't find valid dividers for f_out={}".format(data["FOUT"]))
        rfreq = dividers["FDCO"] / fxtal
        n1_reg = dividers["N1"] - 1
        hs_div_reg = hs_div_num2reg(dividers["HS_DIV"])
        # print("n1_reg = {}".format(n1_reg))
        # print("hs_div_reg = {}".format(hs_div_reg))

        # register sequence
        new_regs += [(137, 0b10000)] # freeze DCO
        new_regs += set_rfreq_n1(rfreq, n1_reg)
        new_regs += [(7, (hs_div_reg & 0b111) << 5 | (n1_reg & 0b1111100) >> 2)]
        new_regs += [(137, 0)] # unfreeze DCO
        new_regs += [(137, 0b100000)] # Apply new frequency
    update_device(device, new_regs)


# Issue reset signal
def reset(device):
    update_device(device, [(135, 0x80)])


# Issue recall signal
def load_nvm(device):
    update_device(device, [(135, 0x01), (135, 0x00)])


# Calculate actual fxtal value for this device
def compute_fxtal(device, data):
    return device["freq_MHz"] * data["HS_DIV"] * data["N1"] / data["RFREQ"]


#Reset the clock and get default frequency
def read_default_frequency(device):
    load_nvm(device)
    data = read_device(device)
    return data["FOUT"]
