import os, pdb
from os import path

from flask import Flask, render_template, request, redirect, url_for
from flask import send_from_directory, make_response

from flask_restful import Api

from flask_bootstrap import Bootstrap5 as Bootstrap

from controllers.si570 import SI570_Controller as SI570
from controllers.sfp import SFP_Controller as SFP
from controllers.sysmon import SYSMON_Controller as SYSMON
from controllers.ina226 import INA226_Controller as INA226
from controllers.ccf_clocks import CCF_ClocksController as CCF_Clocks
from controllers.gpio import GPIO_Controller as GPIO
from controllers.adm106x import ADM106x_Controller as ADM106x
from controllers.adm1266 import ADM1266_Controller as ADM1266
from controllers.vivado_ddrmc import Vivado_DDRMC_Controller as Vivado_DDRMC
from controllers.vivado_ibert import Vivado_IBERT_Controller as Vivado_IBERT
from controllers.chipscopy_ddrmc import Chipscopy_DDRMC_Controller as Chipscopy_DDRMC
from controllers.chipscopy_ibert import Chipscopy_IBERT_Controller as Chipscopy_IBERT
from controllers.chipscopy_eye_scan import ChipscopyEyeScanController as ChipscopyEyeScan
from controllers.si5345 import SI5345_Controller as SI5345
from controllers.si53156 import SI53156_Controller as SI53156
from controllers.self_test import SelfTestController as SelfTest

from models.board_id import BoardID

from rest_api import ina226 as INA226_API
from rest_api import si570 as SI570_API
from rest_api import sysmon as SYSMON_API

from util.vivado import Vivado
from util.chipscopy import ChipscopyWrapper, ChipscopyController

from pydoc import locate

def create_app(name, config):
    app = Flask(name)
    app.config.update(config)
    board_config = app.config["USER_BOARD"]

    api = Api(app)
    Bootstrap(app)

    self_test_models = {}

    # Add routes

    @app.context_processor
    def inject_config():
        return dict(config=board_config)

    @app.route("/")
    def root():
        return render_template("index.html")


    if "board_id" in board_config:
        board_id = BoardID(board_config['board_id'])
        self_test_models["board_id"] = board_id
        @app.context_processor
        def inject_board_id():
            return dict(board_id=board_id.get_board_info())


    if "si570" in board_config:
        with app.app_context():
            si570 = SI570(board_config["si570"])
        self_test_models["si570"] = si570.model
        @app.route('/si570')
        def si570_read_all():
            return si570.read_all()

        @app.route("/si570/set", methods=["POST"])
        def si570_set():
            return si570.set_frequency()

        @app.route("/si570/load", methods=["POST"])
        def si570_load_nvm():
            return si570.load_nvm()

        @app.route("/si570/reset", methods=["POST"])
        def si570_reset():
            return si570.reset()

        api.add_resource(SI570_API.SI570_List, "/api/si570",
            resource_class_kwargs={"model" : si570.model})


    if "ccf_clocks" in board_config:
        ccf_clocks = CCF_Clocks(board_config["ccf_clocks"])
        @app.route('/ccf_clocks')
        def ccf_clocks_read_all():
            return ccf_clocks.read_all()


    if "sfp" in board_config:
        with app.app_context():
            sfp = SFP(board_config["sfp"])
        self_test_models["sfp"] = sfp.model
        @app.route('/sfp')
        def sfp_read_all():
            return sfp.read_all()

        @app.route("/sfp/configure/<device_name>", methods=["POST"])
        def sfp_configure(device_name):
            return sfp.configure(device_name)

        @app.route("/sfp/reset", methods=["POST"])
        def sfp_reset():
            return sfp.reset()

        # api.add_resource(SFP_API.SFP_List, "/api/sfp",
        #     resource_class_kwargs={"model" : sfp.model})


    if "hwmon" in board_config:
        sysmon = SYSMON(board_config["hwmon"])
        self_test_models["hwmon"] = sysmon.model
        @app.route('/hwmon', methods=['GET'])
        def sysmon_read_all():
            return sysmon.read_all()

        # api.add_resource(SYSMON_API.SYSMON_List, "/api/sysmon",
        #     resource_class_kwargs={"model" : sysmon.model})


    if "ina226" in board_config:
        ina226 = INA226(board_config["ina226"])
        self_test_models["ina226"] = ina226.model
        @app.route('/ina226')
        def ina226_read_all():
            return ina226.read_all()

        api.add_resource(INA226_API.INA226_List, "/api/ina226",
            resource_class_kwargs={"model" : ina226.model})


    if "gpio" in board_config:
        gpio = GPIO(board_config["gpio"])
        self_test_models["gpio"] = gpio.model

        @app.route("/gpio")
        def gpio_read_all():
            return gpio.read_all()

        @app.route("/gpio/set", methods=["POST"])
        def gpio_set():
            return gpio.set()


    if "adm106x" in board_config:
        with app.app_context():
            adm106x = ADM106x(board_config["adm106x"])
            self_test_models["adm106x"] = adm106x.model
            
            @app.route("/adm106x")
            def adm106x_read_all():
                return adm106x.read_all()

    if "adm1266" in board_config:
        with app.app_context():
            adm1266 = ADM1266(board_config["adm1266"])
            self_test_models["adm1266"] = adm1266.model
            
            @app.route("/adm1266")
            def adm1266_read_all():
                return adm1266.read_all()

    if "si5345" in board_config:
        with app.app_context():
            si5345 = SI5345(board_config["si5345"])
        
        @app.route("/si5345")
        def si5345_read_all():
            return si5345.read_all()

        @app.route("/si5345/input_config", methods=["POST"])
        def si5345_input_config():
            return si5345.update_input_config()

        @app.route("/si5345/select_preset", methods=["POST"])
        def si5345_select_preset(use_default = False):
            return si5345.select_preset_configuration()

        @app.route("/si5345/upload_config", methods=["POST"])
        def si5345_upload_config():
            return si5345.upload_register_configuration()

        @app.route("/si5345/soft_reset/<device_name>", methods=["POST"])
        def si5345_soft_reset(device_name):
            return si5345.soft_rst(device_name)

        @app.route("/si5345/hard_reset/<device_name>", methods=["POST"])
        def si5345_hard_reset(device_name):
            return si5345.hard_rst(device_name)

    if "si5345" in board_config and "sfp" in board_config:
        @app.route("/configure_firefly", methods=["POST"])
        def configure_firefly():
            si5345.load_default_preset()
            sfp.configure_all()
            sfp.reset()
            return redirect(request.referrer)


    if "si53156" in board_config:
        with app.app_context():
            si53156 = SI53156(board_config["si53156"])
        self_test_models["si53156"] = si53156.model
        
        @app.route("/si53156")
        def si53156_read_all():
            return si53156.read_all()

        @app.route("/si53156/configure", methods=["POST"])
        def si53156_configure():
            return si53156.configure()



    # Initialize user-space test pages and api
    for key, resource in board_config["process_runners"].items():
        name = resource["name"]
        
        controller_path = "controllers.{}.{}Controller".format(name, name.capitalize())
        api_path = "rest_api.{}".format(name)
        resource["app"] = app

        controller = locate(controller_path)(resource)        
        app.add_url_rule("/{}".format(name), name, view_func=controller.get_status)
        app.add_url_rule("/{}/start".format(name), "{}_start".format(name),
            view_func=controller.start_test, methods=["POST"])
        app.add_url_rule("/{}/stop".format(name), "{}_stop".format(name),
            view_func=controller.stop_test, methods=["POST"])

        api_status = locate("{}.{}Status".format(api_path, name.capitalize()))
        api_start = locate("{}.{}Start".format(api_path, name.capitalize()))
        api_stop = locate("{}.{}Stop".format(api_path, name.capitalize()))
        model = controller.model
        api.add_resource(api_status, "/api/{}".format(name),
            resource_class_kwargs={"model" : model})
        api.add_resource(api_start, "/api/{}/start".format(name),
            resource_class_kwargs={"model" : model})
        api.add_resource(api_stop, "/api/{}/stop".format(name),
            resource_class_kwargs={"model" : model})


    if "chipscopy" in board_config:
        chipscopy = ChipscopyWrapper(board_config["chipscopy"])
        chipscopy_controller = ChipscopyController(chipscopy)
        @app.route("/chipscopy/connect", methods=["GET", "POST"])
        def chipscopy_connect():
            return chipscopy_controller.connect()

        @app.route("/chipscopy/disconnect", methods=["POST"])
        def chipscopy_disconnect():
            return chipscopy_controller.disconnect()
    else:
        @app.route("/chipscopy/connect", methods=["GET", "POST"])
        def chipscopy_connect():
            err = "Chipscopy settings are missing, please fix your board configuration!"
            return render_template("error.html", error=err)

    if "chipscopy-ddrmc" in board_config:
        chipscopy_ddrmc = Chipscopy_DDRMC(board_config["chipscopy-ddrmc"], chipscopy)
        @app.route("/ddrmc")
        def chipscopy_ddrmc_read_all():
            return chipscopy_ddrmc.read_all()


    if "chipscopy-ibert" in board_config:
        chipscopy_ibert = Chipscopy_IBERT(board_config["chipscopy-ibert"], chipscopy)
        chipscopy_eye_scan = ChipscopyEyeScan(board_config["chipscopy-ibert"], chipscopy,
            chipscopy_ibert.model)
        @app.route("/ibert")
        def chipscopy_ibert_read_all():
            return chipscopy_ibert.read_all()

        @app.route("/ibert/configure", methods=["POST"])
        def chipscopy_configure():
            return chipscopy_ibert.update_ibert_settings()

        @app.route("/ibert/tx_reset", methods=["POST"])
        def chipscopy_ibert_tx_reset():
            return chipscopy_ibert.tx_reset()

        @app.route("/ibert/rx_reset", methods=["POST"])
        def chipscopy_ibert_rx_reset():
            return chipscopy_ibert.rx_reset()

        @app.route("/ibert/ibert_reset", methods=["POST"])
        def chipscopy_ibert_reset():
            return chipscopy_ibert.ibert_reset()

        @app.route("/eye_scan")
        def chipscopy_eye_scan_read_all():
            return chipscopy_eye_scan.read_all()

        @app.route("/eye_scan/run_scan", methods=["POST"])
        def chipscopy_eye_scan_run():
            return chipscopy_eye_scan.run_scan()

        @app.route("/ibert_eye_scan/<scan>/<link>.png")
        def chipscopy_eye_scan_png(scan, link):
            image = chipscopy_eye_scan.plot_png(scan, link)
            response = make_response(image)
            response.headers.set('Content-Type', 'image/jpeg')
            response.headers.set('Content-Disposition', 'attachment',
                filename="{}_{}.png".format(scan, link))
            return response


    if "vivado" in board_config:
        vivado = Vivado(board_config["vivado"])
        @app.route("/vivado/connect", methods=["POST"])
        def vivado_connect(vivado=vivado):
            destination = request.form.get("redirect", "/")
            try:
                vivado.connect()
                return redirect(destination)
            except Exception as err:
                return render_template("vivado_connect.html", error=str(err),
                    redirect=destination, bad_configuration=False)

        @app.route("/vivado/download_script")
        def vivado_download_script(app=app):
            vivado = path.join(app.root_path, "vivado")
            return send_from_directory(vivado, "remote.tcl", as_attachment=True)

    else:
        @app.route("/vivado/connect", methods=["POST"])
        def vivado_connect():
            destination = request.form.get("redirect", "/")
            return render_template("vivado_connect.html",
                error="Vivado configuration is missing from the board definition file",
                redirect=destination, bad_configuration=True)


    if "vivado-ddrmc" in board_config:
        vivado_ddrmc = Vivado_DDRMC(board_config["vivado-ddrmc"], vivado)
        @app.route("/ddrmc")
        def vivado_ddrmc_read_all():
            return vivado_ddrmc.read_all()


    if "vivado-ibert" in board_config:
        vivado_ibert = Vivado_IBERT(board_config["vivado-ibert"], vivado)
        @app.route("/ibert")
        def vivado_ibert_read_all():
            return vivado_ibert.read_all()

        @app.route("/ibert/tx_reset", methods=["POST"])
        def vivado_ibert_tx_reset():
            return vivado_ibert.tx_reset()

        @app.route("/ibert/rx_reset", methods=["POST"])
        def vivado_ibert_rx_reset():
            return vivado_ibert.rx_reset()

        @app.route("/ibert/ibert_reset", methods=["POST"])
        def vivado_ibert_reset():
            return vivado_ibert.ibert_reset()

        @app.route("/ibert/run_scan", methods=["POST"])
        def vivado_ibert_run_scan():
            return vivado_ibert.run_scan()

        @app.route("/ibert_eye_scan/<scan>/<link>.png")
        def vivado_ibert_eye_scan_png(scan, link):
            image = vivado_ibert.plot_png(scan, link)
            response = make_response(image)
            response.headers.set('Content-Type', 'image/jpeg')
            response.headers.set('Content-Disposition', 'attachment',
                filename="{}_{}.png".format(scan, link))
            return response


    if "self_test" in board_config:

        if not "board_id" in board_config:
            raise RuntimeError("'board_id' definition is mission from the board configuration file!"
                + " board_id is required for the self-test to work!")

        self_test = SelfTest(board_config['self_test'], self_test_models)
        @app.route("/self_test")
        def self_test_status():
            return self_test.status()

        @app.route("/self_test/start", methods=["POST"])
        def self_test_start():
            return self_test.start()

        @app.route("/self_test/stop", methods=["POST"])
        def self_test_stop():
            return self_test.stop()

        @app.route("/self_test/download_report")
        def self_test_download_report():
            return self_test.download_json_results()

    return app, api