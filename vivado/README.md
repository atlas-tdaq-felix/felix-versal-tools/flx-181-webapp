# Vivado remote access

The purpose of the script is to allow DUT to remotely execute TCL commands in Vivado to allow for automated FLX-181 hardware testing. Certain properties, such as DDRMC margins on Versal, are not directly accessible by PS. Vivado remote connection allows the DUT to read any value available via JTAG interface.

Source this script from Vivado Tcl Console in order to enable the server. It will  listen to TCP 127.0.0.1:8020. Any TCL command sent into the socket will be evaluated, and the result is sent back to the connection.

The server is listening on the *localhost only* to prevent unauthorized users from evaluating arbitrary TLC commands on the development machine. To access the server from DUT, reverse-tunnel the port 8020 from the development machine to the DUT via ssh.

To shut down the script send `exit` command to the socket, for example run this on the machine with Vivado:

```
echo exit | nc 127.0.0.1 8020
```

The script is based on https://www.tcl.tk/about/netserver.html

# Support

Please contact Elena Zhivun <elena.zhivun@cern.ch> if you have any questions.