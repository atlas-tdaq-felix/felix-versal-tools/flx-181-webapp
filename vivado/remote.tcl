# The purpose of the script is remotely executing TCL commands
# for automated device testing. Source this script from Vivado
# Tcl Console in order to enable the server.
#
# This script will open 127.0.0.1:8020 and listen for 
# TCL commands. The incoming commands are evaluated, and the
# result is sent back to the connection.
#
# Author: Elena Zhivun <elena.zhivun@cern.ch>
#
# Based on https://www.tcl.tk/about/netserver.html

# To shutdown, send 'exit' command to the socket
# Linux command: 'echo exit | nc 127.0.0.1 8020'

# command server --
#	Open the server listening socket
#	and enter the Tcl event loop
#
# The server is listening on the *localhost only* to avoid 
# security issues, i.e. unauthorized users evaluating
# arbitrary TLC commands on the development machine.
#
# To access the server from DUT, reverse-tunnel the port 
# from the development machine to the DUT via ssh

proc CmdServer {} {
	set port 8020
	global state
	global server_socket
	set state "ready"
	puts "Waiting for TCL commands on 127.0.0.1:$port"
	puts "To shutdown, send 'exit' command to the socket"
	puts "Linux command: 'echo exit | nc 127.0.0.1 $port'"
    set server_socket [socket -server AcceptConnection -myaddr "127.0.0.1" $port]
    while {1} {
    	vwait state
    	if {$state == "exit"} {
    		puts "Shutting down..."
    		break
    	}
    }
    close $server_socket
}

# AcceptConnection --
#	Accept a connection from a new client.
#	This is called after a new socket connection
#	has been created by Tcl.
#
# Arguments:
#	sock	The new socket connection to the client
#	addr	The client's IP address
#	port	The client's port number
	
proc AcceptConnection {sock addr port} {
    global echo, state

    # Record the client's information

    puts "Accepted connection $sock from $addr port $port"
    set echo(addr,$sock) [list $addr $port]    
    set state "accepted"

    # Ensure that each "puts" by the server
    # results in a network transmission

    fconfigure $sock -buffering line

    # Set up a callback for when the client sends data

    fileevent $sock readable [list ServerEventLoop $sock]
}

# CommandEventLoop --
#	This procedure is called when the server
#	can read data from the client
#
# Arguments:
#	sock	The socket connection to the client

proc ServerEventLoop {sock} {
    global echo, state

    # Check end of file or abnormal connection drop,
    # then echo data back to the client.

    if {[eof $sock] || [catch {gets $sock line}]} {
		puts "Close connection $sock"
		close $sock
		set state "closed"
    } else {
    	set t [clock milliseconds]
		set timestamp [format "%s.%03d" \
                  [clock format [expr {$t / 1000}] -format %T] \
                  [expr {$t % 1000}] \
              ]

        if {$line != "place_ready_marker"} {
			puts "\[remote.tcl $timestamp\]: $line"
        }	
        	
		if {$line == "exit"} {
			set state "exit"
			close $sock
		} else {
			if {[catch {puts $sock [eval $line]} errmsg]} {
				puts $sock "!!!EXCEPTION!!!$errmsg"
			}
		}
    }
}


proc place_ready_marker {} { return "!!!READY!!!" }


proc return_file_data {filename} {
	set fp [open $filename r]
	set file_data [read $fp]
	close $fp
	return $file_data
}


# Launch the TCL command server
CmdServer 