open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target
current_hw_device [get_hw_devices xcvm1802_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xcvm1802_1] 0]