# Interface for the automated self-test runner

class SelfTestExecutor():
	
	def self_test_before(self, **kwargs):
		pass


	def self_test_main(self, **kwargs):
		return self.read_all()


	def self_test_after(self, **kwargs):
		pass

	def self_test_skip_step(self, **kwargs):
		pass


	def self_test_check_result(self, result):
		if result is None or not result:
			return { "status" : "error", "result" : "Test results are missing" }

		return { "status" : "notice", "result" : "Readout completed, self-test criteria are not defined" }


	def self_test_configuration(self):
		if not "self_test" in self.config:
			return {}
			
		return self.config["self_test"]


	def self_test_check_device_list(self, result,
		device_getter=lambda x: x.config["mapping"],
		human_readable_name_getter=None):

		if result is None or not result:
			return { "status" : "error", "result" : "No test results available" }
			
		devices_with_errors = []
		devices_with_missing_data = []

		devices = device_getter(self)
		for device_name, config in devices.items():
			if human_readable_name_getter is None:
				human_readable_name = device_name
			else:
				human_readable_name = human_readable_name_getter(device_name)

			if device_name in result:
				if "error" in result[device_name]:
					devices_with_errors.append(human_readable_name)
			else:
				devices_with_missing_data.append(human_readable_name)
		
		if devices_with_errors or devices_with_missing_data:
			status = "notice"
			results = []
			self_test_config = self.self_test_configuration()

			if devices_with_errors:				
				results.append("Error reading out " + ", ".join(devices_with_errors))
				if "warn_on_error" in self_test_config and self_test_config["warn_on_error"]:
					status = "warning"
				else:
					status = "error"

			if devices_with_missing_data:
				status = "error"
				results.append("Missing data from " + ", ".join(devices_with_missing_data))

			return {"status" : status, "result" : "; ".join(results)}
				

		return { "status" : "success", "result" : "Readout completed" }



def field_present(result, field):
	return field in result and result[field]


