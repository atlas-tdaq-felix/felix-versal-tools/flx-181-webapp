from util.vivado import Vivado
import pdb

class Vivado_DDRMC():
	def __init__(self, config, vivado):
		self.config = config
		self.vivado = vivado
		self.cores = None
		

	def get_cores(self):
		result = {}
		try:
			for core in self.config["cores"]:
				ddrmcs = self.vivado.get_hw_ddrmcs(mask="*{}".format(core))
				if ddrmcs:
					result[core] = ddrmcs[0]
			self.cores = result

		except Exception as err:
			self.cores = None
			raise Exception("Can't retrieve DDRMC cores: {}".format(err))



	def read_all(self):
		try:
			if self.vivado is None:
				return {"error" : "Connection configuration is missing, check the board definition file!"}

			if not self.vivado.connected:
				self.vivado.connect()

			if self.cores is None:
				self.get_cores()
		except Exception as err:
			return {"error" : str(err)}

		result = {}
		for name, core in self.cores.items():
			try:
				report = core.report()
				properties = core.properties()
				result[name] = { "report" : report, "properties" : properties }
			except ConnectionError as err:
				self.cores = None
				return {"error" : "Connection dropped"}

			except Exception as err:
				result[name] = { "error" : str(err)}
		return result

