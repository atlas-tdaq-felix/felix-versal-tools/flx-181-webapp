import pdb
from functools import wraps

from util.si570 import read_device, set_frequency, reset, load_nvm
from util.si570 import fmin_MHz, fmax_MHz, compute_fxtal, fxtal_MHz_nominal, read_default_frequency

from util.i2c import I2CError
from threading import Lock
import math

from models.self_test_executor import SelfTestExecutor

class SI570(SelfTestExecutor):

    def __init__(self, config):
        self.config = config
        self.devices = config["mapping"]
        self.update_fxtal()


    def ensure_device_exists(func):
        '''
            Decorator to verify that the device with given device_id
            is present
        '''
        @wraps(func)
        def wrapper(self, device_id, *args, **kwargs):
            if not device_id in self.devices:
                return {"error" : ("SI570 device {} is not defined in "
                    + "configuration files for this board".format(device_id))}
            try:
                return func(self, device_id, *args, **kwargs)
            except I2CError as err:
                return {"error" : ("Can't read SI570 device "
                    + "{}: {}".format(device_id, err))}
        return wrapper


    # return configuration of the selected SI570 device
    @ensure_device_exists
    def read(self, device_id):
        return self.read_device_helper(device_id)


    # Load non-volatile memory of the SI570 device
    @ensure_device_exists
    def load_nvm(self, device_id):
        device = self.devices[device_id]
        if device["protected"]:
            return {"error" : ("Cannot recall settings for {} clock "
                + "because it will cause system instability").format(device_id)}

        load_nvm(device)
        return {}


    # Send reset to the SI570 device
    @ensure_device_exists
    def reset(self, device_id):
        device = self.devices[device_id]
        if device["protected"]:
            return {"error" : ("Cannot reset {} clock because it will "
                + "cause system instability").format(device_id)}

        reset(device)
        return {}
        

    # sets the frequency of the selected device
    @ensure_device_exists
    def set_frequency(self, device_id, freq_MHz):
        device = self.devices[device_id]
        if device["protected"]:
            return {"error" : ("Cannot reconfigure {} clock because it will "
                + "cause system instability").format(device_id)}

        try:
            freq = float(freq_MHz)
        except Exception as err:
            return {"error" : "Invalid frequency format: {}".format(freq_MHz)}

        if freq < fmin_MHz or freq > fmax_MHz:
            return {"error" : ("Requested output frequency is out of range "
                + "for SI570: {}. Frequency must be in range between {} MHz "
                + "and {} MHz").format(freq_MHz, fmin_MHz, fmax_MHz)}

        # read the current configuration of the device
        data = self.read(device_id)
        if "error" in data:
            return data

        try:
            set_frequency(device, freq, data)
        except Exception as err:
            return {"error" : err}

        return {}


    # Retrieve device data by id, but without ensuring that the device exists
    def read_device_helper(self, device_id):
        device = self.devices[device_id]
        result = read_device(device)
        if "error" in device:
            result["error"] = device["error"]
        return result

        
    # returns configuration of all known SI570 devices
    def read_all(self, json_compatible=False):
        data = {}
        
        for device_id, device in self.devices.items():
            try:
                data[device_id] = self.read_device_helper(device_id)
            except I2CError as err:
                if json_compatible:
                    data[device_id] = { "error" : f"{err}" }
                else:
                    data[device_id] = {
                        "error" : f"{err}",
                        "FOUT" : float("NaN"),
                        "RFREQ" : float("NaN"),
                        "FDCO" : float("NaN"),
                        "FXTAL" : float("NaN"),
                        "HS_DIV" : float("NaN"),
                        "N1" : float("NaN"),
                    }

        return data


    # Reset the oscillator to default settings, compute fxtal
    # if the frequency is within expected range
    def update_fxtal(self):
        for device_id, device in self.devices.items():
            try:
                if not device["protected"]:
                    load_nvm(device)

                data = read_device(device)
                f_actual = data["FOUT"]
                f_expected = device["freq_MHz"]

                if math.isclose(f_actual, f_expected, rel_tol=5e-2):
                    fxtal = compute_fxtal(device, data)
                else:
                    device["error"] = (f'Expected output frequency: '
                        + f'{f_expected:.3f} MHz, actual {f_actual:.3f} MHz')
                    fxtal = fxtal_MHz_nominal
                
                device["fxtal"] = fxtal
            except I2CError as err:
                pass


    def self_test_main(self, **kwargs):
        result = self.read_all(json_compatible=True)
        return result


    def compare_clocks(self, name, fout, fref, ppm):
        if abs(fout - fref) > 2*ppm*1e-6*fref:
            return "Clock {} at {:.6f} MHz instead of {:.6f}".format(name, fout, fref)
        else:
            return "OK"


    def self_test_check_result(self, result):
        device_check = self.self_test_check_device_list(result)
        device_check['result'] = [device_check['result']]
        for device in result:
            reading = result[device]
            if "error" in reading:
                continue
            else:
                fout = reading['FOUT']
                fref = self.config["mapping"][device]["freq_MHz"]
                ppm  = float(self.config["mapping"][device]["stability"].strip('ppm'))
                check = "OK" #self.compare_clocks(device, fout, fref, ppm)
                if check != "OK":
                    device_check['status'] = "error"
                    device_check['result'].append(check)

        return { "status" : device_check['status'], "result" : device_check['result']}
        

