import os, re, pdb
from .process_runner import ProcessRunner


class Mtd(ProcessRunner):
	def __init__(self, config):
		ProcessRunner.__init__(self, config)
		

	def start_test(self, **kwargs):	
		print("in model.start_test(), kwargs={}".format(kwargs))
		device_id = kwargs["device_id"] if "device_id" in kwargs else ""
		if not device_id:
			return { "error" : "Please specify MTD device ID"}

		args = []
		if "speed_test" in kwargs:
			args += ["-s"]

		args += ["-d", str(device_id)]

		print("in model.start_test(), args={}".format(args))

		return super().start_test(args=args)


