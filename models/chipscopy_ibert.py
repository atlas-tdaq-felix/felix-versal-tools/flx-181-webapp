#from util.chipscopy import ChipscopyWrapper
from functools import wraps
from more_itertools import one
from threading import Lock, Thread
import queue
from queue import SimpleQueue
from chipscopy.api.ibert import create_eye_scans, create_links, delete_links
from chipscopy.api.ibert.link.manager import LinkManager
from chipscopy.api.ibert.aliases import (
	EYE_SCAN_HORZ_RANGE,
	EYE_SCAN_VERT_RANGE,
	EYE_SCAN_VERT_STEP,
	EYE_SCAN_HORZ_STEP,
	EYE_SCAN_TARGET_BER,
	PATTERN,
	RX_LOOPBACK,
)
import copy, pdb
from datetime import datetime, timedelta
from time import sleep


class Chipscopy_IBERT():
	def __init__(self, config, chipscopy):
		self.config = config
		self.chipscopy = chipscopy
		self.links = None
		self.scans = None
		link_names = list(self.config["gt_links"].keys())
		self.eye_scan_cache = Cache(link_names)
		self.property_cache = Cache(link_names)
		self.chipscopy_lock = chipscopy.lock

		# it seems like only 4 scans can run at a time
		# this background thread will take care of the scan scheduling
		self.scheduler_lock = Lock()
		self.scheduler_thread = Thread()
		self.scheduler_errors = []
		self.scheduler_progress = {'status' : 'Idle', "scans_started" : 0,
			     "scans_total" : 0, "scans_skipped" : 0}
		self.scheduler_kwargs = {}


	def on_disconnect(self):
		self.links = None
		self.scans = None


	def create_gt_link(self, name, params):
		'''
			Creates a new GT link and configures rx and tx with specified parameters
		'''
		with self.chipscopy_lock:
			tx_channel = params["tx"]["channel"]
			rx_channel = params["rx"]["channel"]
			tx_quad = params["tx"]["quad"]
			rx_quad = params["rx"]["quad"]
			tx_links = self.chipscopy.ibert.gt_groups.filter_by(name=tx_quad)
			rx_links = self.chipscopy.ibert.gt_groups.filter_by(name=rx_quad)
			if not tx_links or not rx_links:
				raise RuntimeError(f"Could not create link {tx_quad}.CH_{tx_channel} -> {rx_quad}.CH_{rx_channel}, "
					+ "is it present in the desing?")
			tx_quad = one(tx_links)		
			rx_quad = one(rx_links)		
			gt_link = one(create_links(txs=[tx_quad.gts[tx_channel].tx], rxs=[rx_quad.gts[rx_channel].rx]))

			if self.config["apply_default_link_config_on_start_up"]:
				set_properties(gt_link.rx, self.config["default_link_config"]["rx"])
				set_properties(gt_link.tx, self.config["default_link_config"]["tx"])
			
			return gt_link


	def create_gt_links(self):
		'''
			Find or create all GT links specified in the configuration
		'''		
		with self.chipscopy_lock:
			self.links = {}
			delete_links()

		for name, params in self.config["gt_links"].items():
			try:
				self.links[name] = self.create_gt_link(name, params)
			except Exception as err:
				self.links[name] = None
				# print(f"Could not create GT link {name}: {err}")


	def create_scan_set(self, params):
		'''
			Generate a set of scans for all links specified in configuration
			for the selected scan parameters
		'''		
		result = {}
		for link_name in params['links']:
			link = self.links[link_name]
			if link is None:
				continue

			with self.chipscopy_lock:
				scan = one(create_eye_scans(target_objs=[link]))
				for name, value in params["scan"].items():
					scan.params[name].value = value
					
			result[link_name] = scan
		return result


	def create_scans(self):
		'''
			Create all scans specified in the configuration
		'''
		self.scans = {}
		try:
			for name, params in self.config["scans"].items():				
				self.scans[name] = self.create_scan_set(params)	
		except Exception as err:
			self.scans = None
			raise Exception("Can't find or create eye scans: {}".format(err))


	def ensure_chipscopy_connection(func):
		'''
			Decorator to verify that the Chipscopy connection works correctly
		'''
		@wraps(func)
		def wrapper(self, *args, **kwargs):
			if self.chipscopy is None:
				return { "error" : "Connection configuration is missing, check the board definition file!" }

			if not self.chipscopy.connected():
				try:
					self.chipscopy.connect()					
				except Exception as err:
					self.on_disconnect()
					return {"error" : str(err), 'disconnected' : True}

			if self.chipscopy.ibert is None:
				return {"error" : "cs_server didn't find any IBERT cores, please check the connection and device settings"}

			try:				
				return func(self, *args, **kwargs)
			except Exception as err:
				self.on_disconnect()
				return { "error" : str(err)}

		return wrapper


	def ensure_links_exist(func):
		'''
			Decorator to ensure all links have been created
		'''
		@wraps(func)
		def wrapper(self, *args, **kwargs):
			if self.links is None:
				self.create_gt_links()
				self.scans = None
				self.eye_scan_cache.purge_all()
				self.property_cache.purge_all()

			if self.scans is None:
				self.create_scans()
				self.eye_scan_cache.purge_all()

			return func(self, *args, **kwargs)

		return wrapper


	@ensure_chipscopy_connection
	def update_link_settings(self, link_settings):		
		settings_applied = {}
		for link, settings in link_settings.items():
			if not link in self.links or self.links[link] is None:
				continue

			with self.chipscopy_lock:
				gt_link = self.links[link]			
				set_properties(gt_link.rx, settings["rx"])
				set_properties(gt_link.tx, settings["tx"])
				settings_applied[link] = settings

		return { "status" : "Updated IBERT settings", "settings_applied" : settings_applied }


	@ensure_chipscopy_connection
	@ensure_links_exist
	def ibert_reset(self):
		'''
			Reset BERT data
		'''
		for name, link in self.links.items():
			if link is None:
				continue

			with self.chipscopy_lock:
				link.ibert.reset()
		return { "status" : "Reset IBERT debug core" }


	@ensure_chipscopy_connection
	@ensure_links_exist
	def tx_reset(self):
		'''
			Reset TX link
		'''
		for name, link in self.links.items():
			if link is None:
				continue

			with self.chipscopy_lock:
				link.tx.reset()
		return { "status" : "Issued TX reset to all GT links" }


	@ensure_chipscopy_connection
	@ensure_links_exist
	def rx_reset(self):
		'''
			Reset RX link
		'''
		for name, link in self.links.items():
			if link is None:
				continue

			with self.chipscopy_lock:
				link.rx.reset()
		return { "status" : "Issued RX reset to all GT links" }


	@ensure_chipscopy_connection
	@ensure_links_exist
	def get_ibert_data(self):
		'''
			Returns all BERT data (Serial I/O link properties)
		'''
		result = {}
				
		for name, link in self.links.items():
			if link is None:
				result[name] = { "error" : "GT Link is not instantiated in the design" }
				continue

			data = {}

			with self.chipscopy_lock:
				rx_props = get_property_reports(link.rx,
					[RX_LOOPBACK, PATTERN, "Termination Voltage"])
				tx_props = get_property_reports(link.tx,
					[PATTERN, "Pre Cursor", "Post Cursor", "Differential Control"])
				tx_endpoint = str(link.tx.handle).split(".", maxsplit=1)[1]
				rx_endpoint = str(link.rx.handle).split(".", maxsplit=1)[1]	
				rx_status = link.status
				error_count = link.error_count
				link_ber = link.ber

			data["TX_ENDPOINT"] = tx_endpoint
			data["RX_ENDPOINT"] = rx_endpoint
			data["RX_LOOPBACK"] = rx_props[RX_LOOPBACK]
			data["RX_STATUS"] = rx_status
			try:
				data["RX_PRBS_ERR_CNT"] = float(error_count)
			except:
				data["RX_PRBS_ERR_CNT"] = float("NaN")
			data["RX_PRBS_BER"] = link_ber
			data["TX_PRE_CURSOR"] = tx_props["Pre Cursor"]
			data["TX_POST_CURSOR"] = tx_props["Post Cursor"]
			data["TX_DIFF_CTRL"] = tx_props["Differential Control"]
			data["RX_TERMINATION_VOLTAGE"] = rx_props["Termination Voltage"]
			data["TX_PATTERN"] = tx_props[PATTERN]
			data["RX_PATTERN"] = rx_props[PATTERN]

			result[name] = data
		return result


	@ensure_chipscopy_connection
	@ensure_links_exist
	def run_scan(self, scan_name):
		if not scan_name in self.config["scans"]:
			return { "status" : f"Scan {scan_name} is not defined in configuration", "error" : True }

		self.eye_scan_cache.purge(scan_name)		
		scans_total = len(self.config["scans"][scan_name]['links'])

		# call the scheduler
		with self.scheduler_lock:
			if self.scheduler_thread.is_alive():
				return {"status" : f"Scan {self.schedule_scans_task['scan_name']} is already being scheduled!",
	    			"error" : True }

			self.scheduler_progress = {'status' : 'Scheduled', 'scans_started' : 0,
			      'scans_skipped' : 0, 'scans_total' : scans_total }
			self.scheduler_kwargs = { 'scan_name' : scan_name, 'config' : copy.deepcopy(self.config)}
			self.scheduler_thread = Thread(target=self.schedule_scans_task, daemon=True)		
			self.scheduler_thread.start()

		return { "status" : "Scheduled scan {}".format(self.config["scans"][scan_name]['comment']),
	  		"error" : False }


	def is_scheduler_running(self):
		with self.scheduler_lock:
			return self.scheduler_thread.is_alive()
		

	def get_scheduler_errors(self):
		with self.scheduler_lock:
			return copy.deepcopy(self.scheduler_errors)


	def get_scheduler_progress(self):
		with self.scheduler_lock:
			return copy.deepcopy(self.scheduler_progress)


	def schedule_scans_task(self):
		valid_links = []

		with self.scheduler_lock:
			scan_name = self.scheduler_kwargs['scan_name']
			config = self.scheduler_kwargs['config']
			scan_config = config["scans"][scan_name]
			self.scheduler_errors = []
			self.scheduler_progress['status'] = 'Running'

		# pre-configure all links 
		for link_name in scan_config["links"]:
			link = self.links[link_name]
			if link is None:
				with self.scheduler_lock:
					self.scheduler_progress['scans_skipped'] += 1
					self.scheduler_errors.append(
						f"Skipping link {link_name} because it is not present in the design")
				continue
			
			if not self.plls_are_locked(link):
				with self.scheduler_lock:
					self.scheduler_progress['scans_skipped'] += 1
					self.scheduler_errors.append(
						f"Skipping link {link_name} because the PLLs are not locked")
				continue

			try:
				with self.chipscopy_lock:
					set_properties(link.tx, scan_config["tx"])
			except Exception as err:
				with self.scheduler_lock:
					self.scheduler_errors\
						.append(f"Failed to apply settings to link {link_name} TX: {err}")

			try:
				with self.chipscopy_lock:
					set_properties(link.rx, scan_config["rx"])
			except Exception as err:
				self.scheduler_errors\
						.append(f"Failed to apply settings to link {link_name} RX: {err}")

			

			# record the link status before each scan
			sleep(0.1)
			with self.chipscopy_lock:
				scan = self.scans[scan_name][link_name]
				scan.ez_hw_test_link_status = link.status

			valid_links.append(link_name)

		# run the scans	
		running_scans = []
		running_quad = None
		finished_scans = SimpleQueue()

		for link_name in valid_links:
			scan = self.scans[scan_name][link_name]
			link_quad = config['gt_links'][link_name]['rx']['quad']
			if running_quad is None:
				running_quad = link_quad

			try:
				# wait for the previous quad if switched quads
				if config['scan_by_quad'] and running_quad != link_quad:
					while running_scans:						
						finished_scan = finished_scans.get(timeout=config['scan_timeout_seconds'])
						# print(f"Finished scan {finished_scan}")
						running_scans.remove(finished_scan)

				# start a new scan
				with self.chipscopy_lock:
					# print(f"Starting scan {scan} on link {link_name}")										
					scan.done_callback = lambda x: finished_scans.put(x)
					scan.start(show_progress_bar=False)	

				with self.scheduler_lock:
					self.scheduler_progress['scans_started'] += 1

				if config['scan_by_quad']:
					running_scans.append(scan)
				else:
					finished_scan = finished_scans.get(timeout=config['scan_timeout_seconds'])
					# print(f"Finished scan {finished_scan}")

			except queue.Empty:
				with self.scheduler_lock:
					msg = "Scans scheduler appears to be frozen, aborting the eye scans"
					print(msg)
					self.scheduler_errors.append(msg)
					self.scheduler_progress['status'] = 'Aborted'
				return

			# Chipscopy only throws generic Exception
			except Exception as err:
				with self.scheduler_lock:
					self.scheduler_progress['scans_skipped'] += 1
					msg = f"Skipping link {link_name} due to a {err}"
					print(msg)
					self.scheduler_errors.append(msg)

		# Wait for the remaining scans to complete
		if config['scan_by_quad']:
			while running_scans:				
				finished_scan = finished_scans.get(timeout=config['scan_timeout_seconds'])
				# print(f"Finished scan {finished_scan}")
				running_scans.remove(finished_scan)

		with self.scheduler_lock:
			self.scheduler_progress['status'] = 'Done'


	def pll_is_locked(self, txrx):
		with self.chipscopy_lock:
			pll = txrx.pll
			if not hasattr(pll, "locked"):
				return False
			return pll.locked


	def plls_are_locked(self, link):
		return self.pll_is_locked(link.tx) and self.pll_is_locked(link.rx)
		

	@ensure_chipscopy_connection
	@ensure_links_exist
	def get_scan_data(self):
		'''
			Returns eye scan data given scan name
		'''
		result = {}
		for scan_name, scan in self.scans.items():
			# print(f"Reading out data for scan {scan_name}")
			scan_data = { "links" : {} }
			result[scan_name] = scan_data
			scan_props = self.config["scans"][scan_name]
			scan_data["comment"] = scan_props["comment"]

			for link_name in scan_props["links"]:
				scan_data["links"][link_name] = self.get_scan_link_data(scan, scan_name, link_name)
		return result


	def get_scan_link_data(self, scan, scan_name, link_name):
		'''
			Returns eye scan data for a particular link and scan
		'''		

		if self.eye_scan_cache.contains(scan_name=scan_name, link_name=link_name):
			return self.eye_scan_cache.get(scan_name=scan_name, link_name=link_name)

		try:
			with self.chipscopy_lock:
				data = extract_data(scan[link_name])

			if data["data_ready"] and scan[link_name].status == "Done":
				self.eye_scan_cache.put(scan_name=scan_name, link_name=link_name, data=data)
		except Exception as err:
			data = {}

		return data

# This method should be only called when ChipScoPy lock is grabbed!
def set_properties(trx_object, aliases):
	props = {}
	for alias, value in aliases.items():
		props[trx_object.property_for_alias[alias]] = value
	trx_object.property.set(**props)
	trx_object.property.commit(list(props.keys()))


# This method should be only called when ChipScoPy lock is grabbed!
def get_properties(trx_object, aliases):
	names = {alias : trx_object.property_for_alias[alias] for alias in aliases}
	trx_object.property.refresh(names.values())
	values = trx_object.property.get(names.values())
	return {alias : values[names[alias]] for alias in aliases}


# This method should be only called when ChipScoPy lock is grabbed!
def get_property_reports(trx_object, aliases):
	names = {alias : trx_object.property_for_alias[alias] for alias in aliases}
	# t_start = datetime.now()
	trx_object.property.refresh(names.values())
	# t_refresh = datetime.now()
	report = trx_object.property.report(names.values())
	# t_report = datetime.now()

	# refresh takes around ~60-100 ms, report takes 5-10 ms
	# print(f"Property refresh took {t_refresh - t_start}")
	# print(f"Property report took {t_report - t_refresh}")
	return {alias : report[names[alias]] for alias in aliases}


# This method should be only called when ChipScoPy lock is grabbed!
# converts processed chipscopy scan data to a normalized, serializable format
def extract_data(scan):
	x_values = set()
	y_values = set()
	result = {}

	result["progress"] = scan.progress
	result["status"] = scan.status
	result["data_ready"] = scan.data_points_expected == scan.data_points_read
	result["started"] = scan.status != "Not Started"
	result["Date and Time Started"] = str(scan.start_time)
	result["Date and Time Ended"] = str(scan.stop_time)	
	result['link_status'] = scan.ez_hw_test_link_status\
		if hasattr(scan, 'ez_hw_test_link_status') else 'N/A'

	if scan.scan_data is None:
		return result

	result.update(scan.scan_data.all_params)	
	data_in = scan.scan_data.processed

	if data_in is None:
		return result

	for (x, y) in data_in.scan_points.keys():
		x_values.add(x)
		y_values.add(y)

	x_axis = sorted(x_values)
	y_axis = sorted(y_values)

	# data_out = [[1 for y in range(len(y_axis))] for x in range(len(x_axis))]
	data_out = [[1 for x in range(len(x_axis))] for y in range(len(y_axis))]
	open_points = 0
	num_points = len(y_axis) * len(x_axis)
	target_ber = result["Dwell BER"]

	for i in range(len(x_axis)):
		for j in range(len(y_axis)):
			point = data_in.scan_points[(x_axis[i], y_axis[j])]
			data_out[j][i] = point.ber
			if point.ber <= target_ber:
				open_points += 1	

	result["2d statistical"] = x_axis
	result["voltage_codes"] = y_axis
	result["scan_data"] = data_out
	result["Open UI percentage"] = open_points / num_points * 100
	# this is a guess. I don't know how Vivado actually calculates this value
	result["Open Area"] = int(open_points / num_points * 16384)
	return result



class Cache():
	'''
		Stores eye scan data so that it does not need 
		to be fetched on each page refresh from Chipscopy.
	'''
	def __init__(self, link_names):
		self.data = {}
		self.links = link_names
		self.lock = Lock()


	def make_key(self, *args):
		'''
			Generates hashmap key from the arguments
		'''
		return "+".join(map(str, args))


	def purge(self, scan_name):
		'''
			Remove all data for the particular scan from the storage
		'''
		with self.lock:
			for link_name in self.links:
				key = self.make_key(scan_name, link_name)			
				self.data.pop(key, None)
				# print("Remove from cache: {}".format(key))


	def purge_all(self):
		'''
			Remove all data from cache
		'''
		with self.lock:
			self.data = {}


	def put(self, scan_name, link_name, data):
		with self.lock:
			key = self.make_key(scan_name, link_name)
			self.data[key] = data
			# print("Put in cache: {}".format(key))


	def contains(self, scan_name, link_name):
		with self.lock:
			key = self.make_key(scan_name, link_name)
			# print("{} in cache = {}".format(key, key in self.data))
			return key in self.data


	def get(self, scan_name, link_name):
		with self.lock:
			key = self.make_key(scan_name, link_name)
			# print("Return from cache: {}".format(key))
			return self.data[key]