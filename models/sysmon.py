import re, os, pdb, math
from os import path, listdir
from util.iio import load_file, get_devices
from os.path import isfile
from models.self_test_executor import SelfTestExecutor


# convert regular expression strings into compiled patterns
def compile_patterns(patterns):
	result = {}
	for name, pattern in patterns.items():
		result[name] = re.compile(pattern)
	return result


# returns best guess of what the device name is
def get_name(device_path):
	file_path = path.join(device_path, "of_node/label")
	if isfile(file_path):
		return load_file(file_path).replace('\x00','')

	of_path = os.path.join(device_path, "of_node")
	if path.exists(of_path):
		dir_path, of_name = path.split(os.path.realpath(of_path))
		return of_name
	else:
		return "(name not found)"


# helper function to return all voltage or temperature values
def get_values(device_path, file_names, pattern):
	values = {}
	for file_name in file_names:
		match = pattern.match(file_name)
		if match:
			name = match.group("name")
			value = float(load_file(path.join(device_path, file_name)))
			values[name] = value
	return values


class SYSMON(SelfTestExecutor):
	def __init__(self, config):
		self.config = config

	# Check if device is available
	def device_exists(self, device_class):
		if not device_class in self.config["device_classes"]:
			return False

		devices = get_devices(self.config["device_classes"][device_class]["device_path"], device_class)
		return bool(devices)


	# read temperature, voltage, current, etc of a single device of specific class
	def read_device(self, device_path, compiled_patterns):
		result = {}
		file_names = listdir(device_path)
		for pattern_name, pattern in compiled_patterns.items():
			result[pattern_name] = get_values(device_path, file_names, pattern)
		return result


	# read all sensor outputs of SYSMON
	def read_all(self, device_class):

		if not device_class in self.config["device_classes"]:
			return { "error" : f"Unknown HWMON device class {device_class}. "
				+ f'Valid choices: {list(self.config["device_classes"].keys())}'}

		devices = get_devices(self.config["device_classes"][device_class]["device_path"], device_class)
		if not devices:
			return {"error" : f"Can't access HWMON device: {device_class}"}

		#struct that describes the hwmon device
		class_config = self.config["device_classes"][device_class]

		result = {
			"title" : class_config["title"],
			"class" : device_class,			
			"devices" : {}
		}

		# read all devices of the requested class	
		# device is folder path to IIO device, e.g. "/sys/bus/iio/devices/iio:device4"
		compiled_patterns = compile_patterns(class_config["patterns"])
		for device_path in devices:
			result["devices"][device_path] = self.read_device(device_path, compiled_patterns)
			result["devices"][device_path]["name"] = get_name(device_path)

		# Process readings to add reference values
		if device_class == "xlnx,versal-sysmon":
			return self.process_versal_sysmon(result, class_config, device_path)
		elif device_class == "tmp435":
			return self.process_tmp(result, class_config)
		elif device_class == "ltm4700":
			return self.process_ltm(result, class_config, device_path)
		elif device_class == "jc42":
			return self.process_jc42(result, class_config)


	def self_test_main(self, **kwargs):
		result = {}
		for device_class in self.config["device_classes"]:
			result[device_class] = self.read_all(device_class)
		return result


	# Check whether the measured value is within range
	def check_measured_value(self, meas_type, name, read_value, min_value, max_value):
		check = {"status" : "", "msg" : ""}
		if math.isnan(read_value):
			check["status"] = "BAD"
			check["msg"] = "{0} {1} NaN".format(meas_type, name)
		elif (not math.isnan(max_value) and read_value > max_value) :
			check["status"] = "HIGH"
			check["msg"] = "{0} {1} {2} > max {3}".format(meas_type, name, read_value, max_value)
		elif (not math.isnan(min_value) and read_value < min_value):
			check["status"] = "LOW"
			check["msg"] = "{0} {1} {2} < min {3}".format(meas_type, name, read_value, min_value)
		else:
			check["status"] = "OK"
		return check


	# Test outcome
	def self_test_check_result(self, result):
		status_ok = True
		msg = []
		for device_type in result:
			if 'devices' not in result[device_type]:
				if 'error' in result[device_type]:
					msg.append(result[device_type]["error"])
					status_ok = False
			else:
				for device_path in result[device_type]['devices']:
					for quantity in result[device_type]['devices'][device_path]:
						if quantity == "name":
							continue
						for measurement_name in result[device_type]['devices'][device_path][quantity]:
							measurement = result[device_type]['devices'][device_path][quantity][measurement_name]
							if "status" not in measurement:
								continue
							if measurement["status"] != "OK":
								msg.append(measurement["msg"])
								status_ok = False
		if status_ok:
			msg = "OK"
			msg_level = "success"
		else:
			msg_level = "error"

		return { "status" : msg_level, "result" : msg}


	# Format sysmon meaurements
	def process_versal_sysmon(self, result, class_config, device_path):
		compiled_patterns = compile_patterns(class_config["patterns"])
		for quantity in compiled_patterns:
			for measurement_name in result["devices"][device_path][quantity]:
				value = result["devices"][device_path][quantity][measurement_name]
				# New result format
				new_value = {"read" : value, "min" : float('nan'), "nominal" : float('nan'), "max": float('nan'), "desc" : ""}
				# Populate reference fields
				if measurement_name in class_config["reference"][quantity]:
					new_value["nominal"] = class_config["reference"][quantity][measurement_name]["nominal"]
					new_value["max"] = class_config["reference"][quantity][measurement_name]["max"]
					new_value["min"] = class_config["reference"][quantity][measurement_name]["min"]
					new_value["desc"] = class_config["reference"][quantity][measurement_name]["desc"]
				# Check value
				check = self.check_measured_value(quantity, measurement_name, value, new_value["min"], new_value["max"])
				new_value = {**new_value, **check}
				result["devices"][device_path][quantity][measurement_name] = new_value
		return result


	# Each TMP probe is on a different file device.
	# Data has to be reformatted to have the same format as versal-sysmon's
	def process_tmp(self, result, class_config):
		compiled_patterns = compile_patterns(class_config["patterns"])
		new_devices = {'/sys/class/hwmon/hwmonX' : {'temperature' : {} }}
		for device_path, device_data in result["devices"].items():
			probe_name = device_data['name']
			local_value = device_data['probe']['temp1_input']/1000.0
			probe_value = device_data['probe']['temp2_input']/1000.0
			measurement = {"local" : local_value, "probe" : probe_value, "min" : float('nan'), "nominal" : float('nan'), "max": float('nan'), "desc" : ""}

			if probe_name in class_config["reference"]["probe"]:
				measurement["nominal"] = class_config["reference"]["probe"][probe_name]["nominal"]
				measurement["max"] = class_config["reference"]["probe"][probe_name]["max"]
				measurement["min"] = class_config["reference"]["probe"][probe_name]["min"]
				measurement["desc"] = class_config["reference"]["probe"][probe_name]["desc"]

			# Check value
			check = self.check_measured_value('temperature', probe_name, probe_value, measurement["min"], measurement["max"])
			measurement = {**measurement, **check}
			new_devices['/sys/class/hwmon/hwmonX']['temperature'][probe_name] = measurement
		result['devices'] = new_devices
		return result


	# Format ltm meaurements
	def process_ltm(self, result, class_config, device_path):
		new_device_data = {'voltage':{}, 'current':{}, 'power':{}, 'temperature':{}}
		data = result["devices"][device_path]
		for key in ['in1', 'in2', 'in3']:
			new_device_data['voltage'][key] = {
				'read': data['voltage'].get(f'{key}_input', float('nan'))/1000.0,
				'min': data['voltage_min'].get(f'{key}_min', float('nan'))/1000.0,
				'max': data['voltage_max'].get(f'{key}_max', float('nan'))/1000.0
			}
			d = new_device_data['voltage'][key]
			check = self.check_measured_value('voltage', key, d["read"], d["min"], d["max"])
			new_device_data['voltage'][key] = {**new_device_data['voltage'][key], **check}

		for key in ['curr1', 'curr2', 'curr3']:
			new_device_data['current'][key] = {
				'read': data['current'].get(f'{key}_input', float('nan'))/1000.0,
				'min': float('nan'),
				'max': data['current_max'].get(f'{key}_max', float('nan'))/1000.0
			}
			d = new_device_data['current'][key]
			check = self.check_measured_value('current', key, d["read"], d["min"], d["max"])
			new_device_data['current'][key] = {**new_device_data['current'][key], **check}

		for key in ['power1', 'power2', 'power3']:
			new_device_data['power'][key] = {
				'read': data['power'].get(f'{key}_input', float('nan'))/1e6,
				'min': float('nan'),
				'max': float('nan')
			}
			d = new_device_data['power'][key]
			check = self.check_measured_value('power', key, d["read"], d["min"], d["max"])
			new_device_data['power'][key] = {**new_device_data['power'][key], **check}

		for key in ['temp1', 'temp2', 'temp3']:
			new_device_data['temperature'][key] = {
				'read': data['temp'].get(f'{key}_input', float('nan'))/1000.0,
				'min': float('nan'),
				'max': data['temp_max'].get(f'{key}_max', float('nan'))/1000.0
			}
			d = new_device_data['temperature'][key]
			check = self.check_measured_value('temperature', key, d["read"], d["min"], d["max"])
			new_device_data['temperature'][key] = {**new_device_data['temperature'][key], **check}

		result["devices"][device_path] = new_device_data
		return result


	# Format jc42 meaurements
	def process_jc42(self, result, class_config):
		value = result['devices']['/sys/class/hwmon/hwmon8']['probe']['temp1_input']/1000.0
		name = result['devices']['/sys/class/hwmon/hwmon8']['name']
		measurement = {"read" : value, "min" : float('nan'), "max": float('nan'), "desc" : ""}

		if name in class_config['reference']['probe']:
			measurement["min"] = class_config['reference']['probe'][name]["min"]
			measurement["max"] = class_config['reference']['probe'][name]["max"]
			measurement["desc"] = class_config['reference']['probe'][name]["desc"]

		check = self.check_measured_value('temperature', name, value, measurement["min"], measurement["max"])
		measurement = {**measurement, **check}

		result['devices']['/sys/class/hwmon/hwmon8'] = {"temperature" : {}}
		result['devices']['/sys/class/hwmon/hwmon8']["temperature"][name] = measurement
		return result