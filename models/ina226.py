import pdb
import math
from os import path
from os.path import isdir
from util.iio import load_file, get_devices
from models.self_test_executor import SelfTestExecutor


class INA226(SelfTestExecutor):

	def __init__(self, config):
		self.config = config	
		self.device_path = config["device_path"]
		self.name = config["name"]
		self.devices = get_devices(self.device_path, self.name)


	def check_measured_value(self, meas_type, name, read_value, min_value, max_value):
		check = {"status" : "", "msg" : ""}
		if math.isnan(read_value):
			check["status"] = "BAD"
			check["msg"] = "{0} {1} NaN".format(meas_type, name)
		elif (not math.isnan(max_value) and read_value > max_value) :
			check["status"] = "HIGH"
			check["msg"] = "{0} {1} {2} > max {3}".format(meas_type, name, read_value, max_value)
		elif (not math.isnan(min_value) and read_value < min_value):
			check["status"] = "LOW"
			check["msg"] = "{0} {1} {2} < min {3}".format(meas_type, name, read_value, min_value)
		else:
			check["status"] = "OK"
		return check


	# returns hashmap of all readings available for this device
	def get_data(self, device):
		if not isdir(device):
			return {}

		try:
			v0_raw = int(load_file(path.join(device, "in_voltage0_raw")))
			v0_scale = float(load_file(path.join(device, "in_voltage0_scale")))
			v1_raw = int(load_file(path.join(device, "in_voltage1_raw")))
			v1_scale = float(load_file(path.join(device, "in_voltage1_scale")))
			p2_raw = int(load_file(path.join(device, "in_power2_raw")))
			p2_scale = float(load_file(path.join(device, "in_power2_scale")))
			i3_raw = int(load_file(path.join(device, "in_current3_raw")))
			i3_scale = float(load_file(path.join(device, "in_current3_scale")))
			r_shunt = float(load_file(path.join(device, "in_shunt_resistor")))
			label = load_file(path.join(device, "label"))
		except Exception as err:
			print(err)
			return {}

		voltage = v1_raw * v1_scale / 1000.0
		current = i3_raw * i3_scale / 1000.0
		power   = p2_raw * p2_scale / 1000.0
		V_dev = -1
		V_tol = -1
		I_max = -1

		if label in self.config["reference"]["voltage"]:
			V_min = self.config["reference"]["voltage"][label]["min"]
			V_max = self.config["reference"]["voltage"][label]["max"]

		if label in self.config["reference"]["current"]:
			I_max = self.config["reference"]["current"][label]["max"]

		data = {
			"V" : voltage,
			"V_min" : V_min,
			"V_max" : V_max,
			"V_status" : self.check_measured_value('voltage', label, voltage, V_min, V_max)["status"],
			"V_msg" : self.check_measured_value('voltage', label, voltage, V_min, V_max)["msg"],
			"I" : current,
			"I_max" : I_max,
			"I_status" : self.check_measured_value('current', label, current, float('nan'), I_max)["status"],
			"I_msg" : self.check_measured_value('current', label, current, float('nan'), I_max)["msg"],
			"P" : power
		}

		return data, label


	# returns data for all devices
	def read_all(self):
		result = {}
		for device in self.devices:
			device_data = self.get_data(device)
			if device_data:
				data, label = device_data
				result[label] = data

		return result

	def self_test_main(self, **kwargs):
		return self.read_all()


	def self_test_check_result(self, result):
		status_ok = True
		msg = []
		for entry in result:
			if result[entry]["V_status"] != "OK":
				status_ok = False
				msg.append(result[entry]["V_msg"])
			if result[entry]["I_status"] != "OK":
				status_ok = False
				msg.append(result[entry]["I_msg"])

		if status_ok:
			msg = "OK"
			msg_level = "success"
		else:
			msg_level = "error"

		return { "status" : msg_level, "result" : msg}