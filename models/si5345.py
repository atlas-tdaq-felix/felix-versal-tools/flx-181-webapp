import pdb
from .device_manager import DeviceManager, ensure_device_exists
from util.si5345 import SI5345 as SI5345_Device


class SI5345(DeviceManager):
	'''
		Class to store and manage the SI5345 devices
	'''
	def __init__(self, config):
		super().__init__(SI5345_Device, config)


	@ensure_device_exists
	def load_configuration(self, device_name, register_data):
		return self.devices[device_name].load_configuration(register_data)


	@ensure_device_exists
	def soft_rst(self, device_name):
		return self.devices[device_name].soft_rst()


	@ensure_device_exists
	def hard_rst(self, device_name):
		return self.devices[device_name].hard_rst()


	@ensure_device_exists
	def update_config(self, device_name, new_config):
		self.devices[device_name].update_config(new_config)
		return { "status" : "success" }

