import os, re
from .process_runner import ProcessRunner
from subprocess import check_output


class Iperf3(ProcessRunner):
	def __init__(self, config):
		ProcessRunner.__init__(self, config)
		

	def start_test(self, json_output=False, **kwargs):
		server = kwargs["server"] if "server" in kwargs else ""
		if not server:
			return { "error" : "Please specify iperf3 server IP address"}

		args = []
		if "duration" in kwargs and kwargs["duration"]:
			args += ["-t", str(kwargs["duration"])]

		if "bitrate" in kwargs and kwargs["bitrate"]:
			args += ["-b", str(kwargs["bitrate"])]

		if "buffer_length" in kwargs and kwargs["buffer_length"]:
			args += ["-l", str(kwargs["buffer_length"])]

		if "window_size" in kwargs and kwargs["window_size"]:
			args += ["-w", str(kwargs["window_size"])]

		if "segment_size" in kwargs and kwargs["segment_size"]:
			args += ["-M", str(kwargs["segment_size"])]

		if not json_output and "verbose" in kwargs and kwargs["verbose"]:
			args += ["-V"]

		if "udp" in kwargs and kwargs["udp"]:
			args += ["-u"]

		if "reverse" in kwargs and kwargs["reverse"]:
			args += ["-R"]		

		if "other" in kwargs and kwargs["other"]:
			args += kwargs["other"].split()

		if json_output:
			args += ["-J"]

		args += ["--forceflush", "-c", server]
		return super().start_test(args=args)


	def ifconfig(self):
		return check_output(['ifconfig']).decode("utf-8")
