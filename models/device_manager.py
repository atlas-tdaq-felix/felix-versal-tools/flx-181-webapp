import pdb
from functools import wraps
from models.self_test_executor import SelfTestExecutor


def ensure_device_exists(func):
	@wraps(func)
	def wrapper(self, device_name, *args, **kwargs):
		if not device_name in self.devices:
			return {"error" : f"Unknown device - {device_name}"}
		return func(self, device_name, *args, **kwargs)
	return wrapper


class DeviceManager(SelfTestExecutor):
	'''
		Class to store and manage devices
	'''
	def __init__(self, DeviceClass, config: dict):
		self.config = config
		self.devices = {}
		for name, device in self.config["mapping"].items():
			self.devices[name] = DeviceClass(device)


	def read_all(self):
		result = {}
		for name, device in self.devices.items():
			result[name] = device.read()
		return result


	def device_exists(self, device_name: str):
		return device_name in self.devices


	def get_device_list(self):
		return self.devices.keys()
