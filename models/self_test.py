import pdb, traceback, copy, requests
from threading import Thread, Lock
from datetime import datetime

# convert time objects to json-compatible strings, 
# also calculate the elapsed time

class SelfTest():
    def __init__(self, config, models):
        self.config = config
        self.models = models
        self.lock = Lock()
        self.thread = Thread()
        self.status = {"status" : "Not started", "completed" : 0, "total" : 0, "skipped" : 0,
            "failed" : 0, "current_model" : "N/A", "report_ready" : False,
            "start_time" : None, "end_time" : None}
        self.cancel_test = False
        self.skip_test = False
        self.prepare_schedule_and_summary()
        self.result = {}


    def upload_report(self, endpoint, api_key, **board_info):
        data = self.get_results()
        # board_id = self.models['board_id']
        # board_info = board_id.get_board_info()

        # actually check it server-side
        # if not 'board' in board_info or not board_info['board'] \
        #     or not 'revision' in board_info or not board_info['revision'] \
        #     or not 'idcode' in board_info:
        #     return {"error" : "Board identification information is missing"}

        url = f"{endpoint}/api/reports"
        headers = {"X-API-Key" : api_key}
        # request_data = {'board' : board_info['board'],
        #         'revision' : board_info['revision'],
        #         'board_id' : f"{board_info['idcode']:X}",
        #         "data" : json.dumps(data, indent=4)}
        board_info['data'] = json.dumps(data, indent=4)

        try:
            response = requests.post(endpoint, headers=headers, data=board_info)
        except Exception as err:
            return {"error" : str(err)}
        return response




    def prepare_schedule_and_summary(self):
        self.test_schedule = {}
        self.test_summary = {}
        for name, model in self.models.items():
            test_config = model.self_test_configuration()
            if "description" in test_config:
                description = test_config["description"]
            else:
                description = f"{name.title()} test"

            skip = test_config["skip"] if "skip" in test_config else False

            self.test_schedule[name] = {
                "description" : description,
                "skip" : skip,
            }

            self.test_summary[name] = {
                "description" : description,
                "status" : "notice",
                "result" : "Not started"
            }


    def test_is_running(self):
        with self.lock:
            return self.thread.is_alive()


    def start_test(self, schedule=None):
        with self.lock:
            if self.thread.is_alive():
                return {"error" : "Self-test is already running"}

            if not schedule is None:
                self.update_schedule(schedule)

            self.thread = Thread(target=self.test_runner, daemon=True)
            self.status['start_time'] = datetime.now()
            self.status['report_ready'] = False
            self.result = {}
            self.thread.start()
        
        return {"status" : "Self-test is started successfully"}


    def update_schedule(self, new_schedule):
        param_whitelist = ["skip", "kwargs"]
        for name, params in new_schedule.items():
            if not name in self.test_schedule:
                continue

            for param_name, param_value in params.items():
                if not param_name in param_whitelist:
                    continue

                self.test_schedule[name][param_name] = param_value



    def stop_test(self):
        with self.lock:
            if not self.thread.is_alive():
                return {"error" : "Self-test not running"}
            self.cancel_test = True

        return {"status" : "Self-test is stopped successfully"}


    def skip_test(self):
        with self.lock:
            if not self.thread.is_alive():
                return {"error" : "Self-test not running"}

            if self.skip_test:
                return {"error" : "Already skipping the current test step"}

            self.skip_test = True

        return {"status" : "Skipping a test step"}


    def get_status(self):
        with self.lock:
            status = copy.deepcopy(self.status)
            status['running'] = self.thread.is_alive()
            status['done'] = self.status["total"] > 0 and not self.thread.is_alive()

        return status
    

    def get_results(self):
        with self.lock:
            result = copy.deepcopy(self.result)
            status = copy.deepcopy(self.status)

        test_results = {
            "self_test_status" : status,
            "data" : result,
            "report_format" : "bist_v1",
        }

        return test_results


    def test_runner(self):
        with self.lock:
            self.result = {}
            self.status['status'] = 'Running'
            self.status['completed'] = 0
            self.status['failed'] = 0
            self.status['skipped'] = 0
            self.status['total'] = len(self.models)
            schedule = copy.deepcopy(self.test_schedule)

        for name, model in self.models.items():
            with self.lock:
                self.status['current_model'] = name

            if schedule[name]['skip']:
                test_outcome = { "status" : "notice", "result" : "Skipped" }
                result = {}

                with self.lock:
                    self.status['skipped'] += 1

            else:            
                try:
                    model.self_test_before()
                    result = model.self_test_main()
                    model.self_test_after()
                    test_outcome = model.self_test_check_result(result)

                    with self.lock:
                        self.status['completed'] += 1

                except Exception as err:
                    print(err)
                    print(traceback.format_exc())
                    test_outcome = { "status" : "error", "result" : "BUG: test executor crashed" }
                    result = {"error" : str(err), "trace" : traceback.format_exc()}
                    with self.lock:
                        self.status['failed'] += 1

            with self.lock:
                self.result[name] = {
                    "data" : copy.deepcopy(result),
                    "test_result" : copy.deepcopy(test_outcome)
                }

                self._update_summary_helper(name=name, field="status",
                    test_outcome=test_outcome, default="warning")
                self._update_summary_helper(name=name, field="result",
                    test_outcome=test_outcome, default="(missing test summary)")

                if self.cancel_test:
                    break

        with self.lock:
            self.status['status'] = "Done"
            self.status['current_model'] = "N/A"
            self.cancel_test = False
            self.status['end_time'] = datetime.now()
            self.status['report_ready'] = True


    def _update_summary_helper(self, name, field, test_outcome, default):
        if field in test_outcome:
            self.test_summary[name][field] = test_outcome[field]
        else:
            self.test_summary[name][field] = default


    def get_schedule(self):
        with self.lock:
            return copy.deepcopy(self.test_schedule) 


    def get_summary(self):
        with self.lock:
            return copy.deepcopy(self.test_summary)