from util.vivado import Vivado
from functools import wraps
import copy, pdb


class Vivado_IBERT():
	def __init__(self, config, vivado):
		self.config = config
		self.vivado = vivado
		self.links = None
		self.scans = None
		self.cache = Cache(self.config["gt_links"])


	def find_gt_link(self, name, params):
		'''
			Find specified GT link, or creates a new one if it doesn't exist
		'''
		mask = "{}->{}".format(params["tx"], params["rx"])
		link = self.vivado.get_hw_sio_links(mask=mask)
		if link:
			gt_link = link[0]
		else:
			gt_link = self.vivado.create_hw_sio_link(name, tx=params["tx"], rx=params["rx"])
			gt_link.set_properties(self.config["default_link_config"])
			
			'''
				The links are typically missing if Vivado either just started or the target
				was disconnected and reconnected. Vivado deletes the old links, but not the
				scans. If the old scans are accessed via a TCL command, Vivado will crash.
				So we remove all potentially stale scans to avoid that.
			'''
			if not self.scans is None:
				self.vivado.remove_all_hw_sio_scans()
				self.cache.purge_all()
				self.scans = None
		
		return gt_link


	def find_gt_links(self):
		'''
			Find or create all GT links specified in the configuration
		'''
		self.links = {}
		try:
			for name, params in self.config["gt_links"].items():
				self.links[name] = self.find_gt_link(name, params)
		except Exception as err:
			self.links = None
			raise Exception("Can't find or create transceiver links: {}".format(err))


	def scan_label(self, scan_name, link_name):
		return "{}_{}".format(scan_name, link_name)


	def make_scan_set(self, scan_name, params):
		'''
			Generate a set of scans for all links specified in configuration
			for the selected scan parameters
		'''
		result = {}
		for link_name, link in self.links.items():
			label = self.scan_label(scan_name, link_name)
			scan = self.vivado.create_hw_sio_scan(label, link)
			scan.set_properties(params["scan"])
			result[link_name] = scan
		return result


	def make_scans(self):
		'''
			Create all scans specified in the configuration
		'''
		self.scans = {}
		try:
			self.vivado.remove_all_hw_sio_scans()
			self.cache.purge_all()
			for name, params in self.config["scans"].items():				
				self.scans[name] = self.make_scan_set(name, params)
		except Exception as err:
			self.scans = None
			raise Exception("Can't find or create eye scans: {}".format(err))


	def verify_scans(self):
		'''
			Verify all scans to make sure they still exist in Vivado
		'''
		for scan_name, scan_data in self.scans.items():
			for link_name, scan in scan_data.items():
				scans = self.vivado.get_hw_sio_scans(mask=scan)
				if not scans:
					# the scan is missing, re-create it in Vivado
					link = self.links[link_name]
					label = self.scan_label(scan_name, link_name)
					scan = self.vivado.create_hw_sio_scan(label, link)
					scan.set_properties(self.config["scans"][scan_name]["scan"])
					scan_data[link_name] = scan


	def ensure_vivado_connection(func):
		'''
			Decorator to verify that the Vivado connection works correctly
		'''
		@wraps(func)
		def wrapper(self, *args, **kwargs):
			if self.vivado is None:
				return { "error" : "Connection configuration is missing, check the board definition file!" }

			if not self.vivado.connected:
				try:
					self.vivado.connect()	
				except Exception as err:
					return {"error" : str(err)}

			try:				
				return func(self, *args, **kwargs)
			except ConnectionError as err:
				self.links = None
				self.scans = None
				return { "error" : "Connection dropped" }
			except Exception as err:
				return { "error" : str(err)}
		return wrapper


	def ensure_links_exist(func):
		'''
			Decorator to ensure all links have been created
		'''
		@wraps(func)
		def wrapper(self, *args, **kwargs):
			self.find_gt_links()
			return func(self, *args, **kwargs)
		return wrapper


	@ensure_vivado_connection
	@ensure_links_exist
	def ibert_reset(self):
		'''
			Reset BERT data
		'''
		for name, link in self.links.items():
			link.ibert_reset()
		return { "status" : "Issued IBERT reset to all GT links" }


	@ensure_vivado_connection
	@ensure_links_exist
	def tx_reset(self):
		'''
			Reset TX link
		'''
		for name, link in self.links.items():
			link.tx_reset()
		return { "status" : "Issued TX reset to all GT links" }


	@ensure_vivado_connection
	@ensure_links_exist
	def rx_reset(self):
		'''
			Reset RX link
		'''
		for name, link in self.links.items():
			link.rx_reset()
		return { "status" : "Issued RX reset to all GT links" }


	@ensure_vivado_connection
	@ensure_links_exist
	def get_ibert_data(self):
		'''
			Returns all BERT data (Serial I/O link properties)
		'''
		result = {}
		for name, link in self.links.items():
			result[name] = link.properties(strip_channel_prefix=True)
		return result


	def ensure_scans_exist(func):
		'''
			Decorator to ensure all scans have been created
		'''
		@wraps(func)
		def wrapper(self, *args, **kwargs):
			if self.scans is None:
				self.make_scans()
			else:
				self.verify_scans()
			return func(self, *args, **kwargs)
		return wrapper


	@ensure_vivado_connection
	@ensure_links_exist
	@ensure_scans_exist
	def run_scan(self, scan_name):
		if not scan_name in self.config["scans"]:
			msg = "Scan {} is not defined in configuration".format(scan_name)
			return { "error" : msg }

		self.cache.purge(scan_name)

		link_config = self.config["scans"][scan_name]["link"]
		for link_name, scan in self.scans[scan_name].items():
			link = self.links[link_name]
			link.set_properties(link_config)
			scan.run()
		return { "status" : "Successfully started scan {}".format(scan_name) }


	@ensure_vivado_connection
	@ensure_links_exist
	@ensure_scans_exist
	def get_scan_data(self):
		'''
			Returns eye scan data given scan name
		'''
		result = {}
		for scan_name, scan_props in self.scans.items():
			scan_data = { "links" : {} }
			result[scan_name] = scan_data
			scan_data["comment"] = self.config["scans"][scan_name]["comment"]

			link_config = self.config["scans"][scan_name]["link"]
			for link_name, scan in self.scans[scan_name].items():
				scan_data["links"][link_name] = self.get_scan_link_data(scan, scan_name, link_name)
		return result


	def get_scan_link_data(self, scan, scan_name, link_name):
		'''
			Returns eye scan data for a particular link and scan
		'''
		if self.cache.contains(scan_name=scan_name, link_name=link_name):
			return self.cache.get(scan_name=scan_name, link_name=link_name)

		try:
			data = scan.data()
			props = scan.properties()
			data["progress"] = float(props["PROGRESS"].split("%")[0])
			data["status"] = props["STATUS"]
			data["data_ready"] = props["DATA_READY"]
			data["started"] = props["STATUS"] != "Not Started"

			if data["data_ready"] and props["STATUS"] == "Done":
				self.cache.put(scan_name=scan_name, link_name=link_name, data=data)
		except Exception as err:
			data = {}

		return data


class Cache():
	'''
		Stores eye scan data so that it does not need 
		to be fetched on each page refresh from Vivado.
	'''
	def __init__(self, links):
		self.data = {}
		self.links = list(links.keys())


	def make_key(self, *args):
		'''
			Generates hashmap key from the arguments
		'''
		return "+".join(map(str, args))


	def purge(self, scan_name):
		'''
			Remove all data for the particular scan from the storage
		'''		
		for link_name in self.links:
			key = self.make_key(scan_name, link_name)			
			self.data.pop(key, None)
			# print("Remove from cache: {}".format(key))


	def purge_all(self):
		'''
			Remove all data from cache
		'''
		self.data = {}


	def put(self, scan_name, link_name, data):
		key = self.make_key(scan_name, link_name)
		self.data[key] = data
		# print("Put in cache: {}".format(key))


	def contains(self, scan_name, link_name):
		key = self.make_key(scan_name, link_name)
		# print("{} in cache = {}".format(key, key in self.data))
		return key in self.data


	def get(self, scan_name, link_name):
		key = self.make_key(scan_name, link_name)
		# print("Return from cache: {}".format(key))
		return self.data[key]