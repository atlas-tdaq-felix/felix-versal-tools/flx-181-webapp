import pdb
from util.adm1266 import ADM1266 as ADM1266_Device
from util.i2c import I2CError
from threading import Lock
from functools import wraps
from models.self_test_executor import SelfTestExecutor


def make_device(settings):
	return ADM1266_Device(i2c_bus=settings["i2c_bus"], i2c_addr=settings["i2c_addr"], rails=settings["rails"])


class ADM1266(SelfTestExecutor):
	def __init__(self, config):
		self.config = config
		self.lock = Lock()
		self.devices = {}

		for name, settings in config["mapping"].items():
			try:
				self.devices[name] = make_device(settings)
			except I2CError as err:
				self.devices[name] = None


	def read(self, name):

		with self.lock:			
			try:
				if self.devices[name] is None:
					self.devices[name] = make_device(self.config["mapping"][name])
				return self.devices[name].read()
			except (IOError, I2CError) as err:
				return { "error" : str(err) }


	def read_all(self):
		result = {}
		for name, settings in self.config["mapping"].items():
			result[name] = self.read(name)
		return result


		