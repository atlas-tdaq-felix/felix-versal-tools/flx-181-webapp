from matplotlib import cm, ticker
from PIL import Image, ImageDraw, ImageFont
from threading import Lock
import numpy as np
import matplotlib.pyplot as plt
import json, io, copy, pdb
import copy

# http://127.0.0.1:8080/ibert_eye_scan/pma_near_loopback/FMC_3.png?timestamp=1641321971.3657186


class EyeScanPlotter():
	def __init__(self, model):
		self.data = {}
		self.model = model
		self.image_size_in = (4, 3)
		self.pil_lock = Lock()
		self.plt_lock = Lock()
		self.data_lock = Lock()


	def image_size_px(self, size):
		size = size or self.image_size_in
		return tuple(map(lambda x: x * 72, size))


	def update(self, scan, data):
		'''
			Supply new data for the plot for selected scan
		'''	
		with self.data_lock:
			self.data[scan] = data


	def get_pll_error_text(self, link):
		# checking PLL lock status during the scan locks up the web application somehow
		return ""

		print(f"{link}: Checking PLL status")
		with self.model.chipscopy_lock:
			tx = self.model.links[link].tx
			rx = self.model.links[link].rx

		pll_status = [{
				"locked" : self.model.pll_is_locked(tx),
				"error" : "TX PLL is not locked!" },
			{
				"locked" : self.model.pll_is_locked(rx),
				"error" : "RX PLL is not locked!"},]

		print(f"{link}: Retrieved the PLL status")
		return "\n".join([x["error"] for x in pll_status if not x["locked"]])


	def plot(self, scan, link, format="png", size=None):
		'''
			Returns graphics data of the selected eye scan
		'''	
			

		with self.data_lock:
			data = copy.deepcopy(self.data) 

		if (not scan in data) or (not link in data[scan]["links"]):
			return self.plot_error(text="(configuration error)", format=format)

		if not data[scan]["links"][link]:
			return self.plot_warning(text="(link doesn't exist in the design)", format=format)

		if not link in self.model.links:
			return self.plot_error(text="(unknown link)", format=format)
		
		scan_comment = data[scan]["comment"]
		link_data = data[scan]["links"][link]		
		size = size or self.image_size_in

		if not link_data:
			error = self.get_pll_error_text(link)
			if error:
				return self.plot_error(text=error, format=format)
			else:
				return self.plot_no_data(format=format)
		
		if link_data["status"].lower() == "not started":
			error = self.get_pll_error_text(link)
			if error:
				return self.plot_error(text=error, format=format)
			else:
				return self.plot_no_data(format=format)

		elif not link_data["status"] == "Done":
			return self.plot_in_progress(link_data, format=format)

		if "image_data" in link_data:
			return link_data["image_data"]
		
		link_status = link_data['link_status']

		with self.plt_lock:
			# matplotlib is not thread-safe and will crash the app when the browser 
			# sends multiple simultaneous requests to fetch the images
			
			x = np.array(link_data["2d statistical"]) / 64.0
			y = np.array(link_data["voltage_codes"])
			z = np.array(link_data["scan_data"])

			fig, ax = plt.subplots()
			fig.set_size_inches(*size)
			CS = ax.contourf(x, y, z, locator=ticker.LogLocator(),
				cmap=cm.rainbow)
			ax.set_title(f'{scan_comment}: BER')
			ax.set_xlabel('Unit interval')
			ax.set_ylabel('Voltage codes')
			cbar = fig.colorbar(CS, format='%.0e')
			plt.figtext(0.99, 0.0, link, horizontalalignment='right')
			plt.figtext(0.01, 0.0, link_status, horizontalalignment='left')
			
			with io.BytesIO() as buffer:
				fig.savefig(buffer, format=format, bbox_inches='tight')
				image_data = buffer.getvalue()

		with self.data_lock:
			self.data[scan]["links"][link]["image_data"] = image_data

		return image_data


	def plot_text(self, color, text, text_color=(0,0,0), format="png", size=None):
		with self.pil_lock:
			size_pixels = self.image_size_px(size)
			image = Image.new('RGB', size_pixels, color=color)
			draw = ImageDraw.Draw(image)
			font = ImageFont.load_default()

			# draw a border
			x_max = size_pixels[0] - 1
			y_max = size_pixels[1] - 1
			draw.rectangle([(0, 0), (x_max, y_max)], outline=(0,0,0), width=2)

			# calculate text offset
			line_lengths = [ len(x) for x in text.split("\n")]
			text_offset = 3*max(line_lengths)

			draw.text((x_max / 2 - text_offset, y_max / 2), text, font=font, fill=text_color)

			with io.BytesIO() as buffer:
				image.save(buffer, format=format)
				image_data = buffer.getvalue()

			return image_data


	def plot_no_data(self, format="png", size=None):
		'''
			Image for scans that have not started yet
		'''
		return self.plot_text(color=(255, 255, 255), text="(no data available)",
			text_color=(0, 0, 0), format=format, size=size)


	def plot_warning(self, text, format="png", size=None):
		'''
			The link for this scan is not in the design
		'''
		return self.plot_text(color=(255, 193, 7), text=text,
			text_color=(255, 255, 255), format=format, size=size)


	def plot_error(self, text, format="png", size=None):
		'''
			This scan or link doesn't exist in the configuration
		'''
		return self.plot_text(color=(255, 0, 0), text=text,
			text_color=(255, 255, 255), format=format, size=size)


	def plot_in_progress(self, data, format="png", size=None):
		'''
			Image for scans currently in progress
		'''
		with self.pil_lock:
			size_pixels = self.image_size_px(size)
			image = Image.new('RGB', size_pixels, color=(255, 255, 255))
			draw = ImageDraw.Draw(image)
			font = ImageFont.load_default()

			# draw a border
			x_max = size_pixels[0] - 1
			y_max = size_pixels[1] - 1
			draw.rectangle([(0, 0), (x_max, y_max)], outline=(0,0,0), width=2)

			# draw progress bar
			percentage = data["progress"]
			border = 2
			pbar_outline = [(x_max/2 * 0.5, y_max/2), (x_max/2 * 1.5, y_max/2 * 1.1)]
			pbar_bg = (230, 230, 230)
			pbar_fill = (0, 230, 0)
			draw.rectangle(pbar_outline, outline = (0,0,0), width=border, fill=pbar_bg)
			full_width = pbar_outline[1][0] - pbar_outline[0][0] - 2 * border
			pbar_inner = [(pbar_outline[0][0] + border, pbar_outline[0][1] + border),
				(pbar_outline[0][0] + border + full_width * percentage/100,
				pbar_outline[1][1] - border)]
			draw.rectangle(pbar_inner, fill=pbar_fill)
			text = "Eye scan progress: {:.0f}%".format(percentage)
			draw.text((x_max / 2 - 60, y_max / 2 - 25), text, font=font, fill=(0,0,0))

			with io.BytesIO() as buffer:
				image.save(buffer, format=format)
				image_data = buffer.getvalue()

			return image_data
