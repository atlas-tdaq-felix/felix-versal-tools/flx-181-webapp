import os, re
from .process_runner import ProcessRunner


meminfo_pattern = re.compile("(?P<name>.*):\\s*(?P<value>\\d+)\\s*(?P<unit>[a-zA-Z]+)?")
memtester_size_pattern = re.compile("^\\d+[BKMG]?$")


class Memtester(ProcessRunner):

	def __init__(self, config):
		ProcessRunner.__init__(self, config)
		self.memory = "10M"
		self.iterations = "1"
		self.mask = config["mask_default"]
		self.mask_variable = config["mask_variable"]

		
	# launch memory test
	def start_test(self, memory, iterations, mask):
		mask = mask or self.config['mask_default']

		if memory is None:
			return { "error" : "Missing 'memory' argument"}		

		if iterations is None:
			return { "error" : "Missing 'iterations' argument"}

		if not re.match(memtester_size_pattern, memory):
			return { "error" : "Invalid format of 'memory' argument"}

		try:
			iterations = int(iterations)
			if iterations < 1:
				return { "error" : "'iterations' is our of range (must be >= 1)"}
		except:
			return { "error" : "Invalid format of 'iterations' argument"}

		env = os.environ.copy()
		env[self.mask_variable] = mask

		args = [memory, str(iterations)]

		return super().start_test(args=args, env=env)


	# returns the /proc/meminfo contents as hashmap
	def meminfo(self):		
		with open("/proc/meminfo", "r") as file:
			lines = file.readlines()

		result = {}
		for line in lines:
			match = re.match(meminfo_pattern, line.strip())
			if match:
				result[match.group("name")] = int(match.group("value"))
		return result
