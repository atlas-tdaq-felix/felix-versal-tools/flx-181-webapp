import pdb
from util.iio import load_json
import math


class CCF_Clocks():
    def __init__(self, config):
        self.config = config


    def read_all(self):
        try:
            data = load_json(self.config["clk_dump_path"])
        except Exception as err:
            print(err)
            return { "error" : err }

        result = {}
        
        for name in self.config["whitelist"]:
            if name in data:
                result[name] = data[name]
            else:
                result[name] = { "rate" : float("nan"), "error" : True}

        return result