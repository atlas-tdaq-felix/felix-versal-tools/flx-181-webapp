import os, time, pdb

from datetime import datetime
from os.path import isfile
from string import printable
from copy import deepcopy
from threading import Thread, Lock, Event
from subprocess import Popen, PIPE, STDOUT
from fcntl import fcntl, F_GETFL, F_SETFL
from contextlib import contextmanager

pipe_refresh_s = 0.5


class ProcessRunner():

	def __init__(self, config):
		self.config = config
		self.app = config["app"]
		self.start_time = None
		self.end_time = None
		self.process = None	
		self.thread = Thread()
		self.lock = Lock()
		self.output = []
		self.line = ""

		self.binary = config["binary"]
		self.args = []
		self.env = None
		self.args_checker = None

		self.start_evt = None
		self.stop_evt = None


	def __del__(self):
		self.stop_test()


	# return status of the memory test
	def get_status(self):
		start_time = None
		end_time = None
		return_code = None
		output = []
		running = False

		with self.lock:
			start_time = deepcopy(self.start_time)
			end_time = deepcopy(self.end_time)
			running = self.thread.is_alive()			
			output = self.get_output()
			if not running and not self.process is None:
				return_code = self.process.returncode

		return {
			"running" : running,
			"output" : output,
			"return_code" : return_code,
			"start_time" : start_time,
			"end_time" : end_time
		}


	# returns copy of the process output. Assumes the lock is acquired
	def get_output(self):
		output = deepcopy(self.output)
		if (self.line != ""):
			output.append(self.line)
		return output



	# launch the test
	def start_test(self, args=[], env=None):
		start_time = None
		env = env or os.environ.copy()
		
		with self.lock:
			running = self.thread.is_alive()			
			if not running:
				start_time = datetime.now()
				self.start_time = start_time
				self.args = deepcopy(args)
				self.env = deepcopy(env)
				self.thread = Thread(target=self.test_thread, daemon=True)
				self.thread.start()

		return { "start_time" : start_time }


	# stop the test
	def stop_test(self):
		running = False
		end_time = None

		with self.lock:
			running = self.thread.is_alive()
			if running:
				self.process.terminate()
				end_time = datetime.now()
				self.end_time = end_time

		return {
			"was_running" : running,
			"end_time" : end_time
		}


	# background thread running the test process
	def test_thread(self):
		with self.lock:
			success = False			
			for binary in self.binary:
				binary = self.eval_path(binary)
				if isfile(binary):
					cmd = deepcopy(self.args)
					cmd.insert(0, binary)					
					self.process = Popen(cmd, env=self.env,
						stdout=PIPE, stderr=STDOUT)
					success = True
					self.output = [ "# " + (" ".join(cmd))]
					self.line = ""

					# make STDOUT non-blocking
					fd = self.process.stdout
					fl = fcntl(fd, F_GETFL)
					fcntl(fd, F_SETFL, fl | os.O_NONBLOCK)
					break

			if not success:
				msg = "Cannot find test binary: {}".format(self.binary)
				self.output = [msg]
				self.return_code = 2
				if not self.stop_evt is None: self.stop_evt.set()
				print(msg)
				return

		if not self.stop_evt is None: self.stop_evt.clear()
		if not self.start_evt is None: self.start_evt.set()		

		while (self.process.poll() is None):
			time.sleep(pipe_refresh_s)
			with self.lock:	
				self.try_read_pipe()

		with self.lock:
			self.try_read_pipe()
			if (self.line != ""):
				self.output.append(self.line)
				self.line = ""
			self.end_time = datetime.now()

		if not self.start_evt is None: self.start_evt.clear()
		if not self.stop_evt is None: self.stop_evt.set()


	# read the process's STDOUT, remove all non-printable characters, append
	# complete lines to the output
	def try_read_pipe(self):
		try:			
			data_raw = self.process.stdout.read()
			if data_raw is not None:
				data = data_raw.decode("utf-8")
				for char in data:
					if char == '\n':
						self.output.append(self.line)
						self.line = ''
					elif char == '\x08' and self.line != "":
						self.line = self.line[:-1]
					elif char in printable:
						self.line += char
		except Exception as err:
			print(err)


	# Substitute variables in binary path
	# <app_root_path> resolves to the absolute path of Flask application
	def eval_path(self, path):
		path = path.replace("<app_root_path>", self.app.root_path)
		return path
		