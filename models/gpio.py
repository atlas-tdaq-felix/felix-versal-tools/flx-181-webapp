import threading, copy, re, pdb
from util import gpio
from util.gpio import GPIOError
from models.self_test_executor import SelfTestExecutor


class GPIO(SelfTestExecutor):
	def __init__(self, config):
		self.config = config
		self.lock = threading.Lock()

		for device, params in self.config["mapping"].items():
			params["has_inputs"] = self.has_direction(params["lines"], "in")
			params["has_outputs"] = self.has_direction(params["lines"], "out")

			params["device"] = gpio.detect(device)

		self.initialize_lines()		


	def read_all(self):
		# read out all known GPIO devices
		
		result = {}
		for device, params in self.config["mapping"].items():
			try:
				result[device] = self.read_device(device, params)
			except GPIOError as err:
				result[device] = {"error" : str(err)}
		return result
		


	def set(self, device, bit_data):			
		# write bits of the selected GPIO device
		result = {}
		if not device in self.config["mapping"]:
			return {"error" : "Unknown GPIO device '{}'".format(device)}
		try:
			
			for bit, value in bit_data.items():
				if not isinstance(bit, int):
					bit = int(bit)
					#return {"error" : "Bit number '{}' must be an integer".format(bit)}
				if not bit in self.config["mapping"][device]["lines"]:
					return {"error" : "Unknown line {} in GPIO device '{}'".format(bit, device)}
				elif self.config["mapping"][device]["lines"][bit]["direction"] != "out":
					return {"error" : "Line {} of GPIO device '{}' is not an output".format(
						bit, device)}
				if isinstance(value, int):
					value_bool = bool(value)
				elif isinstance(value, str) and value != "":
					value_bool = False if value == "0" else True
				else:
					return {"error" : "Bit value '{}' must be '0' or '1'".format(bit)}
				with self.lock:
					gpio.set_line(self.config["mapping"][device]["device"], bit, value_bool)
					self.config["mapping"][device]["lines"][bit]["value"] = value_bool
					result[bit] = value_bool		
		except Exception as err:
			return {"error" : str(err)}
		return result


	def self_test_check_result(self, result):
		return self.self_test_check_device_list(result,
			human_readable_name_getter=lambda name: self.config["mapping"][name]["comment"])


	# -------------------------------------------------------

	def read_device(self, device, params):
		# read all input lines of the gpio device
		result = copy.deepcopy(params)
		for line, props in params["lines"].items():
			with self.lock:
				if props["direction"] == "out" and "value" in props:
					value = props["value"]
				else:
					value = gpio.get_line(self.config["mapping"][device]["device"], line)
					if props["direction"] == "out":
						props["value"] = value
				result["lines"][line]["value"] = value
						
		return result


	def has_direction(self, lines, direction):
		for line, props in lines.items():
			if props["direction"] == direction:
				return True
		return False


	def initialize_device_lines(self, device):
		lines = self.config["mapping"][device]["lines"]
		for line, props in lines.items():
			if props["direction"] != "out":
				continue
			with self.lock:
				value = props["value"] if "value" in props else False
				gpio.set_line(self.config["mapping"][device]["device"], line, value)
				props["value"] = value


	def initialize_lines(self):		
		for device, params in self.config["mapping"].items():
			try:
				if params["has_outputs"]:
					self.initialize_device_lines(device)
			except GPIOError as err:
				print(err)

