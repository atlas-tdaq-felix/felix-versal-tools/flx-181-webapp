import pdb
from util.sfp import SFP as SFP_Device
from util.i2c import I2CError
from util.devmem import devmem_write
import time
from models.self_test_executor import SelfTestExecutor


class SFP(SelfTestExecutor):
    def __init__(self, config):
        self.config = config
        self.devices = config["mapping"]

        
    # returns configuration of all known SI570 devices
    def read_all(self):            
        data = {}
        try:
            for name, config in self.devices.items():
                device = SFP_Device(config)
                data[name] = device.read()
        except I2CError as err:
            return {"error" : "Can't read SFP devices: {}".format(err)}       

        return data


    def self_test_main(self, **kwargs):
        return self.read_all()


    def self_test_check_result(self, result):
        msg_level = "success"
        msg = []

        if len(result.keys()) != 5:
            msg_level = "error"

        for label in self.config["mapping"].keys():

            # Missing FF
            if label not in result.keys():
                msg_level = "error"
                msg.append("{} not detected".format(label))
                continue

            # Wrong FF type
            reference = self.config["mapping"][label]
            reading = result[label]

            expected_type = reference['type']
            expected_y12 = True if "CERN" in expected_type else False
            read_part_number = reading['part_number']
            if expected_y12 and "Y12" not in read_part_number:
                msg_level = "error"
                msg.append("{} is not a Y12 module. (P/N: {})".format(label, read_part_number))
            if not expected_y12 and "B04" not in read_part_number:
                msg_level = "error"
                msg.append("{} is not a B04 module (P/N: {})".format(label, read_part_number))

            # Temperature
            ref_value = reference['max_temp']
            read_value = reading['temperature']
            if read_value > ref_value:
                msg_level = "error"
                msg.append("{} temp {} > {}".format(label, read_value, ref_value))

        if msg_level == "success":
            msg = "OK"

        return { "status" : msg_level, "result" : msg}

    
    def configure(self, device_name, config):
        cfg = self.devices[device_name]
        device = SFP_Device(cfg)
        device.configure(config)


    def configure_all(self):
        B04_link_speed = 0
        Y12_link_speed = 0

        #Read all devices to determine link speeds
        for dev_cfg in self.devices.values():
            device = SFP_Device(dev_cfg)
            part = device.get_part_number()
            if ("B04" in part and B04_link_speed == 0):
                if "B0428" in part:
                    B04_link_speed = 28
                elif "B0425" in part:
                    B04_link_speed = 25
                elif "B0414" in part:
                    B04_link_speed = 14
            if ("Y12" in part and Y12_link_speed == 0):
                if ("BY12" in part or "Y1216" in part):
                    Y12_link_speed = 16
                if "Y1225" in part:
                    Y12_link_speed = 25

        if (Y12_link_speed == 16 and B04_link_speed == 14):
            devmem_write(0x20100020008, int(0x00).to_bytes(4, 'little'))

        elif (Y12_link_speed == 16 and B04_link_speed >= 25):
            devmem_write(0x20100020008, int(0x10).to_bytes(4, 'little'))

        elif  (Y12_link_speed == 25 and B04_link_speed == 14):
            devmem_write(0x20100020008, int(0x01).to_bytes(4, 'little'))

        elif (Y12_link_speed == 25 and B04_link_speed >= 25):
            devmem_write(0x20100020008, int(0x11).to_bytes(4, 'little'))
        else:
            raise ValueError(f"Unsupported link speed. Detected: Y12 {Y12_link_speed}G B04 {B04_link_speed}G")


    def reset(self):
        devmem_write(0x20100020000, int(0xFFFFFFFF).to_bytes(4, 'little'))
        time.sleep(0.1)
        devmem_write(0x20100020000, int(0).to_bytes(4, 'little'))
