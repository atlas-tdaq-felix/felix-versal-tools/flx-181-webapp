import pdb
from .device_manager import DeviceManager, ensure_device_exists
from util.i2c import i2c_handler, I2CError, I2C


class SI53156(DeviceManager):
	'''
		Class to store and manage the SI53156 devices
	'''
	def __init__(self, config):
		super().__init__(SI53156_Device, config)


	@ensure_device_exists
	def configure_outputs(self, device_name, settings):
		return self.devices[device_name].configure_outputs(settings)


	def self_test_check_result(self, result):
		return self.self_test_check_device_list(result,
			human_readable_name_getter=lambda name: self.config["mapping"][name]["name"])


output_enable1_addr = 0x81
output_enable2_addr = 0x82
amp_sel_addr = 0x85

amplitudes = {
	0b000 : 300,
	0b001 : 400,
	0b010 : 500,
	0b011 : 600,
	0b100 : 700,
	0b101 : 800,
	0b110 : 900,
	0b111 : 1000,
}

amplitude_choices = [ (k, f"{v} mV") for k, v in amplitudes.items() ]

oe_mask = {
	0 : 0b00010000,
	1 : 0b00000100,
	2 : 0b00000001,
	3 : 0b10000000,
	4 : 0b01000000,
	5 : 0b00100000
}

class SI53156_Device():
	def __init__(self, config: dict):
		self.i2c_addr = config['i2c_addr']
		self.i2c_bus = config['i2c_bus']
		self.name = config['name']


	def read(self):
		result = {}
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				result["output_enabled"] = self.get_output_enable(i2c)
				amplitude_data = self.get_amplitude_control(i2c)
				result["amplitude"] = amplitude_data["amplitude"]
				result["set_amplitude"] = amplitude_data["set_amplitude"]
				result["name"] = self.name
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		return result


	def configure_outputs(self, settings):
		try:
			with i2c_handler(self.i2c_bus) as i2c:
				self.set_output_enable(i2c, settings)
				self.set_amplitude_control(i2c, settings)
		except I2CError as err:
			return { "error" : str(err), "status" : "error"}

		return { "status" : "success" }



	def get_amplitude_control(self, i2c: I2C):
		ctrl = i2c.read(i2c_addr=self.i2c_addr, reg_addr=amp_sel_addr)
		set_amplitude = bool(ctrl & 0b10000000)
		amp_sel = (ctrl & 0b01110000) >> 4

		if not amp_sel in amplitudes:
			raise ValueError(f"Invalid amplitude select index: {amp_sel}")

		return {
			"set_amplitude" : set_amplitude,
			"amplitude" : { "amp_sel" : amp_sel, "value": amplitudes[amp_sel]}
		}


	def set_amplitude_control(self, i2c: I2C, settings: dict):
		ctrl = 0
		amp_sel = settings["amp_sel"]

		if settings["set_amplitude"]:
			ctrl |= 0b10000000

		if not amp_sel in amplitudes:
			raise ValueError(f"Invalid amplitude select index: {amp_sel}")

		ctrl |= amp_sel << 4
		i2c.write(i2c_addr=self.i2c_addr, reg_addr=amp_sel_addr, reg_val=ctrl)


	def get_output_enable(self, i2c: I2C):
		oe1 = i2c.read(i2c_addr=self.i2c_addr, reg_addr=output_enable1_addr)
		oe2 = i2c.read(i2c_addr=self.i2c_addr, reg_addr=output_enable2_addr)
		return {
			0 : bool(oe1 & oe_mask[0]),
			1 : bool(oe1 & oe_mask[1]),
			2 : bool(oe1 & oe_mask[2]),
			3 : bool(oe2 & oe_mask[3]),
			4 : bool(oe2 & oe_mask[4]),
			5 : bool(oe2 & oe_mask[5]),
		}


	def set_output_enable(self, i2c: I2C, settings: dict):
		oe1 = 0
		oe2 = 0
		output_enable = settings["output_enable"]

		for i in [0, 1, 2]:
			if output_enable[i]:
				oe1 |= oe_mask[i]

		for i in [3, 4, 5]:
			if output_enable[i]:
				oe2 |= oe_mask[i]

		i2c.write(i2c_addr=self.i2c_addr, reg_addr=output_enable1_addr, reg_val=oe1)
		i2c.write(i2c_addr=self.i2c_addr, reg_addr=output_enable2_addr, reg_val=oe2)



