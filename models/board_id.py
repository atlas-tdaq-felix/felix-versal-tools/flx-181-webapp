import pdb, re, mmap, os
from os import path, sysconf
from os.path import isdir
from util.iio import load_file
from util.devmem import devmem_read
from models.self_test_executor import SelfTestExecutor, field_present


class BoardID(SelfTestExecutor):
    # This class is responsible for identifying the specific board
    # (e.g to find it in the database)
    def __init__(self, config):
        self.config = config
        self.idcode = None


    def get_id(self):
        if not "interface" in self.config:
            return {"error" : "You must specify interface type for retrieving the IDCODE!" }

        if self.config['interface'] == 'zynq_mpsoc':
            return self.get_id_zynq_mpsoc()            

        else:
            return {"error" : "Unsupported interface for retrieving the IDCODE" }


    def get_id_zynq_mpsoc(self):
        if not self.idcode is None:
            return self.idcode

        if "pm_path" in self.config:
            pm_path = self.config['pm_path']
        else:
            pm_path = "/sys/kernel/debug/zynqmp-firmware/pm"        

        try:
            with open(pm_path, 'w') as file:
                file.write('pm_get_chipid')
            pm_response = load_file(pm_path)
        except (FileNotFoundError, PermissionError, IsADirectoryError) as err:
            return {"error" : str(err)}

        regex = r'idcode:\s*0x(?P<idcode>[0-9a-fA-F]+)'
        match = re.search(regex, pm_response, re.IGNORECASE)
        if not match:
            return {"error" : f"Cannot extract IDCODE from string '{pm_response}'"}
        
        idcode = int(match.group('idcode'), 16)
        return hex(idcode)


    def get_dna(self):
        if not "DNAaddress" in self.config:
            return {"error" : "You must specify a memory address to retrieve the FPGA DNA!" }
        else:
            dna_msb = int.from_bytes(devmem_read(self.config['DNAaddress'][0]), "little")
            dna_lsb = int.from_bytes(devmem_read(self.config['DNAaddress'][1]), "little")
            dna = ( dna_msb << 32 ) |  dna_lsb
            return hex(dna) 


    def get_board_name(self):
        board = self.config['board']
        revision = self.config['revision']
        idcode = self.get_id()
        dna = self.get_dna()
        return f"{board}_rev_{revision}_id_{idcode}_dna_{dna}"


    def get_board_info(self):
        result = {"board" : self.config['board'], "revision" : self.config['revision'], "idcode" : self.get_id(), "dna" : self.get_dna()}
        return result


    def self_test_main(self, **kwargs):
        return self.get_board_info()


    def self_test_check_result(self, result):

        if result is None:
            return { "status" : "error", "result" : "Test results are missing" }

        if not field_present(result, "idcode"):
            return { "status" : "error", "result" : "IDCODE is missing" }

        if not field_present(result, "board"):
            return { "status" : "error", "result" : "Board name is not specified" }

        if not field_present(result, "revision"):
            return { "status" : "error", "result" : "Board revision is not specified" }

        if not field_present(result, "dna"):
            return { "status" : "error", "result" : "DNA is not specified" }

        return { "status" : "success", "result" : result["dna"] }


