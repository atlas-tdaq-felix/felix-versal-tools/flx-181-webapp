#from util.chipscopy import ChipscopyWrapper
import pdb, re, tempfile, os, types
from threading import Lock

class Chipscopy_DDRMC():
	def __init__(self, config, chipscopy):
		self.config = config
		self.chipscopy = chipscopy
		self.file_lock = Lock()


	def read_all(self):
		if self.chipscopy is None:
			return {"error" : "Chipscopy configuration is missing, check the board definition file!"}

		if not self.chipscopy.connected():
			return {"error" : "Chipscopy is not coonnected!"}

		result = {}
		device = self.chipscopy.device
		total_cores = len(device.ddrs)
		enabled_cores = 0
		for ddr in device.ddrs:
			if ddr.is_enabled:
				enabled_cores += 1
				result[ddr.name] = read_core(ddr)

		return result


def read_core(ddr):
	result = {}

	# attach own function to the DDR instance for making proper reports
	# I am doing it because the original method is just too poorly written to be usable
	# "better_report" is likely to break in future releases of chipscopy
	if not callable(getattr(ddr, "better_report", None)):
		ddr.better_report = types.MethodType(better_report, ddr)

	result["report"] = ddr.better_report()
	result["margins"] = {}
	result["status"] = ddr.get_cal_status()
	result["error_msg"] = ddr.ddr_node.get_property("cal_message")

	margins = ddr.get_cal_margin_mode()
	for margin, enabled in margins.items():
		if enabled:
			result["margins"][margin] = read_margin(ddr, margin)
	return result


def read_margin(ddr, margin):
	result = {"rise" : {}, "fall" : {}, "other" : {}}
	if "rd" in margin:
		result["type"] = "read"
		groups = [ f'{margin}_rise_left', f'{margin}_rise_right', f'{margin}_rise_center', 
			f'{margin}_fall_left', f'{margin}_fall_right', f'{margin}_fall_center']
	else:
		result["type"] = "write"
		groups = [ f'{margin}_left', f'{margin}_right', f'{margin}_center']

	# TODO: this should be a regex
	if margin == "f0_rd_simp":
		result["comment"] = "Simple read pattern, F0"
	elif margin == "f1_rd_simp":
		result["comment"] = "Simple read pattern, F1"
	elif margin == "f0_wr_simp":
		result["comment"] = "Simple write pattern, F0"
	elif margin == "f1_wr_simp":
		result["comment"] = "Simple write pattern, F1"
	elif margin == "f0_rd_comp":
		result["comment"] = "Complex read pattern, F0"
	elif margin == "f1_rd_comp":
		result["comment"] = "Complex read pattern, F1"
	elif margin == "f0_wr_comp":
		result["comment"] = "Complex write pattern, F0"
	elif margin == "f1_wr_comp":
		result["comment"] = "Complex write pattern, F1"
	else:
		result["comment"] = margin

	edges = set()

	margins = ddr.ddr_node.get_property_group(groups)
	regex = re.compile(f"^{margin}_?(?P<edge>fall|rise)?_ctp_(?P<item>[a-zA-Z]+_\\d+)$")

	for name, values in margins.items():
		match = regex.match(name)
		if not match:
			continue

		item = match["item"]
		if not match.groupdict().get('edge') is None:
			edge = match["edge"]
			values_right = margins[f"{margin}_{edge}_rm_{item}"]
			values_left = margins[f"{margin}_{edge}_lm_{item}"]
			max_tap=255

		else:
			values_right = margins[f"{margin}_rm_{item}"]
			values_left = margins[f"{margin}_lm_{item}"]
			edge = "other"
			max_tap=512

		result[edge][item] = {
			"center" : values, "right" : values_right, "left" : values_left,
			"plot_bar" : progress_bar(lm_tap=values_left[0],
				rm_tap=values_right[0], center_tap=values[0], max_tap=max_tap)}

		edges.add(edge)

	result["edges"] = list(edges)

	return result



def progress_bar(lm_tap, rm_tap, center_tap, max_tap=255, max_edge=100, tap_width=1):
	'''
		Calculates progress bar based on the margin values
		tap_width = what percentage of the bar plot should center tap marker take
	'''

	free_space = max_edge - tap_width

	left = center_tap - lm_tap
	right = center_tap + rm_tap

	left_margin = lm_tap / max_tap * free_space
	right_margin = rm_tap / max_tap * free_space
	left_edge = left / max_tap * free_space
	right_edge = (max_tap - right) / max_tap * free_space

	#pdb.set_trace()

	return {
		"left_edge" : left_edge,
		"left_margin" : left_margin,
		"left_margin_value" : lm_tap,
		"tap_width" : tap_width,
		"right_margin" : right_margin,
		"right_margin_value" : rm_tap,
		"right_edge" : right_edge,
		"left_value" : left,
		"right_value" : right,
		"center_value" : center_tap,
	}




def better_report(self, to_file=False, file_name=None):
		"""
		Run a report on the current statuses and analytical data of the DDRMC
		Return result as a string

		"""

		with tempfile.TemporaryFile() as file:

			# DDRMC Status & Message
			file.write(b"-------------------\n")
			file.write(b" DDRMC Status \n")
			file.write(b"-------------------\n")
			self.refresh_cal_status()
			self.refresh_health_status()
			cal_status = self.get_cal_status()
			file.write(f'Calibration Status:  {cal_status}\n'.encode())
			results = self.ddr_node.get_property("health_status")
			file.write(f'Overall Health:  {results["health_status"]}\n'.encode())
			results = self.ddr_node.get_property("cal_message")
			file.write(f'Message:  {results["cal_message"]}\n'.encode())

			# Check if there is error condition to report
			results = self.ddr_node.get_property("cal_error_msg")
			if results["cal_error_msg"] != "None":
				file.writef(f'Error:  {results["cal_error_msg"]}\n'.encode())

			# DDRMC ISR Registers
			file.write(b"\n-------------------\n")
			file.write(b" Status Registers\n")
			file.write(b"-------------------\n")

			file.write(b"DDRMC ISR Table\n")
			results = self.ddr_node.get_property_group(["ddrmc_isr_e", "ddrmc_isr_w"])
			for key, val in sorted(results.items()):
				file.write(f"  {key}:  {str(val)}\n".encode())

			file.write(b"\nUB ISR Table\n")
			results = self.ddr_node.get_property_group(["ub_isr_e", "ub_isr_w"])
			for key, val in sorted(results.items()):
				file.write(f"  {key}:  {str(val)}\n".encode())

			# Memory configuration info
			file.write(b"\n----------------------------------\n")
			file.write(b" Memory Configuration \n")
			file.write(b"----------------------------------\n")
			configs = self._DDR__get_configuration()
			for key, val in configs.items():
				file.write(f"  {key}:  {str(val)}\n".encode())

			# Memory calibration stages info
			file.write(b"\n-----------------------------------\n")
			file.write(b" Calibration Stages Information \n")
			file.write(b"-----------------------------------\n")
			# Do not do calibration stages if a system error is detected
			sys_error = False
			if self.get_cal_status() == "SYSTEM ERROR":
				sys_error = True
			if not sys_error:
				results = self.get_cal_stages()
				for key, val in sorted(results.items()):
					file.write(f"  {key}:  {val}\n".encode())
			else:
				file.write(f"\nNote: DDRMC system error is detected. Calibration stages decoding will not be provided for {self.name}\n".encode())

			# Cal Margin Analysis
			file.write(b"\n---------------------------------------\n")
			file.write(b" Calibration Window Margin Analysis \n")
			file.write(b"---------------------------------------\n")

			if not sys_error:
				cal_margin_modes = {}
				left_margins = {}
				right_margins = {}
				center_points = {}
				is_by_8 = True
				num_byte = int(configs["Bytes"])
				num_nibble = int(configs["Nibbles"])
				self.refresh_cal_margin()
				cal_margin_modes = self.get_cal_margin_mode()
				if int(configs["Bits per Byte"]) == 4:
					is_by_8 = False

				# Main loop to go through dual frequencies
				for freq in range(2):
					base = "Frequency " + str(freq)
					# Read Simple
					key = "f" + str(freq) + "_rd_simp"
					if cal_margin_modes[key]:
						# Rising Edge
						self._DDR__get_cal_margins(key + "_rise", left_margins, right_margins, center_points)
						file.write(f"\n{base} - Read Margin - Simple Pattern - Rising Edge Clock in pS and (delay taps):\n\n".encode())
						file.write(format_read_margins(is_by_8, num_byte, num_nibble, left_margins, right_margins, center_points).encode())
						# Falling Edge
						self._DDR__get_cal_margins(key + "_fall", left_margins, right_margins, center_points)
						file.write(f"\n{base} - Read Margin - Simple Pattern - Falling Edge Clock in pS and (delay taps):\n\n".encode())
						file.write(format_read_margins(is_by_8, num_byte, num_nibble, left_margins, right_margins, center_points).encode())
					# Read Complex
					key = "f" + str(freq) + "_rd_comp"
					if cal_margin_modes[key]:
						# Rising Edge
						self._DDR__get_cal_margins(key + "_rise", left_margins, right_margins, center_points)
						file.write(f"\n{base} - Read Margin - Complex Pattern - Rising Edge Clock in pS and (delay taps):\n\n".encode())
						file.write(format_read_margins(is_by_8, num_byte, num_nibble, left_margins, right_margins, center_points).encode())
						# Falling Edge
						self._DDR__get_cal_margins(key + "_fall", left_margins, right_margins, center_points)
						file.write(f"\n{base} - Read Margin - Complex Pattern - Falling Edge Clock in pS and (delay taps):\n\n".encode())
						file.write(format_read_margins(is_by_8, num_byte, num_nibble, left_margins, right_margins, center_points).encode())
					# Write Simple
					key = "f" + str(freq) + "_wr_simp"
					if cal_margin_modes[key]:
						self._DDR__get_cal_margins(key, left_margins, right_margins, center_points)
						file.write(f"\n{base} - Write Margin - Simple Pattern - Calibration Window in pS and (delay taps):\n\n".encode())
						file.write(format_write_margins(num_byte, left_margins, right_margins, center_points).encode())
					# Write Complex
					key = "f" + str(freq) + "_wr_comp"
					if cal_margin_modes[key]:
						self._DDR__get_cal_margins(key, left_margins, right_margins, center_points)
						file.write(f"\n{base} - Write Margin - Complex Pattern - Calibration Window in pS and (delay taps):\n\n".encode())
						file.write(format_write_margins(num_byte, left_margins, right_margins, center_points).encode())
			else:
				file.write(f"\nNote: DDRMC system error is detected. Margin Analysis will not be provided for {self.name}\n\n".encode())

			file.seek(0)
			result = file.read().decode()

		return result


def format_read_margins(by_8_mode, bytes, nibbles, left_margs, right_margs,	centers):
	left_marg_vals = sorted(left_margs.items())
	center_vals = sorted(centers.items())
	right_marg_vals = sorted(right_margs.items())
	result = ""

	if by_8_mode:
		for byte in range(bytes):
			left_marg = left_marg_vals[byte * 2][1]
			center = center_vals[byte * 2][1]
			right_marg = right_marg_vals[byte * 2][1]
			result += (f"Byte {byte} Nibble 0  -  Left Margin:  {left_marg[1]} ({left_marg[0]})  "
				+ f"Center Point:  {center[1]} ({center[0]})  Right Margin:  {right_marg[1]} ({right_marg[0]})\n")

			left_marg = left_marg_vals[byte * 2 + 1][1]
			center = center_vals[byte * 2 + 1][1]
			right_marg = right_marg_vals[byte * 2 + 1][1]
			result += (f"Byte {byte} Nibble 1  -  Left Margin:  {left_marg[1]} ({left_marg[0]})  "
				+ f"Center Point:  {center[1]} ({center[0]})  Right Margin:  {right_marg[1]} ({right_marg[0]})\n")
	else:
		for nibble in range(nibbles):
			left_marg = left_marg_vals[nibble][1]
			center = center_vals[nibble][1]
			right_marg = right_marg_vals[nibble][1]
			result += (f"Nibble {nibble}  -  Left Margin:  {left_marg[1]} ({left_marg[0]})  "
				+ f"Center Point:  {center[1]} ({center[0]})  Right Margin:  {right_marg[1]} ({right_marg[0]})\n")

	return result


def format_write_margins(bytes, left_margs, right_margs, centers):
	left_marg_vals = sorted(left_margs.items())
	center_vals = sorted(centers.items())
	right_marg_vals = sorted(right_margs.items())
	result = ""

	for byte in range(bytes):
		left_marg = left_marg_vals[byte][1]
		center = center_vals[byte][1]
		right_marg = right_marg_vals[byte][1]
		result += (f"Byte {byte}  -  Left Margin:  {left_marg[1]} ({left_marg[0]})  "
			+ f"Center Point:  {center[1]} ({center[0]})  Right Margin:  {right_marg[1]} ({right_marg[0]})\n")
	return result