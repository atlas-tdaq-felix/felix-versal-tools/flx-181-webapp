i2c_data = """
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: 2a 2e 00 00 05 55 00 55 00 00 05 55 00 55 00 0d    *...?U.U..?U.U.?
10: 30 00 00 00 00 60 00 00 00 55 1c XX XX XX XX 00    0....`...U?XXXX.
20: 55 0a 70 XX XX 0f XX XX XX XX XX XX XX XX XX XX    U?pXX?XXXXXXXXXX
30: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
40: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
50: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
60: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
70: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
80: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
90: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
a0: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
b0: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
c0: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
d0: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
e0: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
f0: XX XX XX XX XX XX XX XX XX XX XX XX 00 35 55 35    XXXXXXXXXXXX.5U5
"""

tmp435_data = {'class': 'tmp435',
 'devices': {'/sys/class/hwmon/hwmon0': {'inputs': {'temp1_input': 42313.0,
                                                    'temp2_input': 46063.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'MGTAVCC_TMP'},
             '/sys/class/hwmon/hwmon1': {'inputs': {'temp1_input': 41313.0,
                                                    'temp2_input': 45250.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'MGTAVTT_TMP'},
             '/sys/class/hwmon/hwmon2': {'inputs': {'temp1_input': 40313.0,
                                                    'temp2_input': 44688.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'SYS12_TMP'},
             '/sys/class/hwmon/hwmon3': {'inputs': {'temp1_input': 41188.0,
                                                    'temp2_input': 46688.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'SYS15_TMP'},
             '/sys/class/hwmon/hwmon4': {'inputs': {'temp1_input': 42750.0,
                                                    'temp2_input': 49250.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'SYS18_TMP'},
             '/sys/class/hwmon/hwmon5': {'inputs': {'temp1_input': 43188.0,
                                                    'temp2_input': 56375.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'SYS33_TMP'},
             '/sys/class/hwmon/hwmon6': {'inputs': {'temp1_input': 43188.0,
                                                    'temp2_input': 0.0},
                                         'max': {'temp1_max': 85000.0,
                                                 'temp2_max': 0.0},
                                         'min': {'temp1_min': 0.0,
                                                 'temp2_min': 0.0},
                                         'name': 'LTM4642_TMP'}},
 'title': 'TMP435'}