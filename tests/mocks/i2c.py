from periphery import I2CError
from util.i2c import I2C
import re, pdb, contextlib

# Regexp for parsing output of i2cdump utility
regex = re.compile("^(?P<row>[a-fA-F\\d]{2}):\\s(?P<values>(?:[Xa-fA-F\\d]{2}\\s){16})")


def parse_values(values):
	def parse_one(value):
		try:
			return int(value, 16)
		except Exception:
			return None

	values.strip()
	return [parse_one(x) for x in values.split()]


def parse_dump(dump):
	result = [None] * 256
	for line in dump.splitlines():
		match = regex.match(line)
		if match:
			row_address = int(match["row"], 16)
			values = parse_values(match["values"])
			result[row_address:row_address + 16] = values
	return result


class I2C_Mock():
	"""
		Class for simulating I2C devices based on the i2cdump data
	"""
	def __init__(self, dump):
		self.data = parse_dump(dump)

	def read(self, i2c_addr, reg_addr):
		data = self.data[reg_addr]
		if data is None:
			raise I2CError("I2C read error: no such address")
		return data


	def write(self, i2c_addr, reg_addr, reg_data):
		self.data[reg_addr] = reg_data


	def open(self, i2c_bus):
		pass


	def close(self):
		pass


def make_i2c_handler_mock(mock):	
	@contextlib.contextmanager
	def i2c_handler_mock(i2c_bus):
		try:
			mock.open(i2c_bus)			
			yield mock
		finally:
			mock.close()

	return i2c_handler_mock