import pdb, pytest
from unittest.mock import call
from models.gpio import GPIO
from util.gpio import GPIOError


@pytest.fixture(scope="function")
def config():
	config = {
		"mapping" : {
			"gpiochip0" : {
				"lines" : {
					0 : {
						"label" : "DIP_SW_0 (SW6)",
						"direction" : "in"
					},
					1 : {
						"label" : "LED_0",
						"direction" : "out",
						"value" : False
					},
					2 : {
						"label" : "LED_1",
						"direction" : "out",
						"value" : True
					},
					3: {
						"label" : "LED_2",
						"direction" : "out"
					},
					5 : {
						"label" : "DIP_SW_1 (SW6)",
						"direction" : "in"
					}
				}
			}
		}
	}
	return config


@pytest.fixture(scope="function")
def util_gpio(mocker):
	util_gpio = mocker.patch("models.gpio.gpio")
	return util_gpio


def test_init(config, util_gpio):
	"""
		Verify that the GPIO model resets the output values
		according to the configuration when initialized
	"""
	set_line = util_gpio.set_line
	gpio = GPIO(config)

	assert set_line.call_count == 3
	calls = [
		call("gpiochip0", 1, False),
		call("gpiochip0", 2, True),
		call("gpiochip0", 3, False)]
	set_line.assert_has_calls(calls, any_order=True)


def test_set_bit(config, util_gpio):
	"""
		Verify GPIO line assignments and parameter validation
	"""
	set_line = util_gpio.set_line
	gpio = GPIO(config)
	set_line.reset_mock()

	# no chip
	result = gpio.set("no_such_gpio", {})
	assert "error" in result

	# no line
	result = gpio.set("gpiochip0", {8 : True})
	assert "error" in result

	# wrong line direction
	result = gpio.set("gpiochip0", {0 : True})
	assert "error" in result

	# bogus bit index
	result = gpio.set("gpiochip0", {"bad" : True})
	assert "error" in result

	# bogus value
	result = gpio.set("gpiochip0", {1 : ""})
	assert "error" in result

	set_line.assert_not_called()

	# valid assignments
	result = gpio.set("gpiochip0", {"1" : 0})
	assert result == {1 : 0}
	set_line.assert_called_with("gpiochip0", 1, False)

	result = gpio.set("gpiochip0", {"1" : 1})
	assert result == {1 : 1}
	set_line.assert_called_with("gpiochip0", 1, True)

	result = gpio.set("gpiochip0", {1 : 0})
	assert result == {1 : 0}
	set_line.assert_called_with("gpiochip0", 1, False)

	result = gpio.set("gpiochip0", {1 : 1})
	assert result == {1 : 1}
	set_line.assert_called_with("gpiochip0", 1, True)

	result = gpio.set("gpiochip0", {"1" : "0"})
	assert result == {1 : 0}
	set_line.assert_called_with("gpiochip0", 1, False)

	result = gpio.set("gpiochip0", {"1" : "1"})
	assert result == {1 : 1}
	set_line.assert_called_with("gpiochip0", 1, True)

	result = gpio.set("gpiochip0", {"1" : "y"})
	assert result == {1 : 1}
	set_line.assert_called_with("gpiochip0", 1, True)

	result = gpio.set("gpiochip0", {"1" : "True"})
	assert result == {1 : 1}
	set_line.assert_called_with("gpiochip0", 1, True)


def test_read_all(config, util_gpio):
	"""
		Verify that GPIO lines are read out correctly
	"""
	def mock_reads(device, line):
		result = {0 : True, 5 : False}
		return result[line]

	get_line = util_gpio.get_line
	gpio = GPIO(config)

	get_line.configure_mock(side_effect=mock_reads)
	result = gpio.read_all()
	calls = [call("gpiochip0", 0), call("gpiochip0", 5)]
	get_line.assert_has_calls(calls, any_order=True)	
	assert result["gpiochip0"]["lines"][0]["value"] == True
	assert result["gpiochip0"]["lines"][5]["value"] == False


def test_no_gpio_chip(config, util_gpio):
	"""
		Verify that when the gpio device doesn't exist, the 
		model returns an error
	"""
	msg = "GPIO device not found"
	util_gpio.set_line.configure_mock(side_effect=GPIOError(msg))
	util_gpio.get_line.configure_mock(side_effect=GPIOError(msg))

	gpio = GPIO(config)
	result = gpio.set("gpiochip0", {"1" : "True"})
	assert result == {"error" : msg}
	result = gpio.read_all()
	assert result["gpiochip0"] == {"error" : msg}

	