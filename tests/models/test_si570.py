import pdb, pytest, math

from unittest.mock import call
from models.si570 import SI570
from tests.mocks.i2c import I2C_Mock
from tests.mocks.vmk180 import zsfp_clk


@pytest.fixture(scope="function")
def config():
	config = {
		"mapping" : {
			"zsfp_clk" : {
				"i2c_bus" : "/dev/i2c-7",
				"i2c_addr" : 0x5d,
				"protected" : False,
				"freq_MHz" : 156.25,
				"stability" : "20ppm",
			}
		}
	}
	return config


def test_read_zsfp_clk(config, mocker):
	mock = I2C_Mock(zsfp_clk)
	mocker.patch("util.i2c.I2C", return_value=mock)
	
	si570 = SI570(config)	
	result = si570.read_all()
	assert result["zsfp_clk"]["HS_DIV"] == 4
	assert result["zsfp_clk"]["N1"] == 8
	assert math.isclose(result["zsfp_clk"]["FOUT"], 156.25, rel_tol=1e-4)
	assert math.isclose(result["zsfp_clk"]["RFREQ"], 43.78564, rel_tol=1e-4)
	