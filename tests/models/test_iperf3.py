import pdb, pytest
from unittest.mock import call, mock_open, MagicMock
from models.iperf3 import Iperf3
from tests.mocks import AppMock


@pytest.fixture()
def config():
	config = {
		"binary" : ["/some/binary"],
		"name" : "test",
		"title" : "Test title",
		"app" : AppMock(),
	}
	return config


def test_ifconfig(config, mocker):
	'''
		Returns ifconfig output
	'''
	check_output = mocker.patch("models.iperf3.check_output",
		return_value="ifconfig".encode("utf-8"))
	model = Iperf3(config)
	result = model.ifconfig()
	assert result == "ifconfig"


def test_params(config, mocker):
	'''
		Verifies input parameters and passes them to the base class
	'''
	start_test = mocker.patch("models.iperf3.ProcessRunner.start_test")
	model = Iperf3(config)
	ip = "127.0.0.1"

	# No server IP address
	result = model.start_test()
	assert "error" in result

	# specify duration	
	result = model.start_test(server=ip, duration=123)
	start_test.assert_called_with(args=["-t", "123", "--forceflush", "-c", ip])

	# specify bitrate
	result = model.start_test(server=ip, bitrate=345)
	start_test.assert_called_with(args=["-b", "345", "--forceflush", "-c", ip])

	# specify buffer length
	result = model.start_test(server=ip, buffer_length=456)
	start_test.assert_called_with(args=["-l", "456", "--forceflush", "-c", ip])

	# specify window size
	result = model.start_test(server=ip, window_size=6345)
	start_test.assert_called_with(args=["-w", "6345", "--forceflush", "-c", ip])

	# specify segment size
	result = model.start_test(server=ip, segment_size=65)
	start_test.assert_called_with(args=["-M", "65", "--forceflush", "-c", ip])

	# use UDP
	result = model.start_test(server=ip, udp=True)
	start_test.assert_called_with(args=["-u", "--forceflush", "-c", ip])

	# Reverse
	result = model.start_test(server=ip, reverse=True)
	start_test.assert_called_with(args=["-R", "--forceflush", "-c", ip])

	# Other arguments
	result = model.start_test(server=ip, other="-g 213 -b Mmee -p 512")
	start_test.assert_called_with(
		args=["-g", "213", "-b", "Mmee", "-p", "512", "--forceflush", "-c", ip])