import pdb, pytest
from models.sysmon import SYSMON
import os, math


@pytest.fixture()
def config():
	config = {		
		"device_classes" : {
			"lltc,ltm4700" : {
				"device_path" : "/sys/bus/iio/devices/",
				"title" : "LTM4700",						
				"patterns" : {
					"voltage" : "out_voltage(?P<id>\\d+)_(?P<name>.+)",
				},
			},
			"xlnx,versal-sysmon" : {
				"device_path" : "/sys/bus/iio/devices/",
				"title" : "SYSMON",
				"patterns" : {
					"voltage" : "in_voltage(?P<id>\\d+)_(?P<name>.+)_input",
					"temperature" : "in_temp(?P<id>\\d+)_(?P<name>.+)_input",				
				}
			},
		},
	}
	return config


@pytest.fixture()
def devices():
	return {
		"lltc,ltm4700" : {
			"/sys/bus/iio/devices/iio:device0" : {
				"out_voltage0_FPGA_ref0" : "1.85",
				"out_voltage1_FPGA_ref1" : "1.83",
				"label" : "FPGA_reference"
			},
			"/sys/bus/iio/devices/iio:device0/of_node" : {
				"label" : "FPGA_reference"
			},
			"/sys/bus/iio/devices/iio:device1" : {
				"out_voltage0_Vcc_ref0" : "2.55",
				"out_voltage1_Vcc_ref1" : "2.43",
				"label" : "Vcc_reference"
			},
			"/sys/bus/iio/devices/iio:device1/of_node" : {
				"label" : "Vcc_reference"
			}
		}
	}


class HwmonMock():
	def __init__(self, devices):
		self.devices = devices
		self.device_class = None


	def get_devices(self, device_path, device_class):
		if device_class in self.devices:
			devices = [x for x in self.devices[device_class].keys() if "of_node" not in x]
			return devices
		else:
			return []


	def listdir(self, path):
		for device_class, devices in self.devices.items():
			if path in devices:
				self.device_class = device_class
				self.path = path
				return devices[path].keys()
		self.device_class = None
		raise FileNotFoundError


	def load_file(self, path):		
		dir_path, file_name = os.path.split(path)
		device_class = self.devices[self.device_class]

		if (self.device_class is None or not dir_path in device_class
			or not file_name in device_class[dir_path]):
			raise FileNotFoundError

		return device_class[dir_path][file_name]


@pytest.fixture()
def mocks(mocker, devices):
	mocks = {}
	mocks["load_file"] = mocker.patch("models.sysmon.load_file")
	mocks["listdir"] = mocker.patch("models.sysmon.listdir")
	mocks["isfile"] = mocker.patch("models.sysmon.isfile", returns=True)
	mocks["get_devices"] = mocker.patch("models.sysmon.get_devices")

	hwmock = HwmonMock(devices)
	mocks["get_devices"].configure_mock(side_effect=hwmock.get_devices)
	mocks["listdir"].configure_mock(side_effect=hwmock.listdir)
	mocks["load_file"].configure_mock(side_effect=hwmock.load_file)

	return mocks



def test_sysmon(config, mocks):
	"""
		Verify that the expected devices are listed by the model
	"""
	
	sysmon = SYSMON(config)
	result = sysmon.read_all("lltc,ltm4700")

	assert result["title"] == "LTM4700"
	assert result["devices"]["/sys/bus/iio/devices/iio:device0"]["name"] == "FPGA_reference"
	assert result["devices"]["/sys/bus/iio/devices/iio:device0"]["voltage"]["FPGA_ref0"] == 1.85
	assert result["devices"]["/sys/bus/iio/devices/iio:device0"]["voltage"]["FPGA_ref1"] == 1.83
	assert result["devices"]["/sys/bus/iio/devices/iio:device1"]["name"] == "Vcc_reference"
	assert result["devices"]["/sys/bus/iio/devices/iio:device1"]["voltage"]["Vcc_ref0"] == 2.55
	assert result["devices"]["/sys/bus/iio/devices/iio:device1"]["voltage"]["Vcc_ref1"] == 2.43


def test_no_device(config, mocks):
	"""
		Verifies that the empty result is returned when the specified
		device doesn't exist
	"""

	sysmon = SYSMON(config)

	result = sysmon.read_all("unknown")
	assert "error" in result

	result = sysmon.read_all("xlnx,versal-sysmon")
	assert "error" in result

