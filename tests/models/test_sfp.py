import pdb, pytest
import os, math
from unittest.mock import MagicMock, Mock, patch

from tests.mocks.i2c import I2C_Mock
from tests.mocks.vmk180 import qsfp_molex
from util.gpio import GPIOError
from util.i2c import I2CError
from util.sfp import SFP as SFP_Device
from util.sfp import TransactionValidator
from models.sfp import SFP


@pytest.fixture()
def config():
	return {
		"mapping" : {
			"Molex_0" : {
				"i2c_bus" : "/dev/i2c-5",
				"i2c_addr" : 0x50,
				"select" : {
					"gpio" : "gpiochip2",
					"line" : 10,
					"active_level" : 0
				}
			},
			"Molex_1" : {
				"i2c_bus" : "/dev/i2c-5",
				"i2c_addr" : 0x50,
				"select" : {
					"gpio" : "gpiochip2",
					"line" : 14,
					"active_level" : 0
				}
			},
		}
	}


@pytest.fixture
def validator(config, mocker):	
	molex = I2C_Mock(qsfp_molex)
	validator = TransactionValidator(config=config["mapping"]["Molex_0"],
		i2c=molex, set_line=MagicMock())
	i2c = mocker.patch("util.sfp.I2C", return_value=validator.i2c)			

	result = Mock()
	setattr(result, 'transaction_valid', validator.transaction_valid)
	setattr(result, 'i2c', validator.i2c)
	# setattr(result, 'set_line', set_line)
	setattr(result, "object", validator)
	return result


def assert_empty_data(data):
		assert "N/A" in data["type"]
		assert "N/A" in data["vendor"]
		assert "N/A" in data["part_number"]
		assert "N/A" in data["serial"]


def test_sfp_device_gpio_fail(config, mocker):
	'''
		Verify N/A is returned for data when setting GPIO lines fails
	'''
	molex = I2C_Mock(qsfp_molex)
	mocker.patch("util.sfp.I2C", return_value=molex)
	mocker.patch("util.sfp.set_line", side_effect=GPIOError("GPIO error"))
	sfp = SFP_Device(config["mapping"]["Molex_0"])
	data = sfp.read()	
	assert_empty_data(data)


def test_qsfp_device_read(config, validator, mocker):
	'''
		Verify correct data is returned when a QSFP module is read out
	'''
	set_line = mocker.patch("util.sfp.set_line", validator.object.gpio_set_line)
	sfp = SFP_Device(config["mapping"]["Molex_0"])
	data = sfp.read()	
	
	assert "QSFP" in data["type"]
	assert "Molex" in data["vendor"]
	assert "74763-0020" in data["part_number"]
	assert "720620030" in data["serial"]	
	assert validator.transaction_valid()


# FIXME: the delegate fails to raise the exception during the test
# def test_sfp_device_i2c_fail(config, validator):	
# 	patch.object(validator.object.i2c_delegate, 'read', side_effect=Exception("Error reading I2C"))
# 	sfp = SFP_Device(config["mapping"]["Molex_0"])
# 	data = sfp.read()	
# 	assert_empty_data(data)
	

def test_sfp_model_read(config, mocker):
	device_inst = MagicMock()	
	device_inst.read.configure_mock(side_effect=["result1", "result2"])
	mocker.patch('models.sfp.SFP_Device', return_value=device_inst)

	sfp_model = SFP(config)
	data = sfp_model.read_all()
	assert data["Molex_0"] == "result1"
	assert data["Molex_1"] == "result2"



def test_sfp_model_read_fail(config, mocker):
	device_inst = MagicMock()	
	device_inst.read.configure_mock(side_effect=I2CError("SFP readout failed"))
	sfp_device_class = mocker.patch('models.sfp.SFP_Device', return_value=device_inst)

	sfp_model = SFP(config)
	data = sfp_model.read_all()
	assert "error" in data