import pdb, pytest
from unittest.mock import call, mock_open, MagicMock
from models.memtester import Memtester
from tests.mocks import AppMock


meminfo = """
MemTotal:       32795928 kB
MemFree:        26493224 kB
MemAvailable:   28666952 kB
"""


@pytest.fixture()
def mocks(mocker):
	mocks = {}
	mocks["start_test"] = mocker.patch("models.memtester.ProcessRunner.start_test")
	mocks["open"] = mocker.patch("builtins.open", mock_open(read_data=meminfo))
	return mocks


@pytest.fixture()
def config():
	config = {
		"binary" : ["/some/binary"],
		"name" : "test",
		"title" : "Test title",
		"mask_default" : "123",
		"mask_variable" : "MEMINFO_ENV_VAR",
		"app" : AppMock(),
	}
	return config


def test_meminfo(config, mocks):
	"""
		Meminfo method returns correct values
	"""
	model = Memtester(config)
	meminfo = model.meminfo()
	assert "MemTotal" in meminfo
	assert "MemFree" in meminfo
	assert "MemAvailable" in meminfo
	assert meminfo["MemTotal"] == 32795928
	assert meminfo["MemFree"] == 26493224
	assert meminfo["MemAvailable"] == 28666952


def test_params(config, mocks):
	'''
		Input parameters must be validated
	'''
	model = Memtester(config)	

	# Bogus iterations argument
	result = model.start_test(memory="64M", iterations=-8, mask="12345")
	assert "error" in result

	result = model.start_test(memory="64M", iterations="xyz", mask="12345")
	assert "error" in result

	# Bogus memory argument
	result = model.start_test(memory="128dsa7", iterations=1, mask="12345")
	assert "error" in result

	# Called with valid arguments
	result = model.start_test(memory="128M", iterations=3, mask="12345")	
	mocks["start_test"].assert_called()
	args = mocks["start_test"].call_args.kwargs
	assert args["args"] == ["128M", "3"]
	assert args["env"]["MEMINFO_ENV_VAR"] == "12345"
