import pdb, pytest, math, pprint
from unittest.mock import MagicMock
from util.adm106x import ADM106x
from util.adm106x import TestRegisterManager as RegisterManager
from tests.mocks.adm1066 import model as adm1066_model_data
from controllers.adm106x import ADM106x_Controller



@pytest.fixture()
def device(mocker):
	i2c_bus = "/dev/i2c-0"
	i2c_addr = 0x34
	device = {}
	mocker.patch("util.adm106x.sleep")
	device["registers"] = RegisterManager(i2c_bus=i2c_bus, i2c_addr=i2c_addr)
	device["adm1066"] = ADM106x(i2c_bus=i2c_bus, i2c_addr=i2c_addr, reg_manager=device["registers"])	
	return device


def test_util_read_device(mocker, device):
	adm1066 = device["adm1066"]
	data = adm1066.read()
	# TODO: test with a real device, capture registers, add to the test


@pytest.fixture()
def config():
	return { 
		"mapping" : {
			"U68" : {
				"i2c_bus" : "/dev/i2c-3",
				"i2c_addr" : 0x34,
			},
		}
	}


# def test_render_template(mocker, config):
# 	adm_model = MagicMock()
# 	adm_model.read_all.configure_mock(return_value={ "U68" : adm1066_model_data})
# 	mocker.patch("controllers.adm106x.ADM106x", return_value=adm_model)
# 	adm_controller = ADM106x_Controller(config)
# 	result = adm_controller.read_all()

# 	with open("test_adm1066.html", "w") as file:
# 		file.write(result)