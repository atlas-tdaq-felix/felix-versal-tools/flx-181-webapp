import pdb, pytest
from unittest.mock import call
from models.ina226 import INA226
import math


@pytest.fixture()
def config():
	config = {
		"device_path" : "/sys/bus/iio/devices/",
		"name" : "ina226"
	}
	return config


@pytest.fixture()
def mocks(mocker):
	mocks = {}
	mocks["load_file"] = mocker.patch("models.ina226.load_file", return_value="")
	mocks["get_devices"] = mocker.patch("models.ina226.get_devices",
		return_value=["/some/path"])
	mocks["isdir"] = mocker.patch("models.ina226.isdir", return_value=True)
	return mocks


def test_read_all(config, mocks):
	data = {
		"in_voltage0_raw" : 126,
		"in_voltage0_scale" : 43.8,
		"in_voltage1_raw" : 213,
		"in_voltage1_scale" : 63.4,
		"in_power2_raw" : 733,
		"in_power2_scale" : 24.5,
		"in_current3_raw" : 121,
		"in_current3_scale" : 2.3,
		"in_shunt_resistor" : 123.45,
		"label" : "vcc"
	}

	def load_file(file_name):
		file_name = file_name.replace("/some/path/", "")
		return str(data[file_name])

	mocks["load_file"].configure_mock(side_effect=load_file)

	ina226 = INA226(config)
	result = ina226.read_all()

	assert math.isclose(result["vcc"]["V0"],
		data["in_voltage0_raw"] * data["in_voltage0_scale"], rel_tol=1e-4)
	assert math.isclose(result["vcc"]["V1"],
		data["in_voltage1_raw"] * data["in_voltage1_scale"], rel_tol=1e-4)
	assert math.isclose(result["vcc"]["P2"],
		data["in_power2_raw"] * data["in_power2_scale"], rel_tol=1e-4)
	assert math.isclose(result["vcc"]["I3"],
		data["in_current3_raw"] * data["in_current3_scale"], rel_tol=1e-4)
	assert result["vcc"]["R_shunt"], data["in_shunt_resistor"]


def test_io_error(config, mocks):
	mocks["load_file"].configure_mock(side_effect=IOError("File doesn't exist"))
	ina226 = INA226(config)
	result = ina226.read_all()
	assert result == {}