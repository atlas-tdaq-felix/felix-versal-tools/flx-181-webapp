import pdb, pytest
from unittest.mock import call
from models.ccf_clocks import CCF_Clocks
import math, json


clk_dump = """
{
  "si570_hsdp_clk": {
    "enable_count": 0,
    "prepare_count": 0,
    "protect_count": 0,
    "rate": 156249999,
    "min_rate": 0,
    "max_rate": 18446744073709552000,
    "accuracy": 0,
    "phase": 0,
    "duty_cycle": 50000
  },
  "si570_lpddr4_clk1": {
    "enable_count": 0,
    "prepare_count": 0,
    "protect_count": 0,
    "rate": 199999999,
    "min_rate": 0,
    "max_rate": 18446744073709552000,
    "accuracy": 0,
    "phase": 0,
    "duty_cycle": 50000
  },
  "si570_lpddr4_clk2": {
    "enable_count": 0,
    "prepare_count": 0,
    "protect_count": 0,
    "rate": 199999998,
    "min_rate": 0,
    "max_rate": 18446744073709552000,
    "accuracy": 0,
    "phase": 0,
    "duty_cycle": 50000
  }
}
"""

@pytest.fixture()
def config():
	config = {
		"whitelist" : [
			"si570_hsdp_clk",
			"si570_lpddr4_clk1",
			"si570_lpddr4_clk2",
			"missing_clock"
		],
		"clk_dump_path" : "/sys/kernel/debug/clk/clk_dump"
	}
	return config


@pytest.fixture()
def mocks(mocker):
	mocks = {}
	mocks["load_json"] = mocker.patch("models.ccf_clocks.load_json", return_value=json.loads(clk_dump))
	return mocks


def test_read_all(config, mocks):

	clocks = CCF_Clocks(config)
	result = clocks.read_all()

#	pdb.set_trace()

	assert result["si570_hsdp_clk"]["rate"] == 156249999
	assert result["si570_lpddr4_clk1"]["rate"] == 199999999
	assert result["si570_lpddr4_clk2"]["rate"] == 199999998
	assert math.isnan(result["missing_clock"]["rate"])
	assert result["missing_clock"]["error"]
