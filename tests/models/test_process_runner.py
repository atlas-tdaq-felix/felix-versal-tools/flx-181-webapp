import pdb, pytest
from unittest.mock import call, MagicMock
from models.process_runner import ProcessRunner
import math, threading
from threading import Event
from tests.mocks import AppMock


@pytest.fixture()
def config():
	config = {
		"binary" : ["/some/binary"],
		"name" : "test",
		"title" : "Test title",
		"app" : AppMock()
	}
	return config


class AliveMock():
	# Class for keeping track of thread status
	def __init__(self):
		self.alive = False

	def set(self, status):
		self.alive = status


class PipeProvider():	
	# Class to supply terminal output to the process_runner
	def __init__(self):
		self.eof = False
		self.active = False
		self.output = "Test terminal output"

	def read(self):
		if self.eof or not self.active:
			return None
		self.eof = True
		return self.output.encode('utf-8')


@pytest.fixture()
def mocks(mocker):	
	alive_mock = AliveMock()
	pipe_provider = PipeProvider()

	mocks = {}
	mocks["alive"] = alive_mock
	mocks["pipe"] = pipe_provider

	mocks["isfile"] = mocker.patch("models.process_runner.isfile", return_value=True)
	mocks["Thread"] = mocker.patch("models.process_runner.Thread",
		wraps=threading.Thread)		
	mocks["fcntl"] = mocker.patch("models.process_runner.fcntl")	
	mocks["thread"] = MagicMock()	
	mocks["thread"].is_alive.configure_mock(return_value=alive_mock.alive)
	mocks["thread"].start.configure_mock(side_effect=alive_mock.set(True))
	mocks["thread"].terminate.configure_mock(side_effect=alive_mock.set(False))
	mocks["process"] = MagicMock()
	mocks["process"].poll.configure_mock(return_value=None)
	mocks["process"].stdout.read.configure_mock(return_value=None)
	mocks["process"].returncode = 0
	mocks["process"].stdout.read.configure_mock(side_effect=pipe_provider.read)
	mocks["Popen"] = mocker.patch("models.process_runner.Popen",
		return_value=mocks["process"])
	return mocks


def test_start(config, mocks):
	'''
		Background thread is started when start_test() is called
	'''	
	mocks["Thread"].configure_mock(wraps=None, return_value=mocks["thread"])
	mocks["thread"].is_alive.configure_mock(return_value=False)

	runner = ProcessRunner(config)
	result = runner.start_test()

	assert "start_time" in result	
	mocks["Thread"].assert_called_with(target=runner.test_thread, daemon=True)	
	mocks["thread"].start.assert_called()


def test_stop(config, mocks):
	'''
		Background thread is terminated when stop_test() is called
	'''
	mocks["Thread"].configure_mock(wraps=None, return_value=mocks["thread"])
	mocks["thread"].is_alive.configure_mock(return_value=True)

	runner = ProcessRunner(config)
	runner.process = MagicMock()
	result = runner.stop_test()

	runner.process.terminate.assert_called()


def test_get_status(config, mocks):
	'''
		Status of process runner is correctly returned
		during different stages of running a process
	'''
	runner = ProcessRunner(config)
	runner.start_evt = Event()
	runner.stop_evt = Event()
	result = runner.get_status()

	# before 
	assert not result["running"]
	assert not result["output"]
	assert result["return_code"] is None
	assert result["start_time"] is None
	assert result["end_time"] is None

	# start a test process
	pipe_provider = mocks["pipe"]	
	mocks["process"].poll.configure_mock(return_value=None)
	runner.start_test(args=["arg1", "arg2", "arg3"])
	runner.start_evt.wait(timeout=2)
	result = runner.get_status()

	# while running a test	
	assert result["running"]
	assert result["output"] == [ "# /some/binary arg1 arg2 arg3" ]
	assert result["return_code"] is None
	assert not result["start_time"] is None
	assert result["end_time"] is None

	# make the test process exit
	mocks["process"].returncode = 123
	mocks["process"].poll.configure_mock(return_value=True)
	pipe_provider.active = True
	runner.stop_evt.wait(timeout=2)
	
	assert pipe_provider.eof
	result = runner.get_status()

	# after completing a test	
	assert not result["running"]
	assert result["output"] == [ "# /some/binary arg1 arg2 arg3", pipe_provider.output ]
	assert result["return_code"] == 123
	assert not result["start_time"] is None
	assert not result["end_time"] is None


def test_missing_binary(config, mocks):
	'''
		Error message is recorded when test binary can't be found
	'''
	runner = ProcessRunner(config)
	runner.start_evt = Event()
	runner.stop_evt = Event()
	mocks["isfile"].configure_mock(return_value=False)

	runner.start_test()
	runner.stop_evt.wait(timeout=2)
	result = runner.get_status()
	assert not result["running"]
	assert "Cannot find" in result["output"][0]
	assert "/some/binary" in result["output"][0]
	assert result["return_code"] is None
	assert not result["start_time"] is None
	assert result["end_time"] is None