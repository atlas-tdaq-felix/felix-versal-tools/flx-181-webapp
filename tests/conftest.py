import pytest, sys, os

from unittest.mock import MagicMock, patch

sys.modules["periphery.GPIO"] = MagicMock()
sys.modules["periphery.I2C"] = MagicMock()
sys.modules["periphery.MMIO"] = MagicMock()

from routes import create_app
from . import test_config_all as board_config


@pytest.fixture()
def client():
    config = {
        'TESTING' : True,
        'SECRET_KEY' : os.urandom(32),
        'WTF_CSRF_CHECK_DEFAULT' : True,
        'USER_BOARD' : board_config,
        'USER_SKIP_IO' : True,
    }

    app, api = create_app("app", config)

    # Create a test client using the Flask application configured for testing
    with app.test_client() as testing_client:
        # Establish an application context
        with app.app_context():
            yield testing_client  # this is where the testing happens!
