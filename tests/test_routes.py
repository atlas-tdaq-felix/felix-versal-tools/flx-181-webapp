import pytest, pdb, os
from . import test_config_all as config
from routes import create_app
from tests.mocks.i2c import I2C_Mock
from util.i2c import I2CError
from util.adm106x import TestRegisterManager as RegisterManager
from unittest.mock import MagicMock
from datetime import datetime


@pytest.fixture(scope="module", autouse=True)
def mocks():
    # i2c_mock = I2C_Mock(zsfp_clk)
    # mocker.patch("util.i2c.I2C", return_value=i2c_mock)
    pass


def test_home_page(client):
    response = client.get('/')
    assert response.status_code == 200
    assert str.encode(config["global"]["title"]) in response.data


def test_si570(client):
    response = client.get('/si570')
    assert response.status_code == 200


def test_ina226(client):
    response = client.get('/ina226')
    assert response.status_code == 200


def test_sysmon(client):
    response = client.get('/hwmon?class=xlnx,versal-sysmon')
    assert response.status_code == 200


def test_memtester_status(client):
    response = client.get('/memtester')
    assert response.status_code == 200


def test_memtester_start(client):
    response = client.post('/memtester/start',
        data={"memory" : "10M", "iterations" : "1"})
    assert response.status_code == 200


def test_memtester_stop(client):
    response = client.post('/memtester/stop')
    assert response.status_code == 302


def test_api_memtester_status(client):
    response = client.get('/api/memtester')
    assert response.status_code == 200


def test_api_memtester_start(client):
    response = client.post('/api/memtester/start',
        data={"memory" : "10M", "iterations" : "1"})
    assert response.status_code == 200


def test_api_memtester_stop(client):
    response = client.post('/api/memtester/stop')
    assert response.status_code == 200


def test_api_si570(client):
    response = client.get('/api/si570')
    assert response.status_code == 200


def test_api_ina226(client):
    response = client.get('/api/ina226')
    assert response.status_code == 200


# def test_api_sysmon(client):    
#     response = client.get('/api/hwmon?class=xlnx,versal-sysmon')
#     assert response.status_code == 200

def test_ccf_clocks(client):
    response = client.get('/ccf_clocks')
    assert response.status_code == 200


def test_adm106x_no_device(client):
    response = client.get('/adm106x')
    assert response.status_code == 200


def test_adm106x_fake_device(mocker):
    mocker.patch("util.adm106x.sleep")
    regs = RegisterManager(i2c_bus="/dev/i2c-0", i2c_addr=0x34)
    mocker.patch("util.adm106x.RegisterManager", return_value=regs)

    app_config = {
        'TESTING' : True,
        'SECRET_KEY' : os.urandom(32),
        'WTF_CSRF_CHECK_DEFAULT' : True,
        'USER_BOARD' : config,
        'USER_SKIP_IO' : True,
    }

    app, api = create_app(name="app", config=app_config)
    with app.test_client() as client:
        with app.app_context():
            response = client.get('/adm106x')
            assert response.status_code == 200


# application starts up, but the I2C devices fail to connect
# the application shouldn't lock up in this situation
def test_startup_serialized_i2c_fail(mocker):
    dt_max_seconds = 5
    msg = "I2C communication failed"
    mocker.patch("util.i2c.I2C.read", side_effect=I2CError(msg))
    mocker.patch("util.i2c.I2C.write", side_effect=I2CError(msg))
    mocker.patch("util.i2c.I2C.open", side_effect=I2CError(msg))
    mocker.patch("util.i2c.I2C.close", side_effect=I2CError(msg))

    start = datetime.now()    
    app_config = {
        'TESTING' : True,
        'SECRET_KEY' : os.urandom(32),
        'WTF_CSRF_CHECK_DEFAULT' : True,
        'USER_BOARD' : config,
        'USER_SKIP_IO' : False,
    }

    app, api = create_app(name="app", config=app_config)
    stop = datetime.now()
    assert (stop - start).seconds < dt_max_seconds, \
        f"Application bringup should take less than {dt_max_seconds} seconds"
