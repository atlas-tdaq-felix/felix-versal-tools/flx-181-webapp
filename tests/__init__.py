from pydoc import locate
from copy import deepcopy

test_config_all = deepcopy(locate("boards.vmk180.config"))

test_config_all["ccf_clocks"] = {
	"whitelist" : [
		"si570_sys_clk_40_for_SI5345A",
		"si570_sys_clk_40_for_SI5345B",
		"si570_sys_clk_100",
		"si570_lti_ref_clk",
		"si570_100G_ref_clk",
		"si570_ref_clk"
	],
	"clk_dump_path" : "/sys/kernel/debug/clk/clk_dump"
}

test_config_all["hwmon"] = {
	"device_classes" : {
		"xlnx,versal-sysmon" : {
			"title" : "SYSMON",
			"device_path" : "/sys/bus/iio/devices/",
			"patterns" : {
				"voltage" : "in_voltage(?P<id>\\d+)_(?P<name>.+)_input",
				"temperature" : "in_temp(?P<id>\\d+)_(?P<name>.+)_input",				
			}

		},
		"lltc,ltm4700" : {
			"title" : "LTM4700",
			"device_path" : "/sys/class/hwmon/",					
			"patterns" : {
				"voltage" : "out_voltage(?P<id>\\d+)_(?P<name>.+)",
			}
		},
	}		
}

test_config_all["adm106x"] = {
	"mapping" : {
		"U68" : {
			"i2c_bus" : "/dev/i2c-3",
			"i2c_addr" : 0x34,
		},
		"U69" : {
			"i2c_bus" : "/dev/i2c-3",
			"i2c_addr" : 0x35,
		},
	}
}