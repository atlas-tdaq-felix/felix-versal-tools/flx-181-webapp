from routes import create_app
"""
The tests verify that individual devices can be enabled and disabled
in configuration. The devices only show in the main menu when present
in the configuration file.
"""

# check that the devices can be enabled and disabled in configuration

def menu_helper(config, name):
    """
        Check that for the given configuration, the selected entry
        appears in the menu, while all others don't. Verify that the
        response code is 200.
    """
    menu = {
        "sysmon" : b"SYSMON",
        "ina226" : b"INA226",
        "memtester" : b"DRAM test",
        "iperf3" : b"Ethernet",
        "si570" : b"SI570",
        "gpio" : b"GPIO"
    }

    app_config = {
        'TESTING' : True,
        'WTF_CSRF_ENABLED' : False,
        'USER_BOARD' : config,
        'USER_SKIP_IO' : True,
    }

    app, api = create_app(name="app", config=app_config)
    with app.test_client() as client:
        response = client.get('/')
        assert response.status_code == 200
        for config, text in menu.items():
            if config == name:
                assert text in response.data
            else:
                assert text not in response.data


def test_menu_sysmon():
    """
    GIVEN a Flask application configured for testing AND SYSMON is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for SYSMON
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },
        
        "hwmon" : {
            "device_classes" : {
                "xlnx,versal-sysmon" : {
                    "device_path" : "/sys/bus/iio/devices/",
                    "title" : "SYSMON",
                    "patterns" : {
                        "voltage" : "in_voltage(?P<id>\\d+)_(?P<name>.+)_input",
                        "temperature" : "in_temp(?P<id>\\d+)_(?P<name>.+)_input",               
                    }

                },
                "lltc,ltm4700" : {
                    "device_path" : "/sys/bus/iio/devices/",
                    "title" : "LTM4700",                        
                    "patterns" : {
                        "voltage" : "out_voltage(?P<id>\\d+)_(?P<name>.+)",
                    }
                },
            },
        },

        "process_runners" : {}
    }

    menu_helper(config, "sysmon")    



def test_menu_ina226():
    """
    GIVEN a Flask application configured for testing AND ina226 is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for ina226
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },

        "ina226" : {
            "device_path" : "/sys/bus/iio/devices/",
            "name" : "ina226"
        },

        "process_runners" : {}
    }

    menu_helper(config, "ina226")


def test_menu_si570():
    """
    GIVEN a Flask application configured for testing AND SI570 is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for SI570
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },

        "si570" : {
            "mapping" : {}
        },

        "process_runners" : {}
    }

    menu_helper(config, "si570")


def test_menu_gpio():
    """
    GIVEN a Flask application configured for testing AND SI570 is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for GPIO
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },

        "gpio" : {
            "mapping" : {
                "gpiochip0" : {
                    "lines" : {}
                }
            }

        },

        "process_runners" : {}
    }

    menu_helper(config, "gpio")


def test_menu_memtester():
    """
    GIVEN a Flask application configured for testing AND SI570 is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for memtester
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },

        "process_runners" : {
            "memtester" : {
                "binary" : ["/usr/bin/memtester", "/usr/sbin/memtester"],
                "mask_variable" : "MEMTESTER_TEST_MASK",
                "mask_default" : "6144",
                "name" : "memtester",
                "title" : "DRAM test",
            },
        }
    }

    menu_helper(config, "memtester")


def test_menu_iperf3():
    """
    GIVEN a Flask application configured for testing AND SI570 is enabled
    WHEN the '/' page is requested (GET)
    THEN the response is valid
    THEN the menu contains the option for iperf3
    THEN no other devices show on the menu

    """
    config = {
        "global" : {
            "title" : "Test board demo"
        },

        "process_runners" : {
            "iperf3" : {
                "binary" : ["/usr/bin/iperf3"],
                "name" : "iperf3",
                "title" : "Ethernet",
            }
        }
    }

    menu_helper(config, "iperf3")