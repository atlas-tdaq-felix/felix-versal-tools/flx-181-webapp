__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "elena.zhivun@cern.ch"


# This frequency will be assumed for the reference clocks on IN1 of SI5345_A
# and SI5345_B in case if the alternative part is used that doesn't have I2C 
# communications
default_si570_frequency = 40.08

# Possible alternative frequencies for SI570 chips at IN1 of SI5345_A and SI5345_B
alt_si570_frequencies = [200.0]

# Change the settings below to match your platform

config = {
	"global" : {
		"title" : "FLX182 system monitor"
	},

	"board_id" : {
		"interface" : "zynq_mpsoc",
		"board" : "FLX-182",
		"revision" : "3_1b",
		"DNAaddress" : [0xF1250024, 0xF1250020],
		"self_test" : {
			"description" : "Identify the board",
		}
	},

	"self_test" : {
		"endpoint" : "https://bnl-board-database.web.cern.ch",
	},

	# fancy summary table for INA226
	# TODO: merge with HWMON
	"ina226" : {
		"device_path" : "/sys/bus/iio/devices/",
		"name" : "ina226",
		"self_test" : {
			"description" : "INA226 chip readout",
		},
		#tolerance and max current set by hand
		"reference" : {
			"voltage" : {
				"12P0V" :   {"nominal" : 12.00, "min" : 11.0,  "max" : 23.0,  "desc" : "+/- 1V expected from power supply"},
				"MGTAVCC" : {"nominal" : 0.88 , "min" : 0.854, "max" : 0.906, "desc" : "V limits from FPGA recommended ranges"},
				"MGTAVTT" : {"nominal" : 1.20 , "min" : 1.164, "max" : 1.236, "desc" : "V limits from FPGA recommended ranges"},
				"SYS12" :   {"nominal" : 1.20 , "min" : 1.164, "max" : 1.236, "desc" : "V limits from FPGA recommended ranges"},
				"SYS15" :   {"nominal" : 1.50 , "min" : 1.455, "max" : 1.545, "desc" : "V limits from FPGA recommended ranges"},
				"SYS18" :   {"nominal" : 1.80 , "min" : 1.710, "max" : 1.890, "desc" : "V limits from 25G FF"},
				"SYS25" :   {"nominal" : 2.50 , "min" : 2.15,  "max" : 2.85,  "desc" : ""},
				"SYS33" :   {"nominal" : 3.30 , "min" : 3.15,  "max" : 3.45,  "desc" : "V limits from 14/16G FF"},
				"SYS38" :   {"nominal" : 3.80 , "min" : 3.50,  "max" : 4.10,  "desc" : ""},
				"VCCINT" :  {"nominal" : 0.80 , "min" : 0.775, "max" : 0.825, "desc" : ""}
			},
			"current" : {
				"12P0V" :   {"nominal" : 1,     "max" : 8,    "desc" : ""},
				"MGTAVCC" : {"nominal" : 1.5 ,  "max" : 3.5,  "desc" : ""},
				"MGTAVTT" : {"nominal" : 3.4 ,  "max" : 6.5,  "desc" : ""},
				"SYS12" :   {"nominal" : 0.8 ,  "max" : 1.5,  "desc" : ""},
				"SYS15" :   {"nominal" : 0.9 ,  "max" : 1.5,  "desc" : ""},
				"SYS18" :   {"nominal" : 0.8 ,  "max" : 1.5,  "desc" : ""},
				"SYS25" :   {"nominal" : 0.3 ,  "max" : 0.5,  "desc" : ""},
				"SYS33" :   {"nominal" : 3.30 , "max" : 6.0,  "desc" : ""},
				"SYS38" :   {"nominal" : 0.0 ,  "max" : 5,    "desc" : ""},
				"VCCINT" :  {"nominal" : 20.0 , "max" : 50.0, "desc" : ""}
			}
		}
	},

	# crudely summarize IIO/HWMON devices with available driver
	# "name" in the regular expression determines the row title
	# TODO: this class should be handling INA226
	"hwmon" : {	
		"device_classes" : {
			"xlnx,versal-sysmon" : {
				"device_path" : "/sys/bus/iio/devices/",
				"title" : "SYSMON",
				"patterns" : {
					"voltage" : "in_voltage(?P<id>\\d+)_(?P<name>.+)_input",
					"temperature" : "in_temp(?P<id>\\d+)_(?P<name>.+)_input",
				},
				"reference" : {
					# Reference Versal Prime Series Data Sheet: DC and AC Switching Characteristics
					# DS956 2024-04-30
					# Table: Recommended Operating Conditions
					"temperature" : {
						"max_max" :         {"nominal" : 36, "min" : 0, "max" : 50, "source" : "N/A", "desc" : "FPGA diode temperature (latched max)"},
						"temp" : 		     {"nominal" : 36, "min" : 0, "max" : 50, "source" : "N/A", "desc" : "FPGA diode temperature"},
					},
					"voltage" : {
						"gty_avcc_103" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_104" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_105" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_106" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_200" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_201" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_202" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_203" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_204" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_205" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avcc_206" :    {"nominal" : 0.880, "min" : 0.854, "max" : 0.906, "source" : "DS956", "desc" : "GTY primary analog (PLL) power supply"},
						"gty_avccaux_103" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_104" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_105" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_106" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_200" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_201" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_202" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_203" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_204" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_205" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avccaux_206" : {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "GTY aux analog (PLL) power supply"},
						"gty_avtt_103" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_104" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_105" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_106" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_200" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_201" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_202" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_203" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_204" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_205" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
						"gty_avtt_206" : 	 {"nominal" : 1.200, "min" : 1.164, "max" : 1.236, "source" : "DS956", "desc" : "GTY termination power supply"},
#						"vcc_batt" : 		 {"nominal" : 0, "min" : 1.200, "max" : 1.500, "source" : "DS956", "desc" : "Battery power supply"},
						"vcc_pmc" :         {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "PMC primary power supply"},
						"vcc_psfp" :        {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "PS full-power domain power supply"},
						"vcc_pslp" :        {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "PS low-power domain power supply"},
						"vcc_ram" :         {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "PL RAM and clocking network" },
						"vcc_soc" :         {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "Network on Chip (NoC) and DDR memory controller power supply"},
						"vccaux" : 		 {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "Auxiliary power supply	"},
						"vccaux_pmc" : 	 {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "PMC auxiliary power supply voltage"},
						"vccaux_smon" : 	 {"nominal" : 1.500, "min" : 1.455, "max" : 1.545, "source" : "DS956", "desc" : "PMC system monitor power supply relative to GND_SMON"},
						"vccint" : 	     {"nominal" : 0.800, "min" : 0.775, "max" : 0.825, "source" : "DS956", "desc" : "PL primary power supply"},
						"vcco_306" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "HDIO bank 3 output driver"},
						"vcco_406" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "HDIO bank 4 output driver"},
						"vcco_500" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "PSIO bank 5 power supply"},
						"vcco_501" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "PSIO bank 5 power supply"},
						"vcco_502" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "PSIO bank 5 power supply"},
						"vcco_503" : 		 {"nominal" : 1.800, "min" : 1.710, "max" : 3.400, "source" : "DS956", "desc" : "PSIO bank 5 power supply"},
						"vcco_700" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_701" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_702" : 		 {"nominal" : 1.500, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_703" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_704" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_705" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_706" : 		 {"nominal" : 1.500, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_707" : 		 {"nominal" : 1.500, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_708" : 		 {"nominal" : 1.500, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_709" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_710" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"},
						"vcco_711" : 		 {"nominal" : 1.200, "min" : 0.950, "max" : 1.575, "source" : "DS956", "desc" : "XPIO bank 7 output driver"}
					}
				}
			},

			"ltm4700" : {
				"device_path" : "/sys/class/hwmon/",
				"title" : "LTM4700",						
				"patterns" : {
					"voltage" : "(?P<name>in\\d+_input)",
					"voltage_min" : "(?P<name>in\\d+_min)(?!_alarm)",
					"voltage_max" : "(?P<name>in\\d+_max)(?!_alarm)",
					"voltage_crit" : "(?P<name>in\\d+_crit)(?!_alarm)",
					"current" : "(?P<name>curr\\d+_input)",
					"current_max" : "(?P<name>curr\\d+_max)(?!_alarm)",
					"power" : "(?P<name>power\\d+_input)",
					"temp" : "(?P<name>temp\\d+_input)",
					"temp_max" : "(?P<name>temp\\d+_crit)(?!_alarm)",
				},
			}, #end of ltm4700

			"tmp435" : {
				"device_path" : "/sys/class/hwmon/",
				"title" : "TMP435",
				"patterns" : {
					"probe" : "(?P<name>temp\\d+_input)",
				},
				"self_test" : {
					"description" : "TMP435 readout",
				},
				"reference" : {
					"probe" : {
						"LTM4642_TMP" : {"min" : 20.0, "nominal" : 30.0, "max" : 50.0, "desc" : "LTM4642, 2.5V (DDR, ETH) and 3.8V (FF) rails"},
						"MGTAVCC_TMP" : {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 0.88V rail (FPGA) "},
						"MGTAVTT_TMP" : {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 1.2V rail (FPGA, DDR)"},
						"SYS12_TMP" :   {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 1.2V rail (FPGA, DDR)"},
						"SYS15_TMP" :   {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 1.5V rail (FPGA)"},
						"SYS18_TMP" :   {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 1.8V rail (FPGA, FF, Si5345)"},
						"SYS33_TMP" :   {"min" : 20.0, "nominal" : 40.0, "max" : 60.0, "desc" : "LTM4638, 3.3V rail (FF, Si5345, QSPI)"},
					}
				}
			},

			"jc42" : {
				"device_path" : "/sys/class/hwmon/",
				"title" : "JC-42.4",
				"patterns" : {
					"probe" : "(?P<name>temp\\d+_input)",
				},
				"reference" : {
					"probe" : {
						"DRAM temperature sensor" : {"min" : 20.0, "nominal" : 33.0, "max" : 50.0, "desc" : "DRAM temperature"},
					}
				}
			},
		},
		"self_test" : {
			"description" : "HWMON device readout (SYSMON, LTM4700, TMP435, etc)",
		}
	},

	# displays clocks listed in Linux device tree and
	# controlled by kernel via the Common Clock Framework
	# "ccf_clocks" : {
	# 	"whitelist" : [
	# 		"si570_sys_clk_40_for_SI5345A",
	# 		"si570_sys_clk_40_for_SI5345B",
	# 		"si570_sys_clk_100",
	# 		"si570_lti_ref_clk",
	# 		"si570_100G_ref_clk",
	# 		"si570_ref_clk"
	# 	],
	# 	"clk_dump_path" : "/sys/kernel/debug/clk/clk_dump"
	# },

	"adm1266" : { 
		"mapping" : {
			"U68" : {
				"i2c_bus" : "/dev/i2c-6",
				"i2c_addr" : 0x40,
				"rails" : ["12P0V", "NC", "NC", "NC",
					"VCCINT", "MGTAVCC", "MGTAVTT", "SYS12",
					"MGTYVCCAUX", "SYS15", "SYS18", "SYS25",
					"SYS33", "SYS38", "3V3_AUX", "VCC5V",
					"DDR4_B_VTT"
				],
			},
		},

		"self_test" : {
			"description" : "ADM1266 chip readout",
		}
	},

	"si5345" : {
		"mapping" : {
			"si5345_a" : {
				"comment" : "SI5345_A (U60)",
				"default_preset" : "Si5345-RevD-5345EVB2_si5345a_200_to_320-Registers.txt",
				"i2c_bus" : "/dev/i2c-11",
				"i2c_addr" : 0x68,
				# if the device is found at one of the alternative addresses, 
				# it's base I2C address will be updated to match the i2c_addr
				"i2c_addr_alt" : [0x7c],
				"f_xaxb_MHz"	: 48.0,
				"f_in0_MHz"		: 125.0,
				"f_in1_MHz"		: 40.08,
				"f_in2_MHz"		: 0.0,
				"f_in3_MHz"		: 0.0,
				"in_sel"		: 0b00, # device in_sel pins value
			},
			"si5345_b" : {
				"comment" : "SI5345_B (U59)",
				"default_preset" : "Si5345-RevD-5345EVB2_si5345a_200_to_320-Registers.txt",
				"i2c_bus" : "/dev/i2c-11",
				"i2c_addr" : 0x69,
				"i2c_addr_alt" : [0x7d],
				"f_xaxb_MHz"	: 48.0,
				"f_in0_MHz"		: 40.08,
				"f_in1_MHz"		: 40.08,
				"f_in2_MHz"		: 0.0,
				"f_in3_MHz"		: 0.0,
				"in_sel"		: 0b00, # device in_sel pins value
			},
		},
		"self_test" : {
			"description" : "SI5345 readout",
		}
	},

	# si569 might be replaced by Crystek CVPD-922
	"si570" : {
		# Valid stability values: 7ppm, 20ppm, 50ppm

		# Protected flag forbids updating the clock, for example
		# because the system will become unstable if it is touched

		# If system or memory clock are not marked as protected
		# the webapp will freeze the OS on startup since it 
		# resets the clocks to calculate fxtal

		"mapping" : {

			"ddr4_clk" : {
				"i2c_bus" : "/dev/i2c-6",
				"i2c_addr" : 0x60,
				"freq_MHz" : 200,
				"stability" : "20ppm",
				"protected" : True,
			},

			"sys_clk_100" : {
				"i2c_bus" : "/dev/i2c-7",
				"i2c_addr" : 0x55,
				"freq_MHz" : 100,
				"stability" : "20ppm",
				"protected" : False,
			},

			# NOTE: alternative part may be placed
			"SI5345A_ref_clk" : {
				"i2c_bus" : "/dev/i2c-7",
				"i2c_addr" : 0x60,
				"freq_MHz" : default_si570_frequency,
				"stability" : "20ppm",
				"protected" : False,
			},

			# NOTE: alternative part may be placed
			"SI5345B_ref_clk" : {
				"i2c_bus" : "/dev/i2c-8",
				"i2c_addr" : 0x60,
				"freq_MHz" : default_si570_frequency,
				"stability" : "20ppm",
				"protected" : False,
			},

			# No I2C access
#			"lti_ref_clk" : {
#				"i2c_bus" : "/dev/i2c-9",
#				"i2c_addr" : 0x60,
#				"freq_MHz" : 240.474,
#				"stability" : "20ppm",
#				"protected" : False,
#			},
#
#			"100G_ref_clk" : {
#				"i2c_bus" : "/dev/i2c-10",
#				"i2c_addr" : 0x60,
#				"freq_MHz" : 322.265625,
#				"stability" : "20ppm",
#				"protected" : False,
#			},

			"ps_ref_clk" : {
				"i2c_bus" : "/dev/i2c-11",
				"i2c_addr" : 0x5d,
				"freq_MHz" : 33.333333, # schematic says 30.3333333?
				"stability" : "20ppm",
				"protected" : True,
			},
		},
		"self_test" : {
			"description" : "SI570 device readout",
			"warn_on_error" : True,
		}
	},

	"sfp" : {
		"mapping" : {
			"FireFly_J36" : {
				"i2c_bus" : "/dev/i2c-8",
				"i2c_addr" : 0x50,
				"type" : "CERN-B-* Transmitter",
				"max_temp" : 75,
				# "select" : {
				# 	"gpio" : "gpiochip3",
				# 	"line" : 6,
				# 	"active_level" : 0
				# }
			},
			"FireFly_J35" : {
				"type" : "CERN-B-* Receiver",
				"i2c_bus" : "/dev/i2c-8",
				"i2c_addr" : 0x54,
				"max_temp" : 75,
				# "select" : {
				# 	"gpio" : "gpiochip3",
				# 	"line" : 2,
				# 	"active_level" : 0
				# }
			},
			"FireFly_J33" : {
				"type" : "CERN-B-* Transmitter",
				"i2c_bus" : "/dev/i2c-9",
				"i2c_addr" : 0x50,
				"max_temp" : 75,
				# "select" : {
				# 	"gpio" : "gpiochip3",
				# 	"line" : 14,
				# 	"active_level" : 0
				# }
			},
			"FireFly_J32" : {
				"type" : "CERN-B-* Receiver",
				"i2c_bus" : "/dev/i2c-9",
				"i2c_addr" : 0x54,
				"max_temp" : 75,
				# "select" : {
				# 	"gpio" : "gpiochip3",
				# 	"line" : 10,
				# 	"active_level" : 0
				# }
			},
			"FireFly_J34" : {
				"type" : "B04 Transceiver",
				"i2c_bus" : "/dev/i2c-10",
				"i2c_addr" : 0x50,
				"max_temp" : 75,
				# "select" : {
				# 	"gpio" : "gpiochip3",
				# 	"line" : 18,
				# 	"active_level" : 0
				# }
			},
		},
		"self_test" : {
			"description" : "SFP and FireFly module readout",
		}
	},

	"process_runners" : {
		"memtester" : {
			"binary" : ["/usr/bin/memtester", "/usr/sbin/memtester"],
			"mask_variable" : "MEMTESTER_TEST_MASK",
			"mask_default" : "6144",
			"name" : "memtester",
			"title" : "DRAM test",
			"self_test" : {
				"description" : "DRAM module test",
			}
		},

		"iperf3" : {
			"binary" : ["/usr/bin/iperf3"],
			"name" : "iperf3",
			"title" : "Ethernet",
			"self_test" : {
				"description" : "Ethernet test",
			}
		},

		"mtd" : {
			"binary" : ["<app_root_path>/executables/mtd_test.sh"],
			"name" : "mtd",
			"default_device" : 4,
			"title" : "QSPI flash",
			"self_test" : {
				"description" : "QSPI flash test",
			}
		}
	},

	"gpio" : {
		"self_test" : {
			"description" : "GPIO readout",
		},
		"mapping" : {
			"20100000000.gpio" : {
				"comment" : "Versal AXI GPIO",
				"lines" : {
					# GPIO_0 port in Vivado
					0 : {
						"label" : "LED_2",
						"direction" : "out",
						"value" : False
					},
					1 : {
						"label" : "i2c_sw_reset_b",
						"direction" : "out",
						"value" : True
					},
					2 : {
						"label" : "LED_1",
						"direction" : "out",
						"value" : False
					},
					3 : {
						"label" : "LED_0",
						"direction" : "out",
						"value" : False
					},
					4 : {
						"label" : "LED_3",
						"direction" : "out",
						"value" : False
					},
					5 : {
						"label" : "fan_fail_b",
						"direction" : "in"
					},
					6 : {
						"label" : "fan_fullsp",
						"direction" : "in"
					},
					7 : {
						"label" : "pcie_perst_b",
						"direction" : "in"
					},
					8 : {
						"label" : "ff3_prsnt_b",
						"direction" : "in"
					},
					9 : {
						"label" : "ioexpan1_intr_b",
						"direction" : "in",
					},
					10 : {
						"label" : "fan_overtemp_b",
						"direction" : "in"
					},
					11 : {
						"label" : "fan_pwm_out",
						"direction" : "in"
					},
					12 : {
						"label" : "ioexpand2_rst_b",
						"direction" : "out",
						"value" : True
					},
					13 : {
						"label" : "ioexpand2_intr_b",
						"direction" : "in"
					},
					14 : {
						"label" : "fan_tach",
						"direction" : "in"
					},
					15 : {
						"label" : "pcie_wake_b",
						"direction" : "out",
						"value" : True,
					},
					16 : {
						"label" : "ioexpand1_rst_b",
						"direction" : "out",
						"value" : True
					},

					# There is gpio-hog on these lines
					# 17 : {
					# 	"label" : "qspi1_rst_b (R433 DNP)",
					# 	"direction" : "out",
					# 	"value" : True
					# },
					# 18 : {
					# 	"label" : "qspi_rst_b",
					# 	"direction" : "out",
					# 	"value" : True
					# },
					# 19 : {
					# 	"label" : "qspi0_rst_b (R432 DNP)",
					# 	"direction" : "out",
					# 	"value" : True
					# },

					20 : {
						"label" : "ARM_CPU_controls_GPIO",
						"direction" : "out",
						"value" : False
					},

					# GPIO_1 port in Vivado
					21 : {
						"label" : "si5345a_sa1",
						"direction" : "out",
						"value" : False
					},
					22 : {
						"label" : "si5345b_rst_b",
						"direction" : "out",
						"value" : True
					},
					23 : {
						"label" : "si5345a_lol_b",
						"direction" : "in"
					},
					24 : {
						"label" : "si5345a_finc",
						"direction" : "out",
						"value" : False
					},
					25 : {
						"label" : "si5345b_sa0",
						"direction" : "out",
						"value" : True
					},
					26 : {
						"label" : "si5345a_fdec",
						"direction" : "out",
						"value" : False
					},
					27 : {
						"label" : "si5345b_fdec",
						"direction" : "out",
						"value" : False
					},
					28 : {
						"label" : "si5345b_lol_b",
						"direction" : "in"
					},
					29 : {
						"label" : "si5345a_intr_b",
						"direction" : "in"
					},
					30 : {
						"label" : "si5345a_output_enable_b",
						"direction" : "out",
						"value" : False
					},
					31 : {
						"label" : "si5345b_sa1",
						"direction" : "out",
						"value" : False
					},
					32 : {
						"label" : "si5345b_intr_b",
						"direction" : "in"
					},
					33 : {
						"label" : "si5345a_sa0",
						"direction" : "out",
						"value" : False
					},
					34 : {
						"label" : "si5345b_output_enable_b",
						"direction" : "out",
						"value" : False
					},
					35 : {
						"label" : "si5345b_insel0",
						"direction" : "out",
						"value" : False
					},
					36 : {
						"label" : "si5345b_insel1",
						"direction" : "out",
						"value" : False
					},
					37 : {
						"label" : "si5345a_insel0",
						"direction" : "out",
						"value" : False
					},
					38 : {
						"label" : "si5345a_rst_b",
						"direction" : "out",
						"value" : True
					},
					39 : {
						"label" : "si5345b_finc",
						"direction" : "out",
						"value" : False
					},
					40 : {
						"label" : "si5345a_insel1",
						"direction" : "out",
						"value" : False
					},
					41 : {
						"label" : "pcie_pwrbrk",
						"direction" : "in",
					},
				}
			},


			"pmc_gpio" : {
				"comment" : "Versal PMC Bank 1 GPIO",
				"lines" : {
					37 : {
						"label" : "ADM1266 GPIO1 (U33)",
						"direction" : "out",
						"value" : True,
					},
					38 : {
						"label" : "PCIE_PERST_B (R396 DNP)",
						"direction" : "in",
					},
					39 : {
						"label" : "PCIE_PERST_B (R398 DNP)",
						"direction" : "in"
					},
				}
			},


			"11-0022" : {
				"comment" : "I2C GPIO expander TCA6424A (U80)",
				"lines" : {
					0 : {
						"label" : "alert_sys12",
						"direction" : "in"
					},
					1 : {
						"label" : "alert_vcc_int",
						"direction" : "in"
					},
					2 : {
						"label" : "sys12_temp_alert_b",
						"direction" : "in"
					},
					3 : {
						"label" : "sys12_temp_err_b",
						"direction" : "in"
					},
					4 : {
						"label" : "sys15_temp_alert_b",
						"direction" : "in"
					},
					5 : {
						"label" : "alert_sys15",
						"direction" : "in"
					},
					6 : {
						"label" : "alert_sys33",
						"direction" : "in"
					},
					7 : {
						"label" : "alert_sys38",
						"direction" : "in"
					},
					8 : {
						"label" : "sys33_temp_alert_b",
						"direction" : "in"
					},
					9 : {
						"label" : "sys33_temp_err_b",
						"direction" : "in",
					},
					10 : {
						"label" : "ltm4700_alert_b",
						"direction" : "in"
					},
					11 : {
						"label" : "vcc_int_fault",
						"direction" : "in"
					},
					12 : {
						"label" : "sys18_temp_err_b",
						"direction" : "in"
					},
					13 : {
						"label" : "alert_sys18",
						"direction" : "in"
					},
					14 : {
						"label" : "sys18_temp_alert_b",
						"direction" : "in"
					},
					15 : {
						"label" : "alert_sys25",
						"direction" : "in"
					},
					16 : {
						"label" : "alert_mgtavcc",
						"direction" : "in"
					},
					17 : {
						"label" : "mgtavcc_temp_alert_b",
						"direction" : "in"
					},
					18 : {
						"label" : "mgtavcc_temp_err_b",
						"direction" : "in"
					},
					19 : {
						"label" : "alert_12p0v",
						"direction" : "in"
					},
					20 : {
						"label" : "mgtavtt_temp_err_b",
						"direction" : "in"
					},
					21 : {
						"label" : "mgtavtt_temp_alert_b",
						"direction" : "in"
					},
					22 : {
						"label" : "alert_mgtavtt",
						"direction" : "in"
					},
					23 : {
						"label" : "sys15_tmp_err_b",
						"direction" : "in"
					},
				}
			},


			"11-0023" : {
				"comment" : "I2C GPIO expander TCA6424A (U81)",
				"lines" : {
					0 : {
						"label" : "ff1_rx_rst_b",
						"direction" : "out",
						"value" : True
					},
					1 : {
						"label" : "ff1_rx_intr_b",
						"direction" : "in"
					},
					2 : {
						# Mark as "in" if controlled by the SFP model
						"label" : "ff1_rx_select_b",
						"direction" : "out",
						"value" : False
					},
					3 : {
						"label" : "ff1_rx_prsnt_b",
						"direction" : "in"
					},

					4 : {
						"label" : "ff1_tx_rst_b",
						"direction" : "out",
						"value" : True
					},
					5 : {
						"label" : "ff1_tx_intr_b",
						"direction" : "in"
					},
					6 : {
						# Mark as "in" if controlled by the SFP model
						"label" : "ff1_tx_select_b",
						"direction" : "out",
						"value" : False
					},
					7 : {
						"label" : "ff1_tx_prsnt_b",
						"direction" : "in"
					},
					8 : {
						"label" : "ff2_rx_rst_b",
						"direction" : "out",
						"value" : True
					},
					9 : {
						"label" : "ff2_rx_intr_b",
						"direction" : "in",
					},
					10 : {
						# Mark as "in" if controlled by the SFP model
						"label" : "ff2_rx_select_b",
						"direction" : "out",
						"value" : False
					},
					11 : {
						"label" : "ff2_rx_prsnt_b",
						"direction" : "in"
					},
					12 : {
						"label" : "ff2_tx_rst_b",
						"direction" : "out",
						"value" : True
					},
					13 : {
						"label" : "ff2_tx_intr_b",
						"direction" : "in"
					},
					14 : {
						# Mark as "in" if controlled by the SFP model
						"label" : "ff2_tx_select_b",
						"direction" : "out",
						"value" : False
					},
					15 : {
						"label" : "ff2_tx_prsnt_b",
						"direction" : "in"
					},
					16 : {
						"label" : "ff3_rst_b",
						"direction" : "out",
						"value" : True
					},
					17 : {
						"label" : "ff3_intr_b",
						"direction" : "in"
					},
					18 : {
						# Mark as "in" if controlled by the SFP model
						"label" : "ff3_select_b",
						"direction" : "out",
						"value" : False
					},
					19 : {
						"label" : "ltm4642_temp_alt_b",
						"direction" : "in"
					},
					20 : {
						"label" : "ltm4642_temp_err_b",
						"direction" : "in"
					},
					21 : {
						"label" : "versal_cfg_done_gpio",
						"direction" : "in"
					},
					22 : {
						"label" : "i2c_gpio_por",
						"direction" : "in"
					},
					23 : {
						"label" : "versal_error_out_gpio",
						"direction" : "in"
					},
				}
			},
		},

	},


	"chipscopy" : {
		"cs_server_url" : "TCP:192.168.0.18:3042",
		"hw_server_url" : "TCP:192.168.0.18:3121",
		"device_family" : "versal",
		"bypass_version_check" : False,
	},

	"chipscopy-ddrmc" : {
		"self_test" : {
			"description" : "Chipscopy-based DDRMC test",
		},
	},

	"chipscopy-ibert" : {		
		# Allow scheduling scans in entire quad at once. This may or may not work well
		# and may or may not be faster than doing the scans one by one

		# True = schedule scans in the entire quad at once 
		# False = schedule scans one by one (use that if Eye Scan page freezes)
		'scan_by_quad' : False,
        
		# how long before the scheduler gives up on the scan
        'scan_timeout_seconds' : 300,

		"self_test" : {
			"description" : "Chipscopy-based IBERT test and eye scans",
		},
        
		# please list the links where RX is from the same quad contiguously
		# one after another so that scans can be scheduled in batches
		"gt_links" : {
			"PCIE_0" : {
				"rx" : { "quad" : "Quad_103", "channel" : 0},
				"tx" : { "quad" : "Quad_103", "channel" : 0},
			},
			"PCIE_1" : {
				"rx" : { "quad" : "Quad_103", "channel" : 1},
				"tx" : { "quad" : "Quad_103", "channel" : 1},
			},
			"PCIE_2" : {
				"rx" : { "quad" : "Quad_103", "channel" : 2},
				"tx" : { "quad" : "Quad_103", "channel" : 2},
			},
			"PCIE_3" : {
				"rx" : { "quad" : "Quad_103", "channel" : 3},
				"tx" : { "quad" : "Quad_103", "channel" : 3},
			},
			"PCIE_4" : {
				"rx" : { "quad" : "Quad_104", "channel" : 0},
				"tx" : { "quad" : "Quad_104", "channel" : 0},
			},
			"PCIE_5" : {
				"rx" : { "quad" : "Quad_104", "channel" : 1},
				"tx" : { "quad" : "Quad_104", "channel" : 1},
			},
			"PCIE_6" : {
				"rx" : { "quad" : "Quad_104", "channel" : 2},
				"tx" : { "quad" : "Quad_104", "channel" : 2},
			},
			"PCIE_7" : {
				"rx" : { "quad" : "Quad_104", "channel" : 3},
				"tx" : { "quad" : "Quad_104", "channel" : 3},
			},
			"PCIE_8" : {
				"rx" : { "quad" : "Quad_105", "channel" : 0},
				"tx" : { "quad" : "Quad_105", "channel" : 0},
			},
			"PCIE_9" : {
				"rx" : { "quad" : "Quad_105", "channel" : 1},
				"tx" : { "quad" : "Quad_105", "channel" : 1},
			},
			"PCIE_10" : {
				"rx" : { "quad" : "Quad_105", "channel" : 2},
				"tx" : { "quad" : "Quad_105", "channel" : 2},
			},
			"PCIE_11" : {
				"rx" : { "quad" : "Quad_105", "channel" : 3},
				"tx" : { "quad" : "Quad_105", "channel" : 3},
			},
			"PCIE_12" : {
				"rx" : { "quad" : "Quad_106", "channel" : 0},
				"tx" : { "quad" : "Quad_106", "channel" : 0},
			},
			"PCIE_13" : {
				"rx" : { "quad" : "Quad_106", "channel" : 1},
				"tx" : { "quad" : "Quad_106", "channel" : 1},
			},
			"PCIE_14" : {
				"rx" : { "quad" : "Quad_106", "channel" : 2},
				"tx" : { "quad" : "Quad_106", "channel" : 2},
			},
			"PCIE_15" : {
				"rx" : { "quad" : "Quad_106", "channel" : 3},
				"tx" : { "quad" : "Quad_106", "channel" : 3},
			},
			"FF3_0" : {
				"rx" : { "quad" : "Quad_200", "channel" : 0},
				"tx" : { "quad" : "Quad_200", "channel" : 0},
			},
			"FF3_1" : {
				"rx" : { "quad" : "Quad_200", "channel" : 1},
				"tx" : { "quad" : "Quad_200", "channel" : 1},
			},
			"FF3_2" : {
				"rx" : { "quad" : "Quad_200", "channel" : 2},
				"tx" : { "quad" : "Quad_200", "channel" : 2},
			},
			"FF3_3" : {
				"rx" : { "quad" : "Quad_200", "channel" : 3},
				"tx" : { "quad" : "Quad_200", "channel" : 3},
			},
			"FF1_0" : {
				"rx" : { "quad" : "Quad_201", "channel" : 0},
				"tx" : { "quad" : "Quad_201", "channel" : 0},
			},
			"FF1_1" : {
				"rx" : { "quad" : "Quad_201", "channel" : 1},
				"tx" : { "quad" : "Quad_201", "channel" : 1},
			},
			"FF1_2" : {
				"rx" : { "quad" : "Quad_201", "channel" : 2},
				"tx" : { "quad" : "Quad_201", "channel" : 2},
			},
			"FF1_3" : {
				"rx" : { "quad" : "Quad_201", "channel" : 3},
				"tx" : { "quad" : "Quad_201", "channel" : 3},
			},
			"FF1_4" : {
				"rx" : { "quad" : "Quad_202", "channel" : 0},
				"tx" : { "quad" : "Quad_202", "channel" : 0},
			},
			"FF1_5" : {
				"rx" : { "quad" : "Quad_202", "channel" : 1},
				"tx" : { "quad" : "Quad_202", "channel" : 1},
			},
			"FF1_6" : {
				"rx" : { "quad" : "Quad_202", "channel" : 2},
				"tx" : { "quad" : "Quad_202", "channel" : 2},
			},
			"FF1_7" : {
				"rx" : { "quad" : "Quad_202", "channel" : 3},
				"tx" : { "quad" : "Quad_202", "channel" : 3},
			},
			"FF1_8" : {
				"rx" : { "quad" : "Quad_203", "channel" : 0},
				"tx" : { "quad" : "Quad_203", "channel" : 0},
			},
			"FF1_9" : {
				"rx" : { "quad" : "Quad_203", "channel" : 1},
				"tx" : { "quad" : "Quad_203", "channel" : 1},
			},
			"FF1_10" : {
				"rx" : { "quad" : "Quad_203", "channel" : 2},
				"tx" : { "quad" : "Quad_203", "channel" : 2},
			},
			"FF1_11" : {
				"rx" : { "quad" : "Quad_203", "channel" : 3},
				"tx" : { "quad" : "Quad_203", "channel" : 3},
			},
			"FF2_0" : {
				"rx" : { "quad" : "Quad_204", "channel" : 0},
				"tx" : { "quad" : "Quad_204", "channel" : 0},
			},
			"FF2_1" : {
				"rx" : { "quad" : "Quad_204", "channel" : 1},
				"tx" : { "quad" : "Quad_204", "channel" : 1},
			},
			"FF2_2" : {
				"rx" : { "quad" : "Quad_204", "channel" : 2},
				"tx" : { "quad" : "Quad_204", "channel" : 2},
			},
			"FF2_3" : {
				"rx" : { "quad" : "Quad_204", "channel" : 3},
				"tx" : { "quad" : "Quad_204", "channel" : 3},
			},
			"FF2_4" : {
				"rx" : { "quad" : "Quad_205", "channel" : 0},
				"tx" : { "quad" : "Quad_205", "channel" : 0},
			},
			"FF2_5" : {
				"rx" : { "quad" : "Quad_205", "channel" : 1},
				"tx" : { "quad" : "Quad_205", "channel" : 1},
			},
			"FF2_6" : {
				"rx" : { "quad" : "Quad_205", "channel" : 2},
				"tx" : { "quad" : "Quad_205", "channel" : 2},
			},
			"FF2_7" : {
				"rx" : { "quad" : "Quad_205", "channel" : 3},
				"tx" : { "quad" : "Quad_205", "channel" : 3},
			},
			"FF2_8" : {
				"rx" : { "quad" : "Quad_206", "channel" : 0},
				"tx" : { "quad" : "Quad_206", "channel" : 0},
			},
			"FF2_9" : {
				"rx" : { "quad" : "Quad_206", "channel" : 1},
				"tx" : { "quad" : "Quad_206", "channel" : 1},
			},
			"FF2_10" : {
				"rx" : { "quad" : "Quad_206", "channel" : 2},
				"tx" : { "quad" : "Quad_206", "channel" : 2},
			},
			"FF2_11" : {
				"rx" : { "quad" : "Quad_206", "channel" : 3},
				"tx" : { "quad" : "Quad_206", "channel" : 3},
			},
		},

		"apply_default_link_config_on_start_up" : False,
		
		"default_link_config" : {
			"tx" : {
				"Pattern"	: "PRBS 31",
				"Differential Control" :  "804 mV",
			},
			"rx" : {
				"Loopback"	: "Near-End PMA",
				"Pattern"	: "PRBS 31",
				"Termination Voltage" : "800mv",
			}
		},


		"scans" : {
            
			"pcie_pma_near_loopback" : {
				"comment" : "PCIe Near-End PMA Loopback",
				"links" : [
					"PCIE_0", "PCIE_1", "PCIE_2", "PCIE_3", "PCIE_4",  "PCIE_5", "PCIE_6", "PCIE_7",
					"PCIE_8", "PCIE_9", "PCIE_10", "PCIE_11", "PCIE_12", "PCIE_13", "PCIE_14", "PCIE_15", 
				],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},
				
				"tx" : {
					"Pattern"	: "PRBS 31",
					"Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "Near-End PMA",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",               
				}
			},
			
			"pcie_fiber_loopback" : {
				"comment" : "PCIe Physical Loopback",
				"links" : [
					"PCIE_0", "PCIE_1", "PCIE_2", "PCIE_3", "PCIE_4",  "PCIE_5", "PCIE_6", "PCIE_7",
					"PCIE_8", "PCIE_9", "PCIE_10", "PCIE_11", "PCIE_12", "PCIE_13", "PCIE_14", "PCIE_15", 
				],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},				

				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "User Design",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},


			"ff1_pma_near_loopback" : {
				"comment" : "FireFly 1 (J32, J33) Near-End PMA Loopback",
				"links" : [ "FF1_0", "FF1_1", "FF1_2", "FF1_3", "FF1_4",  "FF1_5", "FF1_6", "FF1_7",
					"FF1_8", "FF1_9", "FF1_10", "FF1_11", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},
				
				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "Near-End PMA",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},
			
			"ff1_fiber_loopback" : {
				"comment" : "FireFly 1 (J32, J33) Physical Loopback",
				"links" : [ "FF1_0", "FF1_1", "FF1_2", "FF1_3", "FF1_4",  "FF1_5", "FF1_6", "FF1_7",
					"FF1_8", "FF1_9", "FF1_10", "FF1_11", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},				

				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "User Design",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},

			"ff2_pma_near_loopback" : {
				"comment" : "FireFly 2 (J32, J33) Near-End PMA Loopback",
				"links" : [ "FF2_0", "FF2_1", "FF2_2", "FF2_3", "FF2_4",  "FF2_5", "FF2_6", "FF2_7",
					"FF2_8", "FF2_9", "FF2_10", "FF2_11", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},
				
				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "Near-End PMA",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},
			
			"ff2_fiber_loopback" : {
				"comment" : "FireFly 2 (J32, J33) Physical Loopback",
				"links" : [ "FF2_0", "FF2_1", "FF2_2", "FF2_3", "FF2_4",  "FF2_5", "FF2_6", "FF2_7",
					"FF2_8", "FF2_9", "FF2_10", "FF2_11", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},				

				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "User Design",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},

			"ff3_pma_near_loopback" : {
				"comment" : "FireFly 3 (J34) Near-End PMA Loopback",
				"links" : [ "FF3_0", "FF3_1", "FF3_2", "FF3_3", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},
				
				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "Near-End PMA",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},
			
			"ff3_fiber_loopback" : {
				"comment" : "FireFly 3 (J34) Physical Loopback",
				"links" : [ "FF3_0", "FF3_1", "FF3_2", "FF3_3", ],

				"scan" : {
					"Dwell BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},				

				"tx" : {
					"Pattern"	: "PRBS 31",
                    "Differential Control" :  "804 mV",
				},

				"rx" : {
					"Loopback"	: "User Design",
					"Pattern"	: "PRBS 31",
                    "Termination Voltage" : "500mv",
				}
			},

		},
	},

}


# check that the I2C bus looks more or less as expected
# to protect the devices from being overwritten
# import re
# import subprocess

# output = subprocess.check_output(["i2cdetect", "-l"]).decode('utf-8')
# pattern = r"i2c-3\s+i2c\s+xiic-i2c\s+[0-9a-fA-F]+\.i2c\s+I2C adapter"

# if not re.search(pattern, output):
# 	print(output)
# 	raise RuntimeError("Expected xiic-i2c driver on i2c-3 bus! Aborting the self-test!")


# check the SI570 frequency and update the configuration 
# when alternative parts are used

from util.si570 import read_default_frequency
from util.i2c import I2CError
import math, pdb


def check_and_redefine_clock(ref_clk_name, target_clk_name, alt_frequency, tolerance=5e-2):	
	try:
		ref_clk = config['si570']['mapping'][ref_clk_name]		
		f_actual = read_default_frequency(ref_clk)		
		if math.isclose(f_actual, alt_frequency, rel_tol=tolerance):
			print(f"{ref_clk_name} is {alt_frequency} MHz instead of {default_si570_frequency} MHz!")
			config['si5345']["mapping"][target_clk_name]["f_in1_MHz"] = alt_frequency
			ref_clk["freq_MHz"] = alt_frequency
	except I2CError:
		pass


for alt_frequency in alt_si570_frequencies:
	check_and_redefine_clock(ref_clk_name="SI5345A_ref_clk", target_clk_name='si5345_a',
		alt_frequency=alt_frequency)
	check_and_redefine_clock(ref_clk_name="SI5345B_ref_clk", target_clk_name='si5345_b',
		alt_frequency=alt_frequency)




