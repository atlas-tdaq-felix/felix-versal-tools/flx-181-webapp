__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "elena.zhivun@cern.ch"


# Change the settings below to match your platform

config = {
	"global" : {
		"title" : "VMK180 test demo"
	},

	"ina226" : {
		"device_path" : "/sys/bus/iio/devices/",
		"name" : "ina226"

		# INA226 are automatically detected when using IIO driver.
		# Device tree entry should contain label and shunt resistor value:
		#
		# ina226@47 {
		#    #io-channel-cells = <0x01>;
		#    label = "ina226-vcc-3v3";
		#    compatible = "ti,ina226";
		#    reg = <0x47>;
		#    shunt-resistor = <0x1388>; # [uOhms]
		#};
		#
	},

	"hwmon" : {		
		"device_classes" : {
			"xlnx,versal-sysmon" : {
				"title" : "SYSMON",
				"device_path" : "/sys/bus/iio/devices/",
				"patterns" : {
					"voltage" : "in_voltage(?P<id>\\d+)_(?P<name>.+)_input",
					"temperature" : "in_temp(?P<id>\\d+)_(?P<name>.+)_input",				
				}

			},
		}		
	},


	"si570" : {
		# Valid stability values: 7ppm, 20ppm, 50ppm

		# Protected flag forbids updating the clock, for example
		# because the system will become unstable if it is touched

		# If system or memory clock are not marked as protected
		# the webapp will freeze the OS on startup since it 
		# resets the clocks to calculate fxtal

		"mapping" : {
			"zsfp_clk" : {
				"i2c_bus" : "/dev/i2c-7",
				"i2c_addr" : 0x5d,
				"freq_MHz" : 156.25,
				"stability" : "20ppm",
				"protected" : False,
			},

			"user_clk" : {
				"i2c_bus" : "/dev/i2c-8",
				"i2c_addr" : 0x5f,
				"freq_MHz" : 100,
				"stability" : "20ppm",
				"protected" : False,
			},

			"ref_clk" : {
				"i2c_bus" : "/dev/i2c-10",
				"i2c_addr" : 0x5d,
				"freq_MHz" : 33.333333,
				"stability" : "20ppm",
				"protected" : True,
			},

			"hsdp_clk" : {
				"i2c_bus" : "/dev/i2c-16",
				"i2c_addr" : 0x5d,
				"freq_MHz" : 156.25,
				"stability" : "20ppm",
				"protected" : False,
			},

			"lpddr_clk1" : {
				"i2c_bus" : "/dev/i2c-15",
				"i2c_addr" : 0x60,
				"freq_MHz" : 200,
				"stability" : "20ppm",
				"protected" : False,
			},

			"lpddr_clk2" : {
				"i2c_bus" : "/dev/i2c-14",
				"i2c_addr" : 0x60,
				"freq_MHz" : 200,
				"stability" : "20ppm",
				"protected" : False,
			},

			# Reading any I2C register from ddrdimm1_clk, causes garbage data
			# to arrive from  all I2C connected to I2C MUX TCA9548 addr=0x75

			# This issue is not specific to this webapp, but occurs also when
			# an I2C register is read with any other command, e.g. 'i2cdump -y 13 0x60'
			#
			# The suspected reason is interference between I2C data communication
			# and generated DRAM clock
			#
			# The data be reverted back to normal by running 'i2cdump -y 17 command'

			# "ddrdimm1_clk" : {
			# 	"i2c_bus" : "/dev/i2c-13",
			# 	"i2c_addr" : 0x60,
			# 	"freq_MHz" : 200,
			# 	"stability" : "20ppm",
			# 	"protected" : True,
			# },
		}
	},

	"sfp" : {
		"mapping" : {
			"SFP0" : {
				"type" : "sfp",
				"i2c_bus" : "/dev/i2c-18",
				"i2c_addr" : 0x50,
			},

			"SFP1" : {
				"type" : "sfp",
				"i2c_bus" : "/dev/i2c-19",
				"i2c_addr" : 0x50,
			},

			"QSFP0" : {
				"type" : "qsfp",
				"i2c_bus" : "/dev/i2c-20",
				"i2c_addr" : 0x50,
			},
		}
	},

	"process_runners" : {
		"memtester" : {
			"binary" : ["/usr/bin/memtester", "/usr/sbin/memtester"],
			"mask_variable" : "MEMTESTER_TEST_MASK",
			"mask_default" : "6144",
			"name" : "memtester",
			"title" : "DRAM test",
		},

		"iperf3" : {
			"binary" : ["/usr/bin/iperf3"],
			"name" : "iperf3",
			"title" : "Ethernet",
		},

		"mtd" : {
			"binary" : ["<app_root_path>/executables/mtd_test.sh"],
			"name" : "mtd",
			"default_device" : 4,
			"title" : "QSPI flash",
		}
	},

	"gpio" : {
		"mapping" : {
			"gpiochip0" : {
				"comment" : "AXI GPIO0",
				"lines" : {
					0 : {
						"label" : "DIP_SW_0 (SW6)",
						"direction" : "in"
					},
					1 : {
						"label" : "DIP_SW_1 (SW6)",
						"direction" : "in"
					},
					2 : {
						"label" : "DIP_SW_2 (SW6)",
						"direction" : "in"
					},
					3 : {
						"label" : "DIP_SW_3 (SW6)",
						"direction" : "in"
					},
					4 : {
						"label" : "PUSH_B0 (SW4)",
						"direction" : "in"
					},
					5 : {
						"label" : "PUSH_B1 (SW5)",
						"direction" : "in"
					},
					6 : {
						"label" : "LED_0 (DS6)",
						"direction" : "out",
						"value" : False
					},
					7 : {
						"label" : "LED_1 (DS5)",
						"direction" : "out",
						"value" : False
					},
					8 : {
						"label" : "LED_2 (DS4)",
						"direction" : "out",
						"value" : False
					},
					9 : {
						"label" : "LED_3 (DS3)",
						"direction" : "out",
						"value" : False
					},
					10 : {
						"label" : "QSFP_MODSELL",
						"direction" : "out",
						"value" : False
					},
					11 : {
						"label" : "QSFP_RESETL",
						"direction" : "out",
						"value" : True
					},
					12 : {
						"label" : "QSFP_MODPRSL",
						"direction" : "in",
					},
					13 : {
						"label" : "QSFP_INTL",
						"direction" : "in",
					},
					14 : {
						"label" : "QSFP_LPMODE",
						"direction" : "out",
						"value" : False
					}
				}
			},
			
			"gpiochip2" : {
				"comment" : "I2C GPIO expander TCA6416A (U233)",
				"lines" : {
					0 : {
						"label" : "MAX6643_OT_B",
						"direction" : "in"
					},
					1 : {
						"label" : "MAX6643_FAINFAIL_B",
						"direction" : "in"
					},
					4 : {
						"label" : "PMBUS2_INA226_ALERT",
						"direction" : "in"
					},
					7 : {
						"label" : "MAX6643_FULLSPD",
						"direction" : "in"
					},

					8 : {
						"label" : "FMCP1_FMC_PRSNT_M2C_B",
						"direction" : "in"
					},
					9 : {
						"label" : "FMCP2_FMC_PRSNT_M2C_B",
						"direction" : "in"
					},
					10 : {
						"label" : "FMCP1_FMCP_PRSNT_M2C_B",
						"direction" : "in"
					},
					11 : {
						"label" : "FMCP2_FMCP_PRSNT_M2C_B",
						"direction" : "in"
					},
					12 : {
						"label" : "VCCINT_VRHOT_B",
						"direction" : "in"
					},
					13 : {
						"label" : "8A34001_EXP_RST_B",
						"direction" : "out",
						"value" : True
					},
					14 : {
						"label" : "PMBUS_ALERT",
						"direction" : "in"
					},
					15 : {
						"label" : "PMBUS1_INA226_ALERT",
						"direction" : "in"
					},
				}
			}
		}
	},


	"chipscopy" : {
		"cs_server_url" : "TCP:192.168.1.100:3042",
		"hw_server_url" : "TCP:192.168.1.100:3121",
		"device_family" : "versal",
		"bypass_version_check" : False,
	},

	"chipscopy-ddrmc" : {},

	"chipscopy-ibert" : {
		"gt_links" : {
			"SFP0" : {
				"rx" : { "quad" : "Quad_105", "channel" : 2},
				"tx" : { "quad" : "Quad_105", "channel" : 2},
			},
			"SFP1" : {
				"rx" : { "quad" : "Quad_105", "channel" : 3},
				"tx" : { "quad" : "Quad_105", "channel" : 3},
			},
		},

		"apply_default_link_config_on_start_up" : True,
		
		"default_link_config" : {
			"tx" : {
				"Pattern"	: "PRBS 31",
				"Differential Control" :  "852 mV",
			},
			"rx" : {
				"Loopback"	: "Near-End PMA",
				"Pattern"	: "PRBS 31",
				"Termination Voltage" : "800mv",
			}
		},


		# Allow scheduling scans in entire quad at once. This may or may not work well
		# and may or may not be faster than doing the scans one by one

		# True = schedule scans in the entire quad at once 
		# False = schedule scans one by one (use that if Eye Scan page freezes)
		'scan_by_quad' : True,
        
		# how long before the scheduler gives up on the scan
        'scan_timeout_seconds' : 300,
        
		# please list the links where RX is from the same quad contiguously
		# one after another so that scans can be scheduled in batches

		"scans" : {
			"pma_near_loopback" : {
				"comment" : "Near-End PMA Loopback",
				"links" : ["SFP0", "SFP1"],

				"scan" : {
					"Target BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},
				
				"tx" : {
					"Pattern"	: "PRBS 31",
					"Differential Control" :  "852 mV",
				},

				"rx" : {
					"Loopback"	: "Near-End PMA",
					"Pattern"	: "PRBS 31",
					"Termination Voltage" : "800mv",
				}
			},
			
			"fiber_loopback" : {
				"comment" : "Optical fiber Loopback",
				"links" : ["SFP0", "SFP1"],

				"scan" : {
					"Target BER" : 1e-6,
					"Horizontal Step" : 2,
					"Vertical Step" : 2,
				},				

				"tx" : {
					"Pattern"	: "PRBS 31",
					"Differential Control" :  "852 mV",
				},

				"rx" : {
					"Loopback"	: "User Design",
					"Pattern"	: "PRBS 31",
					"Termination Voltage" : "800mv",
				},
			},
		},
	},



	# Vivado-based DDRMC and IBERT tests are obsolete, do not use

	# "vivado" : {
	# 	"server" : "127.0.0.1",
	# 	"port" : 8020,
	# 	"timeout" : 3,
	# 	"device" : "xcvm1802_1",		
	# },


	# "vivado-ibert" : {
	# 	"gt_links" : {
	# 		"SFP0" : {
	# 			"rx" : "IBERT_0.Quad_105.CH_2.RX",
	# 			"tx" : "IBERT_0.Quad_105.CH_2.TX",
	# 		},
	# 		"SFP1" : {
	# 			"rx" : "IBERT_0.Quad_105.CH_3.RX",
	# 			"tx" : "IBERT_0.Quad_105.CH_3.TX",
	# 		},
	# 	},
		
	# 	"default_link_config" : {
	# 		"LOOPBACK"		: "Near-End PMA",
	# 		"RX_PATTERN"	: "PRBS 31",
	# 		"TX_PATTERN"	: "PRBS 31",
	# 		"RX_TERMINATION_VOLTAGE" : "800mv",
	# 		"TX_DIFF_CTRL" :  "852 mV"
	# 	},

	# 	"scans" : {
	# 		"pma_near_loopback" : {
	# 			"comment" : "Near-End PMA Loopback",
	# 			"scan" : {
	# 				"DWELL_BER" : "1e-6",
	# 				"HORIZONTAL_INCREMENT" : "2",
	# 				"VERTICAL_INCREMENT" : "2",
	# 			},
	# 			"link" : {
	# 				"LOOPBACK"		: "Near-End PMA",
	# 				"RX_PATTERN"	: "PRBS 31",
	# 				"TX_PATTERN"	: "PRBS 31",
	# 				"RX_TERMINATION_VOLTAGE" : "800mv",
	# 				"TX_DIFF_CTRL" :  "852 mV"
	# 			}
	# 		},
	# 		"fiber_loopback" : {
	# 			"comment" : "Optical fiber Loopback",
	# 			"scan" : {
	# 				"DWELL_BER" : "1e-6",
	# 				"HORIZONTAL_INCREMENT" : "2",
	# 				"VERTICAL_INCREMENT" : "2",
	# 			},
	# 			"link" : {
	# 				"LOOPBACK"		: "User Design",
	# 				"RX_PATTERN"	: "PRBS 31",
	# 				"TX_PATTERN"	: "PRBS 31",
	# 				"RX_TERMINATION_VOLTAGE" : "800mv",
	# 				"TX_DIFF_CTRL" :  "852 mV"
	# 			}
	# 		}
	# 	},
	# },


	# "vivado-ddrmc" : {
	# 	"cores" : ["DDRMC_1"],
	# 	"style" : "versal"
	# }

}

