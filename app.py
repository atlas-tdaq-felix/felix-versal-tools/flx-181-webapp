#!/usr/bin/env python3

import os, argparse, importlib, sys, pdb, logging
from os import path
from unittest.mock import MagicMock, patch
from tests.mocks.i2c import I2C_Mock, make_i2c_handler_mock
from tests.mocks.vmk180 import zsfp_clk, qsfp_molex

from routes import create_app

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "elena.zhivun@cern.ch"

boards_module = "boards"


def main():
    platforms = get_board_list()
    parser = argparse.ArgumentParser(description="Webapp for automated hardware testing")
    parser.add_argument("board", nargs="?", type=str,
        help=f"Development board ({platforms})")
    parser.add_argument("-e", "--emulate_io", action="store_true", help="Emulate I/O operations with peripherals")
    parser.add_argument("-s", "--skip_io", action="store_true", help="Skip I/O operations with peripherals")
    args = parser.parse_args()

    if args.board is None:        
        parser.error(f"Please specify the hardware platform! Available platforms: {platforms}")
    else:
        config_module = f"{boards_module}.{args.board}"

    board = importlib.import_module(config_module)
    board_config = board.config

    if args.emulate_io:
        board_config["global"]["title"] += " -- EMULATED I/O"

        '''
            I2C_Mock model currently responds with the same register values 
            regardless of what I2C bus or address are. So this will create a
            single "fake" device for each device type that "responds" to all 
            queries for that device type. Meaning the webapp will show several 
            identical si570 devices, several identical SFP devices, etc.

        '''

        # assign fake si570
        print("Loading SI570 model")
        si570_i2c_handler = patch("util.si570.i2c_handler")
        si570_i2c_handler.new = make_i2c_handler_mock(I2C_Mock(zsfp_clk))
        si570_i2c_handler.start()

        # assign fake SFP devices
        print("Loading SFP model")
        sfp_i2c = patch("util.sfp.I2C")        
        sfp_i2c.new = lambda: I2C_Mock(qsfp_molex)        
        sfp_i2c.start()
        patch("util.sfp.set_line").start()

        # TODO: other devices
        

    config = {
        'SECRET_KEY' : os.urandom(32),
        'WTF_CSRF_CHECK_DEFAULT' : True,
        'USER_BOARD' : board_config,
        'USER_SKIP_IO' : args.skip_io,
    }

    app, api = create_app(name=__name__, config=config)
    # app.logger.setLevel(logging.DEBUG)

    if os.geteuid() == 0: # running as root
        app.run(host="0.0.0.0", port=80, debug=True)
    else:
        app.run(host="127.0.0.1", port=8080, debug=True)


def board_names(boards_path):
    for f in os.listdir(boards_path):        
        name, ext =  path.splitext(f)
        if path.isfile(path.join(boards_path, f)) and ext == ".py" and name != "__init__":
            yield name


def get_board_list():
    base_path = os.path.dirname(sys.argv[0])  
    boards_path = os.path.join(base_path, boards_module)
    platforms = [name for name in board_names(boards_path)]
    platforms_str = ", ".join(platforms)
    return platforms_str



if __name__ == '__main__':
    main()